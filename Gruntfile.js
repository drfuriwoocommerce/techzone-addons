module.exports = function (grunt) {
    // Project configuration
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        replace: {
            core: {
                src: [
                    '*.php',
                    'inc/*.php',
                    'inc/**/*.php'
                ],
                overwrite: true,
                replacements: [
                    {
                        from: 'drcore_',
                        to: '<%= pkg.prefix.func %>'
                    },
                    {
                        from: 'drcoreShortCode',
                        to: '<%= pkg.prefix.shortcode %>'
                    },
                    {
                        from: 'drcore-',
                        to: '<%= pkg.prefix.text %>' + '-'
                    },
                    {
                        from: 'DrCore_',
                        to: '<%= pkg.prefix.class %>'
                    },
                    {
                        from: 'DrCore - ',
                        to: '<%= pkg.prefix.widget %>'
                    },
                    {
                        from: 'THEME_DOMAIN',
                        to: "<%= pkg.name %>"
                    },
                    {
                        from: 'THEME_VERSION',
                        to: "'<%= pkg.date %>'"
                    },
                    {
                        from: '@package DrCore',
                        to: "@package <%= pkg.pkgname %>"
                    },
                    {
                        from: 'DRCORE_',
                        to: "<%= pkg.prefix.dir %>"
                    },
                ]

            },
            upgrade: {
                src: [
                    'functions.php'
                ],
                overwrite: true,
                replacements: [
                    {
                        from: /define\( 'MRBARA_VERSION', '(.*)' \)/g,
                        to: "define( 'MRBARA_VERSION', '<%= pkg.version %>' );"
                    }
                ]
            }
        },

        autoprefixer: {
            options: {
                browsers: ['Android >= 2.1', 'Chrome >= 21', 'Explorer >= 8', 'Firefox >= 17', 'Opera >= 12.1', 'Safari >= 6.0']
            },
            core: {
                expand: true,
                src: ['style.css', 'rtl.css']
            }
        },

        sass: {
            dist: {
                sourcemap: 'none',
                files: {
                    "style.css": "sass/style.scss"
                },
                lineNumbers: 4
            }
        },


        watch: {
            jsdev: {
                options: {livereload: true},
                files: ['js/scripts.js', 'js/comments.js', 'js/elements.js'],
                tasks: []
            },
            css: {
                files: ['sass/*.scss', 'sass/**/*.scss'],
                tasks: ['sass', 'autoprefixer']
            },
            core: {
                options: {livereload: true},
                files: ['*.php', '*/*.php', 'parts/*.php', 'inc/*.php', 'inc/*/*.php', '!inc/backend/*.php'],
                tasks: []
            },
            livereload: {
                options: {livereload: true},
                files: ['style.css', 'rtl.css']
            }

        }
    });

    // Load tasks.
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

    // Register makepot task
    grunt.registerTask('buildstart', ['replace:core']);

    // Register default tasks
    grunt.registerTask('default', ['sass', 'watch']);
};