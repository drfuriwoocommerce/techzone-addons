<?php
/**
 * Plugin Name: Teckzone Addons
 * Plugin URI: http://drfuri.com/plugins/teckzone-addons.zip
 * Description: Extra elements for Elementor. It was built for Teckzone theme.
 * Version: 1.0.0
 * Author: Drfuri
 * Author URI: http://drfuri.com/
 * License: GPL2+
 * Text Domain: teckzone
 * Domain Path: /lang/
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if ( ! defined( 'TECKZONE_ADDONS_DIR' ) ) {
	define( 'TECKZONE_ADDONS_DIR', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'TECKZONE_ADDONS_URL' ) ) {
	define( 'TECKZONE_ADDONS_URL', plugin_dir_url( __FILE__ ) );
}

require_once TECKZONE_ADDONS_DIR . '/inc/backend/brand-tax.php';
require_once TECKZONE_ADDONS_DIR . '/inc/backend/condition-tax.php';
require_once TECKZONE_ADDONS_DIR . '/inc/backend/collection-tax.php';
require_once TECKZONE_ADDONS_DIR . '/inc/backend/theme-builder.php';
require_once TECKZONE_ADDONS_DIR . '/inc/frontend/socials.php';
require_once TECKZONE_ADDONS_DIR . '/inc/frontend/user.php';
require_once TECKZONE_ADDONS_DIR . '/inc/widgets/widgets.php';

if ( is_admin() ) {
	require_once TECKZONE_ADDONS_DIR . '/inc/backend/importer.php';
}

/**
 * Init
 */
function teckzone_vc_addons_init() {
	load_plugin_textdomain( 'THEME_DOMAIN', false, dirname( plugin_basename( __FILE__ ) ) . '/lang' );

	new Teckzone_Product_Brands;
	new Teckzone_Product_Condition;
	new Teckzone_Product_Collection;

	new Teckzone_Theme_Builder;
}

add_action( 'after_setup_theme', 'teckzone_vc_addons_init', 20 );

/**
 * Undocumented function
 */
function teckzone_init_elementor() {
	// Check if Elementor installed and activated
	if ( ! did_action( 'elementor/loaded' ) ) {
		return;
	}

	// Check for required Elementor version
	if ( ! version_compare( ELEMENTOR_VERSION, '2.0.0', '>=' ) ) {
		return;
	}

	// Check for required PHP version
	if ( version_compare( PHP_VERSION, '5.4', '<' ) ) {
		return;
	}

	// Once we get here, We have passed all validation checks so we can safely include our plugin
	include_once( TECKZONE_ADDONS_DIR . 'inc/elementor/elementor.php' );
	include_once( TECKZONE_ADDONS_DIR . 'inc/elementor/controls.php' );
}

add_action( 'plugins_loaded', 'teckzone_init_elementor' );

/**
 * Check plugin dependencies.
 * Check if page builder plugin is installed.
 */
function teckzone_check_dependencies() {
	if ( ! defined( 'ELEMENTOR_VERSION' ) ) {
		$plugin_data = get_plugin_data( __FILE__ );

		printf(
			'<div class="notice notice-warning is-dismissible"><p>%s</p></div>',
			sprintf(
				__( '<strong>%s</strong> requires <strong><a href="https://wordpress.org/plugins/elementor/" target="_blank">Elementor Page Builder</a></strong> plugin to be installed and activated on your site.', 'teckzone' ),
				$plugin_data['Name']
			)
		);
	}
}

add_action( 'admin_notices', 'teckzone_check_dependencies' );