<?php

use Elementor\Controls_Stack;
use Elementor\Controls_Manager;
use Elementor\Element_Column;
use Elementor\Core\Base\Module;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class TZ_Element_Column extends Module {

	public function __construct() {

		$this->add_actions();
	}

	public function get_name() {
		return 'tz-content-align';
	}

	/**
	 * @param $element    Controls_Stack
	 * @param $section_id string
	 */
	public function register_controls( Controls_Stack $element, $section_id ) {
		if ( ! $element instanceof Element_Column ) {
			return;
		}
		$required_section_id = 'layout';

		if ( $required_section_id !== $section_id ) {
			return;
		}


		$element->add_control(
			'_tz_content_align',
			[
				'label'        => esc_html__( 'Content Align', 'martfury' ),
				'type'         => Controls_Manager::CHOOSE,
				'options'      => [
					'vertical'   => [
						'title' => esc_html__( 'Vertical', 'teckzone' ),
						'icon'  => 'fa fa-ellipsis-v',
					],
					'horizontal' => [
						'title' => esc_html__( 'Horizontal', 'teckzone' ),
						'icon'  => 'fa fa-ellipsis-h',
					],
				],
				'default'      => 'vertical',
				'prefix_class' => 'tz-flex-column-',
			]
		);

		$element->update_control(
			'content_position',
			['prefix_class' => 'tz-column-items-',]
		);
	}

	protected function add_actions() {
		add_action( 'elementor/element/before_section_end', [ $this, 'register_controls' ], 10, 2 );
	}
}

new TZ_Element_Column();
