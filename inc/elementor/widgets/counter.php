<?php

namespace TeckzoneAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Icon Box widget
 */
class Counter extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'techzone-counter';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Teckzone - Counter', 'teckzone' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-counter-circle';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'teckzone' ];
	}

	public function get_script_depends() {
		return [
			'techzone-elementor'
		];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->section_content();
		$this->section_style();
	}

	/**
	 * Section Content
	 */
	protected function section_content() {
		$this->start_controls_section(
			'section_content',
			[ 'label' => esc_html__( 'Content', 'teckzone' ) ]
		);

		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'icon',
			[
				'label'   => esc_html__( 'Icon', 'teckzone' ),
				'type'    => Controls_Manager::ICONS,
				'default'          => [
					'value'   => 'fas fa-star',
					'library' => 'fa-solid',
				],
			]
		);
		$repeater->add_control(
			'value', [
				'label'   => esc_html__( 'Value', 'teckzone' ),
				'type'    => Controls_Manager::TEXT,
				'default' => '',
			]
		);
		$repeater->add_control(
			'unit',
			[
				'label'       => esc_html__( 'Unit', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => '',
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'title',
			[
				'label'       => esc_html__( 'Title', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => '',
				'label_block' => true,
			]
		);
		$this->add_control(
			'counter',
			[
				'label'         => '',
				'type'          => Controls_Manager::REPEATER,
				'fields'        => $repeater->get_controls(),
				'default'       => [
					[
						'icon'  => ['value' => 'icon-group-work'],
						'value' => '8',
						'unit'  => 'M',
						'title' => esc_html__( 'Registed users', 'teckzone' ),
					],
					[
						'icon'  => ['value' => 'icon-store'],
						'value' => '15000',
						'unit'  => '+',
						'title' => esc_html__( 'Sellers', 'teckzone' ),
					],
					[
						'icon'  => ['value' => 'icon-receipt'],
						'value' => '50000',
						'unit'  => '+',
						'title' => esc_html__( 'Daily ordered', 'teckzone' ),
					],
					[
						'icon'  => ['value' => 'icon-users2'],
						'value' => '1',
						'unit'  => 'M',
						'title' => esc_html__( 'Daily page visits', 'teckzone' ),
					],
					[
						'icon'  => ['value' => 'icon-chart-growth'],
						'value' => '50',
						'unit'  => '%',
						'title' => esc_html__( 'Growth per year', 'teckzone' ),
					],
					[
						'icon'  => ['value' => 'icon-diamond4'],
						'value' => '400',
						'unit'  => '+',
						'title' => esc_html__( 'Top brands', 'teckzone' ),
					],
				],
				'prevent_empty' => false
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Section Style
	 */
	protected function section_style() {
		$this->general_section_style();
		$this->icon_section_style();
		$this->value_section_style();
		$this->title_section_style();
	}

	/**
	 * Element in Tab Style
	 *
	 * General
	 */
	protected function general_section_style() {
		$prefix = 'general';

		$this->start_controls_section(
			'section_' . $prefix . '_style',
			[
				'label' => __( 'General', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'columns',
			[
				'label'        => esc_html__( 'Columns', 'teckzone' ),
				'type'         => Controls_Manager::SELECT,
				'options'      => [
					'1' => esc_html__( '1 Columns', 'teckzone' ),
					'2' => esc_html__( '2 Columns', 'teckzone' ),
					'3' => esc_html__( '3 Columns', 'teckzone' ),
					'4' => esc_html__( '4 Columns', 'teckzone' ),
				],
				'default'      => '3',
				'toggle'       => false,
				'prefix_class' => 'columns-%s',
			]
		);
		$this->add_control(
			$prefix . '_background_color',
			[
				'label'     => __( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .techzone-counter .counter-item' => 'background-color: {{VALUE}};',
				],
				'default'   => '',
			]
		);
		$this->add_control(
			$prefix . '_border_style',
			[
				'label'     => esc_html__( 'Border Style', 'teckzone' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'dotted' => esc_html__( 'Dotted', 'teckzone' ),
					'dashed' => esc_html__( 'Dashed', 'teckzone' ),
					'solid'  => esc_html__( 'Solid', 'teckzone' ),
					'none'   => esc_html__( 'None', 'teckzone' ),
				],
				'default'   => 'solid',
				'toggle'    => false,
				'selectors' => [
					'{{WRAPPER}} .techzone-counter .counter-item' => 'border-style: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			$prefix . '_border_color',
			[
				'label'     => __( 'Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-counter .counter-item' => 'border-color: {{VALUE}};',
				],
			]
		);
		$this->add_responsive_control(
			$prefix . '_padding',
			[
				'label'      => __( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .techzone-counter .counter-item' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Element in Tab Style
	 *
	 * Icon
	 */
	protected function icon_section_style() {
		$prefix = 'icon';
		$this->start_controls_section(
			'section_' . $prefix . '_style',
			[
				'label' => __( 'Icon', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			$prefix . '_position',
			[
				'label'   => esc_html__( 'Position', 'teckzone' ),
				'type'    => Controls_Manager::SELECT,
				'options' => [
					'left'   => esc_html__( 'Left', 'teckzone' ),
					'right'  => esc_html__( 'Right', 'teckzone' ),
					'center' => esc_html__( 'Top Center', 'teckzone' ),
				],
				'default' => 'left',
				'toggle'  => false,
			]
		);
		$this->add_responsive_control(
			$prefix . '_spacing',
			[
				'label'     => __( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 200,
						'min' => 10,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-counter.techzone-counter--icon-left .counter-item__icon'   => 'padding-right: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .techzone-counter.techzone-counter--icon-right .counter-item__icon'  => 'padding-left: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .techzone-counter.techzone-counter--icon-center .counter-item__icon' => 'padding-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			$prefix . '_font_size',
			[
				'label'     => __( 'Font Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .techzone-counter .teckzone-icon' => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .techzone-counter .teckzone-icon svg' => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			$prefix . '_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-counter .teckzone-icon' => 'color: {{VALUE}};',
					'{{WRAPPER}} .techzone-counter .teckzone-icon svg' => 'fill: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Element in Tab Style
	 *
	 * Value
	 */
	protected function value_section_style() {
		$prefix = 'value';

		$this->start_controls_section(
			'section_' . $prefix . '_style',
			[
				'label' => __( 'Value', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			$prefix . '_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-counter .counter-value' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => $prefix . '_typography',
				'selector' => '{{WRAPPER}} .techzone-counter .counter-value',
			]
		);
		$this->add_responsive_control(
			$prefix . '_spacing',
			[
				'label'     => __( 'Bottom Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 30,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-counter .counter-value' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Element in Tab Style
	 *
	 * Title
	 */
	protected function title_section_style() {
		$prefix = 'title';

		$this->start_controls_section(
			'section_' . $prefix . '_style',
			[
				'label' => __( 'Title', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			$prefix . '_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-counter .title' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => $prefix . '_typography',
				'selector' => '{{WRAPPER}} .techzone-counter .title',
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$this->add_render_attribute(
			'wrapper', 'class', [
				'teckzone-counter techzone-counter',
				'teckzone-counter--icon-' . $settings['icon_position'],
				'techzone-counter--icon-' . $settings['icon_position'],
			]
		);

		$counter = $settings['counter'];

		$output = [ ];

		if ( ! empty ( $counter ) ) {
			$output[] = '<div class="counter-row">';

			foreach ( $counter as $index => $item ) {

				$icon = $value = $title = $unit = '';
				if ( $item['icon'] && \Elementor\Icons_Manager::is_migration_allowed() ) {
					ob_start();
					\Elementor\Icons_Manager::render_icon( $item['icon'], [ 'aria-hidden' => 'true' ] );
					$icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
				}

				if ( $item['unit'] ) {
					$unit = '<span class="unit">' . $item['unit'] . '</span>';
				}

				if ( $item['value'] ) {
					$value = '<span class="value">' . $item['value'] . '</span>';
					$value = '<div class="counter-value">' . $value . $unit . '</div>';
				}

				if ( $item['title'] ) {
					$title = '<div class="title">' . $item['title'] . '</div>';
				}

				$output[] = sprintf(
					'<div class="counter-item">
						<div class="counter-item__icon">%s</div>
						<div class="counter-item__content">%s%s</div>
					</div>',
					$icon,
					$value,
					$title
				);
			}

			$output[] = '</div>';
		}

		echo sprintf(
			'<div %s>%s</div>',
			$this->get_render_attribute_string( 'wrapper' ),
			implode( '', $output )
		);

	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected
	function _content_template() {
	}
}