<?php

namespace TeckzoneAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Background;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Icon Box widget
 */
class Banner extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'techzone-banner';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Teckzone - Banner', 'teckzone' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-banner';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'teckzone' ];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->start_controls_section(
			'section_banner',
			[ 'label' => esc_html__( 'Banner', 'teckzone' ) ]
		);
		$this->add_responsive_control(
			'height',
			[
				'label'     => esc_html__( 'Height', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [
					'unit' => 'px',
					'size' => 200,
				],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 1000,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .techzone-banner' => 'height: {{SIZE}}{{UNIT}};',
				],
				'separator' => 'after',
			]
		);
		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name'           => 'banners_background',
				'label'          => __( 'Background', 'teckzone' ),
				'types'          => [ 'classic', 'gradient' ],
				'selector'       => '{{WRAPPER}} .feature-image',
				'fields_options' => [
					'background' => [
						'default' => 'classic',
					],
					'image'      => [
						'default' => [
							'url' => 'https://via.placeholder.com/1170X200/f8f8f8?text=1170x200+Image',
						],
					],
				],
			]
		);
		$this->end_controls_section();

		// Elements
		$this->start_controls_section(
			'section_elements',
			[ 'label' => esc_html__( 'Elements', 'teckzone' ) ]
		);
		$repeater = new \Elementor\Repeater();

		$repeater->start_controls_tabs( 'elements_repeater' );
		// Content
		$repeater->start_controls_tab( 'content', [ 'label' => esc_html__( 'Content', 'teckzone' ) ] );
		$repeater->add_control(
			'elements',
			[
				'label'   => esc_html__( 'Elements', 'teckzone' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'text',
				'options' => [
					'text'   => esc_html__( 'Text/HTML', 'teckzone' ),
					'image'  => esc_html__( 'Image', 'teckzone' ),
					'button' => esc_html__( 'Button', 'teckzone' ),
				],
			]
		);
		$repeater->add_control(
			'text',
			[
				'label'     => esc_html__( 'Text', 'teckzone' ),
				'type'      => Controls_Manager::WYSIWYG,
				'default'   => esc_html__( 'Banner Text', 'teckzone' ),
				'condition' => [
					'elements' => [ 'text' ],
				],
			]
		);
		$repeater->add_control(
			'image', [
				'label'     => esc_html__( 'Choose Image', 'teckzone' ),
				'type'      => Controls_Manager::MEDIA,
				'default'   => [
					'url' => 'https://via.placeholder.com/270/f8f8f8?text=270x270+Image',
				],
				'condition' => [
					'elements' => [ 'image' ],
				],
			]
		);

		$repeater->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name'      => 'image',
				// Usage: `{name}_size` and `{name}_custom_dimension`, in this case `image_size` and `image_custom_dimension`.
				'default'   => 'large',
				'separator' => 'none',
				'condition' => [
					'elements' => [ 'image' ],
				],
			]
		);
		$repeater->add_control(
			'link_text', [
				'label'       => esc_html__( 'Link Text', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'Click Here', 'teckzone' ),
				'label_block' => true,
				'condition'   => [
					'elements' => [ 'button' ],
				],
			]
		);

		$repeater->add_control(
			'link_url', [
				'label'         => esc_html__( 'URL', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
				'condition'     => [
					'elements' => [ 'button' ],
				],
			]
		);
		$repeater->add_control(
			'link_icon',
			[
				'label'     => esc_html__( 'Icon', 'teckzone' ),
				'type'      => Controls_Manager::ICONS,
				'default'   => [
					'value'   => '',
					'library' => 'fa-solid',
				],
				'condition' => [
					'elements' => [ 'button' ],
				],
			]
		);
		$repeater->end_controls_tab();

		// Style
		$repeater->start_controls_tab( 'style', [ 'label' => esc_html__( 'Style', 'teckzone' ) ] );
		$repeater->add_responsive_control(
			'position_x',
			[
				'label'      => __( 'Position X', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 2000,
					],
					'%'  => [
						'min' => 0,
						'max' => 200,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .banner-layer-wrapper {{CURRENT_ITEM}}' => 'left: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$repeater->add_responsive_control(
			'position_y',
			[
				'label'      => __( 'Position Y', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 2000,
					],
					'%'  => [
						'min' => 0,
						'max' => 200,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .banner-layer-wrapper {{CURRENT_ITEM}}' => 'top: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$repeater->add_control(
			'divider_1',
			[
				'type' => Controls_Manager::DIVIDER,
			]
		);
		$repeater->add_responsive_control(
			'padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .banner-layer-wrapper {{CURRENT_ITEM}}:not(.banner-layer--button)' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					'{{WRAPPER}} .banner-layer-wrapper {{CURRENT_ITEM}}.banner-layer--button a'     => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$repeater->add_control(
			'text_align',
			[
				'label'     => __( 'Alignment', 'teckzone' ),
				'type'      => Controls_Manager::CHOOSE,
				'options'   => [
					'left'   => [
						'title' => __( 'Left', 'teckzone' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'teckzone' ),
						'icon'  => 'fa fa-align-center',
					],
					'right'  => [
						'title' => __( 'Right', 'teckzone' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				'default'   => 'left',
				'toggle'    => true,
				'selectors' => [
					'{{WRAPPER}} .banner-layer-wrapper {{CURRENT_ITEM}}' => 'text-align: {{VALUE}}',
				],
			]
		);
		$repeater->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'typography',
				'selector' => '{{WRAPPER}} .banner-layer-wrapper {{CURRENT_ITEM}}',
			]
		);
		$repeater->add_control(
			'color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .banner-layer-wrapper {{CURRENT_ITEM}}:not(.banner-layer--button)' => 'color: {{VALUE}}',
					'{{WRAPPER}} .banner-layer-wrapper {{CURRENT_ITEM}}.banner-layer--button a'     => 'color: {{VALUE}}',
				],
			]
		);
		$repeater->add_control(
			'background_color',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .banner-layer-wrapper {{CURRENT_ITEM}}:not(.banner-layer--button)' => 'background-color: {{VALUE}}',
					'{{WRAPPER}} .banner-layer-wrapper {{CURRENT_ITEM}}.banner-layer--button a'     => 'background-color: {{VALUE}}',
				],
			]
		);
		$repeater->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'      => 'border',
				'label'     => __( 'Border', 'teckzone' ),
				'selector'  => '{{WRAPPER}} .banner-layer-wrapper {{CURRENT_ITEM}}:not(.banner-layer--button)',
				'condition' => [
					'elements' => [ 'text', 'image' ],
				],
				'separator' => 'before',
			]
		);
		$repeater->add_responsive_control(
			'border_radius',
			[
				'label'      => esc_html__( 'Border Radius', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .banner-layer-wrapper {{CURRENT_ITEM}}:not(.banner-layer--button)' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'condition'  => [
					'elements' => [ 'text', 'image' ],
				],
			]
		);

		$repeater->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'      => 'button_border',
				'label'     => __( 'Border', 'teckzone' ),
				'selector'  => '{{WRAPPER}} .banner-layer-wrapper {{CURRENT_ITEM}}.banner-layer--button a',
				'condition' => [
					'elements' => [ 'button' ],
				],
				'separator' => 'before',
			]
		);
		$repeater->add_responsive_control(
			'button_border_radius',
			[
				'label'      => esc_html__( 'Border Radius', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .banner-layer-wrapper {{CURRENT_ITEM}}.banner-layer--button a' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'condition'  => [
					'elements' => [ 'button' ],
				],
			]
		);

		$repeater->add_control(
			'button_hover',
			[
				'label'     => __( 'Hover', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
				'condition' => [
					'elements' => [ 'button' ],
				],
			]
		);

		$repeater->add_control(
			'hover_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .banner-layer-wrapper {{CURRENT_ITEM}}.banner-layer--button a:hover' => 'color: {{VALUE}}',
				],
				'condition' => [
					'elements' => [ 'button' ],
				],
			]
		);
		$repeater->add_control(
			'hover_background_color',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .banner-layer-wrapper {{CURRENT_ITEM}}.banner-layer--button a:hover' => 'background-color: {{VALUE}}',
				],
				'condition' => [
					'elements' => [ 'button' ],
				],
			]
		);
		$repeater->add_control(
			'hover_border_color',
			[
				'label'     => esc_html__( 'Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .banner-layer-wrapper {{CURRENT_ITEM}}.banner-layer--button a:hover' => 'border-color: {{VALUE}}',
				],
				'condition' => [
					'elements' => [ 'button' ],
				],
			]
		);
		$repeater->add_control(
			'icon_settings',
			[
				'label'     => __( 'Icon', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
				'condition' => [
					'elements' => [ 'button' ],
				],
			]
		);
		$repeater->add_responsive_control(
			'icon_size',
			[
				'label'     => esc_html__( 'Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .banner-layer-wrapper {{CURRENT_ITEM}}.banner-layer--button a .teckzone-icon' => 'font-size: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'elements' => [ 'button' ],
				],
			]
		);
		$repeater->add_responsive_control(
			'icon_spacing',
			[
				'label'     => esc_html__( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .banner-layer-wrapper {{CURRENT_ITEM}}.banner-layer--button a .teckzone-icon' => 'padding-left: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'elements' => [ 'button' ],
				],
			]
		);
		$repeater->end_controls_tab();

		$repeater->end_controls_tabs();

		$this->add_control(
			'banner_elements',
			[
				'label'       => esc_html__( 'Banner Elements', 'teckzone' ),
				'type'        => Controls_Manager::REPEATER,
				'show_label'  => true,
				'fields'      => $repeater->get_controls(),
				'default'     => [
					[
						'elements' => 'text',
						'text'     => esc_html__( 'Banner Text 1', 'teckzone' ),
					],
					[
						'elements' => 'text',
						'text'     => esc_html__( 'Banner Text 2', 'teckzone' ),
					],
					[
						'elements'  => 'button',
						'link_text' => esc_html__( 'Click Here', 'teckzone' ),
						'link_url'  => [
							'url'         => '#',
							'is_external' => false,
							'nofollow'    => false,
						],
					],
				],
				'title_field' => '<# var el; if ( elements === "text" ) { el = text; } if ( elements === "button" ) {el = link_text;}#>
				<span style="text-transform: capitalize;">{{{ elements }}}: </span><span style="display: inline-block;">{{{el}}}</span>',
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$classes = [
			'teckzone-banner techzone-banner',
		];

		$this->add_render_attribute( 'wrapper', 'class', $classes );

		?>
		<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
			<div class="feature-image"></div>
			<?php
			$elements = $settings['banner_elements'];

			if ( $elements ) :

				echo '<div class="banner-layer-wrapper">';

				foreach ( $elements as $index => $element ) {
					$index   = $index + 1;
					$content = '';

					//esc_attr( $element['_id'] )

					if ( $element['elements'] == 'text' ) {
						$content = $element['text'];
					}

					if ( $element['elements'] == 'image' ) {
						$image = Group_Control_Image_Size::get_attachment_image_html( $element );

						$content = $image;
					}

					if ( $element['elements'] == 'button' ) {
						$link_key = 'link_' . $index;

						$icon = '';

						if ( $element['link_icon'] && ! empty( $element['link_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
							ob_start();
							\Elementor\Icons_Manager::render_icon( $element['link_icon'], [ 'aria-hidden' => 'true' ] );
							$icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
						}

						$text = '<span class="link-text">' . $element['link_text'] . '</span>';

						$content = $this->get_link_control( $link_key, $element['link_url'], $text . $icon, 'link' );

					}

					echo '<div class="elementor-repeater-item-' . $element['_id'] . ' banner-layer banner-layer--' . esc_attr( $element['elements'] ) . ' banner-layer--' . esc_attr( $index ) . '">' . $content . '</div>';
				}
				echo '</div>';
			endif;
			?>
		</div>
		<?php
	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected function _content_template() {

	}

	/**
	 * Get the link control
	 *
	 * @return string.
	 */
	protected function get_link_control( $link_key, $url, $content, $class_css ) {

		if ( $url['is_external'] ) {
			$this->add_render_attribute( $link_key, 'target', '_blank' );
		}

		if ( $url['nofollow'] ) {
			$this->add_render_attribute( $link_key, 'rel', 'nofollow' );
		}

		$attr = 'span';
		if ( $url['url'] ) {
			$this->add_render_attribute( $link_key, 'href', $url['url'] );
			$attr = 'a';
		}

		return sprintf( '<%1$s class="%4$s" %2$s>%3$s</%1$s>', $attr, $this->get_render_attribute_string( $link_key ), $content, $class_css );
	}
}