<?php

namespace TeckzoneAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Icon Box widget
 */
class Quick_Links extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'techzone-quick-links';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Teckzone - Quick Links', 'teckzone' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-editor-link';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'teckzone' ];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->_register_quick_links_settings_controls();
	}

	/**
	 * Register the widget content controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_quick_links_settings_controls() {
		// Settings
		$this->start_controls_section(
			'section_quick_links',
			[ 'label' => esc_html__( 'Links', 'teckzone' ) ]
		);

		$repeater = new \Elementor\Repeater();
		$repeater->add_control(
			'link_text', [
				'label'       => esc_html__( 'Text', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => '',
				'label_block' => true,
			]
		);

		$repeater->add_control(
			'link_url', [
				'label'         => esc_html__( 'Link', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);
		$repeater->add_control(
			'link_icon',
			[
				'label'   => esc_html__( 'Icon', 'teckzone' ),
				'type'    => Controls_Manager::ICONS,
				'default' => [
					'value'   => '',
					'library' => 'fa-solid',
				],
			]
		);

		$this->add_control(
			'links_group',
			[
				'label'         => esc_html__( 'Links', 'teckzone' ),
				'type'          => Controls_Manager::REPEATER,
				'fields'        => $repeater->get_controls(),
				'default'       => [
					[
						'link_text' => esc_html__( 'Link #1', 'teckzone' ),
						'link_url'  => '#',
						'link_icon' => '',
					],
					[
						'link_text' => esc_html__( 'Link #2', 'teckzone' ),
						'link_url'  => '#',
						'link_icon' => '',
					],
					[
						'link_text' => esc_html__( 'Link #3', 'teckzone' ),
						'link_url'  => '#',
						'link_icon' => '',
					],

				],
				'title_field'   => '{{{ link_text }}}',
				'prevent_empty' => false
			]
		);

		$this->end_controls_section(); // End Heading Settings

		// Style
		$this->start_controls_section(
			'section_quick_links_style',
			[
				'label' => esc_html__( 'Links', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'alignment',
			[
				'label'                => esc_html__( 'Alignment', 'teckzone' ),
				'label_block'          => false,
				'type'                 => Controls_Manager::SELECT,
				'options'              => [
					'left'   => esc_html__( 'Left', 'teckzone' ),
					'center' => esc_html__( 'Center', 'teckzone' ),
					'right'  => esc_html__( 'Right', 'teckzone' ),
				],
				'default'              => 'left',
				'selectors'            => [
					'{{WRAPPER}} .tz-quick-links ul' => '{{VALUE}}',
				],
				'selectors_dictionary' => [
					'left'   => 'justify-content: flex-start;',
					'center' => 'justify-content: center;',
					'right'  => 'justify-content: flex-end;',
				],
			]
		);
		$this->add_responsive_control(
			'spacing',
			[
				'label'     => __( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 200,
						'min' => 0,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-quick-links ul li' => 'padding-left: {{SIZE}}{{UNIT}}; padding-right: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .tz-quick-links ul'    => 'margin-left: -{{SIZE}}{{UNIT}}; margin-right: -{{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'link_typography',
				'selector' => '{{WRAPPER}} .tz-quick-links ul a',
			]
		);

		$this->add_control(
			'link_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-quick-links ul a span' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'link_color_hover',
			[
				'label'     => __( 'Hover Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-quick-links ul a:hover .link-text'     => 'color: {{VALUE}}; box-shadow: inset 0 0 0 transparent, inset 0 -1px 0 {{VALUE}};',
					'{{WRAPPER}} .tz-quick-links ul a:hover .teckzone-icon' => 'color: {{VALUE}};',
				],
			]
		);

		// Icon
		$this->add_control(
			'icon_style',
			[
				'label'        => __( 'Icon', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();

		$this->add_responsive_control(
			'icon_font_size',
			[
				'label'     => __( 'Font Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-quick-links ul a .teckzone-icon'     => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .tz-quick-links ul a .teckzone-icon svg' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->add_responsive_control(
			'icon_spacing',
			[
				'label'     => __( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 50,
						'min' => 0,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-quick-links ul a .teckzone-icon' => 'padding-left: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_popover();

		// Separator
		$this->add_control(
			'sep_style',
			[
				'label'        => __( 'Separator', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();

		$this->add_responsive_control(
			'sep_width',
			[
				'label'      => __( 'Width', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-quick-links ul li:after' => 'width: {{SIZE}}{{UNIT}};right: calc(-{{SIZE}}{{UNIT}}/2);',
				],
			]
		);
		$this->add_responsive_control(
			'sep_height',
			[
				'label'      => __( 'Height', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-quick-links ul li:after' => 'height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'sep_bg',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-quick-links ul li:after' => 'background-color: {{VALUE}};',
				],
			]
		);
		$this->end_popover();

		$this->end_controls_section();
	}

	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$this->add_render_attribute(
			'wrapper', 'class', [
				'tz-quick-links',
			]
		);

		$links = $settings['links_group'];

		?>
		<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
			<?php if ( ! empty( $links ) ) : ?>
				<ul class="extra-links">
					<?php
					foreach ( $links as $index => $item ) {
						$link_key = 'quick_link_' . $index;

						$icon = '';

						if ( $item['link_icon'] && ! empty( $item['link_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
							ob_start();
							\Elementor\Icons_Manager::render_icon( $item['link_icon'], [ 'aria-hidden' => 'true' ] );
							$icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
						}

						$text = '<span class="link-text">' . $item['link_text'] . '</span>';

						echo sprintf( '<li>%s</li>', $this->get_link_control( $link_key, $item['link_url'], $text . $icon, [ 'class' => 'extra-link' ] ) );
					}
					?>
				</ul>
			<?php endif; ?>
		</div>
		<?php
	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected function _content_template() {
	}

	/**
	 * Get the link control
	 *
	 * @return string.
	 */
	protected function get_link_control( $link_key, $url, $content, $attr = [ ] ) {
		$attr_default = [
			'href' => $url['url'] ? $url['url'] : '#',
		];

		if ( $url['is_external'] ) {
			$attr_default['target'] = '_blank';
		}

		if ( $url['nofollow'] ) {
			$attr_default['rel'] = 'nofollow';
		}

		$attr = wp_parse_args( $attr, $attr_default );

		$this->add_render_attribute( $link_key, $attr );

		return sprintf( '<a %1$s>%2$s</a>', $this->get_render_attribute_string( $link_key ), $content );
	}
}