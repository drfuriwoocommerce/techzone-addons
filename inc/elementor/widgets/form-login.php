<?php

namespace TeckzoneAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Widget_Base;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Icon Box widget
 */
class Form_Login extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'techzone-form-login';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Teckzone - Form Login', 'teckzone' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-user-circle-o';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'teckzone' ];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->_register_form_style_controls();
		$this->_register_field_style_controls();
		$this->_register_button_style_controls();
	}

	protected function _register_form_style_controls() {
		$this->start_controls_section(
			'section_form',
			[
				'label' => __( 'Form', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);


		$this->end_controls_section();
	}

	protected function _register_field_style_controls() {
		$this->start_controls_section(
			'section_field',
			[
				'label' => __( 'Field', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);


		$this->end_controls_section();
	}

	protected function _register_button_style_controls() {
		$this->start_controls_section(
			'section_button',
			[
				'label' => __( 'Button', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);


		$this->end_controls_section();
	}


	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		if ( is_user_logged_in() ) {
			return;
		}
		?>
		<div class="techzone-form-login woocommerce">
			<?php wc_get_template( 'myaccount/form-login.php' ); ?>
		</div>
		<?php
	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected
	function _content_template() {
	}
}