<?php

namespace TeckzoneAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Typography;
use Elementor\Utils;
use Elementor\Widget_Base;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Icon Box widget
 */
class Banner_App extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'techzone-banner-app';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Teckzone - Banner App', 'teckzone' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-apps';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'teckzone' ];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->section_content();
		$this->section_style();
	}

	// Tab Content
	protected function section_content() {
		$this->section_banner();
		$this->section_content_banner();
	}

	protected function section_banner() {
		$this->start_controls_section(
			'section_banner',
			[ 'label' => esc_html__( 'Banner', 'teckzone' ) ]
		);
		$this->add_control(
			'image',
			[
				'label'   => esc_html__( 'Banner', 'teckzone' ),
				'type'    => Controls_Manager::MEDIA,
				'default' => [
					'url' => 'https://via.placeholder.com/487x379/eeeeee?text=487x379+Banner',
				],

			]
		);

		$this->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name'    => 'image',
				'default' => 'full',
			]
		);

		$this->end_controls_section();
	}

	protected function section_content_banner() {
		$this->start_controls_section(
			'section_newsletter',
			[ 'label' => esc_html__( 'Content', 'teckzone' ) ]
		);

		$this->start_controls_tabs( 'content_setting_tabs' );

		// Image
		$this->start_controls_tab(
			'content_image_tab',
			[
				'label' => __( 'Image', 'teckzone' ),
			]
		);

		$this->add_control(
			'content_image',
			[
				'label'   => esc_html__( 'Image', 'teckzone' ),
				'type'    => Controls_Manager::MEDIA,
				'default' => [
					'url' => 'https://via.placeholder.com/146x26/eeeeee?text=146x26+Logo',
				],

			]
		);

		$this->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name'    => 'content_image',
				'default' => 'full',
			]
		);

		$this->end_controls_tab();

		// Title
		$this->start_controls_tab(
			'content_title',
			[
				'label' => __( 'Title', 'teckzone' ),
			]
		);

		$this->add_control(
			'title',
			[
				'label'       => esc_html__( 'Title', 'teckzone' ),
				'type'        => Controls_Manager::TEXTAREA,
				'default'     => esc_html__( 'This is the heading', 'teckzone' ),
				'placeholder' => esc_html__( 'Enter your title', 'teckzone' ),
				'label_block' => true,
			]
		);

		$this->end_controls_tab();

		// Desc
		$this->start_controls_tab(
			'content_desc',
			[
				'label' => __( 'Description', 'teckzone' ),
			]
		);
		$this->add_control(
			'desc',
			[
				'label'       => esc_html__( 'Description', 'teckzone' ),
				'type'        => Controls_Manager::WYSIWYG,
				'default'     => esc_html__( 'This is the description.', 'teckzone' ),
				'placeholder' => esc_html__( 'Enter your description', 'teckzone' ),
				'rows'        => 10,
			]
		);
		$this->end_controls_tab();

		// Button
		$this->start_controls_tab(
			'content_btn',
			[
				'label' => __( 'Button', 'teckzone' ),
			]
		);
		$this->add_control(
			'btn',
			[
				'label'     => esc_html__( 'Show Buttons', 'teckzone' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => esc_html__( 'Show', 'teckzone' ),
				'label_off' => esc_html__( 'Hide', 'teckzone' ),
				'default'   => 'yes',
			]
		);

		$repeater = new \Elementor\Repeater();
		$repeater->add_control(
			'btn_image', [
				'label'   => esc_html__( 'Choose Image', 'teckzone' ),
				'type'    => Controls_Manager::MEDIA,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
			]
		);
		$repeater->add_control(
			'button_link', [
				'label'         => esc_html__( 'Link', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],

			]
		);

		$this->add_control(
			'btn_setting',
			[
				'label'         => esc_html__( 'Buttons', 'teckzone' ),
				'type'          => Controls_Manager::REPEATER,
				'fields'        => $repeater->get_controls(),
				'default'       => [
					[
						'btn_image'   => [
							'url' => 'https://via.placeholder.com/127x43/eeeeee?text=127x43+Button'
						],
						'button_link' => [
							'url' => '#'
						]
					],
					[
						'btn_image'   => [
							'url' => 'https://via.placeholder.com/127x43/eeeeee?text=127x43+Button'
						],
						'button_link' => [
							'url' => '#'
						]
					]
				],
				'condition'     => [
					'btn' => 'yes',
				],
				'prevent_empty' => false
			]
		);
		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	// Tab Style
	protected function section_style() {
		$this->section_style_banner();
		$this->section_style_content();
		$this->section_style_image();
		$this->section_style_title();
		$this->section_style_desc();
		$this->section_style_btn();
	}

	protected function section_style_banner() {
		$this->start_controls_section(
			'section_style_banner',
			[
				'label' => esc_html__( 'Banner', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'banner_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .techzone-banner-app .form-image' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'banner_position',
			[
				'label'     => esc_html__( 'Banner Position', 'teckzone' ),
				'type'      => Controls_Manager::CHOOSE,
				'options'   => [
					'left'  => [
						'title' => esc_html__( 'Left', 'teckzone' ),
						'icon'  => 'fa fa-align-left',
					],
					'center'      => [
						'title' => esc_html__( 'Center', 'teckzone' ),
						'icon'  => 'fa fa-align-center',
					],
					'right' => [
						'title' => esc_html__( 'Right', 'teckzone' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				'default'   => 'right',
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-app .form-image' => 'text-align: {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();
	}

	protected function section_style_content() {
		$this->start_controls_section(
			'section_style_content',
			[
				'label' => esc_html__( 'Content', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'horizontal_position',
			[
				'label'       => esc_html__( 'Horizontal Position', 'teckzone' ),
				'type'        => Controls_Manager::CHOOSE,
				'label_block' => false,
				'options'     => [
					'end'      => [
						'title' => esc_html__( 'Top', 'teckzone' ),
						'icon'  => 'eicon-v-align-top',
					],
					'center'   => [
						'title' => esc_html__( 'Middle', 'teckzone' ),
						'icon'  => 'eicon-v-align-middle',
					],
					'flex-end' => [
						'title' => esc_html__( 'Bottom', 'teckzone' ),
						'icon'  => 'eicon-v-align-bottom',
					],
				],
				'default'     => 'center',
				'selectors'   => [
					'{{WRAPPER}} .techzone-banner-app .form-area' => 'justify-content:{{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'vertical_position',
			[
				'label'       => esc_html__( 'Vertical Position', 'teckzone' ),
				'type'        => Controls_Manager::CHOOSE,
				'label_block' => false,
				'options'     => [
					'left'   => [
						'title' => esc_html__( 'Left', 'teckzone' ),
						'icon'  => 'eicon-h-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Middle', 'teckzone' ),
						'icon'  => 'eicon-h-align-center',
					],
					'right'  => [
						'title' => esc_html__( 'Right', 'teckzone' ),
						'icon'  => 'eicon-h-align-right',
					],
				],
				'default'     => 'left',
				'selectors'   => [
					'{{WRAPPER}} .techzone-banner-app .form-area--inner' => 'text-align: {{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'content_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .techzone-banner-app .form-area' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'content_align',
			[
				'label'     => esc_html__( 'Alignment', 'teckzone' ),
				'type'      => Controls_Manager::CHOOSE,
				'options'   => [
					'left'       => [
						'title' => esc_html__( 'Left', 'teckzone' ),
						'icon'  => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'teckzone' ),
						'icon'  => 'fa fa-align-center',
					],
					'right'  => [
						'title' => esc_html__( 'Right', 'teckzone' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				'default'   => 'left',
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-app .form-area' => 'text-align: {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();
	}

	protected function section_style_image() {
		$this->start_controls_section(
			'section_style_bftitle',
			[
				'label' => esc_html__( 'Image', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'bftitle_spacing',
			[
				'label'     => esc_html__( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [
					'unit' => 'px',
				],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-app .content-image' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->end_controls_section();
	}

	protected function section_style_title() {
		$this->start_controls_section(
			'section_style_title',
			[
				'label' => esc_html__( 'Title', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'title_spacing',
			[
				'label'     => esc_html__( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [
					'unit' => 'px',
				],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-app .form-title' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'title_color',
			[
				'label'     => esc_html__( 'Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-app .form-title' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'title_typography',
				'selector' => '{{WRAPPER}} .techzone-banner-app .form-title',
			]
		);

		$this->end_controls_section();
	}

	protected function section_style_desc() {
		$this->start_controls_section(
			'section_style_description',
			[
				'label' => esc_html__( 'Description', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'description_spacing',
			[
				'label'     => esc_html__( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [
					'unit' => 'px',
				],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-app .form-desc' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'description_color',
			[
				'label'     => esc_html__( 'Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-app .form-desc' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'description_typography',
				'selector' => '{{WRAPPER}} .techzone-banner-app .form-desc',
			]
		);

		$this->end_controls_section();
	}

	protected function section_style_btn() {
		$this->start_controls_section(
			'section_style_buttons',
			[
				'label' => esc_html__( 'Buttons', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'buttons_item_spacing',
			[
				'label'     => esc_html__( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [
					'unit' => 'px',
				],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-app .btn-area a'               => 'margin-right: {{SIZE}}{{UNIT}}; margin-left: {{SIZE}}{{UNIT}}',
					'{{WRAPPER}} .techzone-banner-app .btn-area > a:last-child'  => 'margin-right: 0',
					'{{WRAPPER}} .techzone-banner-app .btn-area > a:first-child' => 'margin-left: 0',
				],
			]
		);
		$this->add_responsive_control(
			'button_alignment',
			[
				'label'     => esc_html__( 'Alignment', 'teckzone' ),
				'type'      => Controls_Manager::CHOOSE,
				'options'   => [
					'left'         => [
						'title' => esc_html__( 'Left', 'teckzone' ),
						'icon'  => 'fa fa-align-left',
					],
					'center'   => [
						'title' => esc_html__( 'Center', 'teckzone' ),
						'icon'  => 'fa fa-align-center',
					],
					'flex-end' => [
						'title' => esc_html__( 'Right', 'teckzone' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				'default'   => 'left',
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-app .btn-area' => 'justify-content: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}
	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$classes = [
			'teckzone-banner-app techzone-banner-app',
		];

		$image_html = '';

		$image = Group_Control_Image_Size::get_attachment_image_html( $settings );

		if ( $image ) {
			$image_html = sprintf( '<div class="form-image col-flex-md-6 col-flex-xs-12 col-flex-sm-12">%s</div>', $image );
		} else {
			$classes[] = 'no-image';
		}

		$this->add_render_attribute( 'wrapper', 'class', $classes );

		$btn_settings = $settings['btn_setting'];

		$output = [ ];

		if ( ! empty ( $btn_settings ) ) {
			foreach ( $btn_settings as $index => $item ) {
				$link_key = 'link_' . $index;

				if ( $item['button_link']['is_external'] ) {
					$this->add_render_attribute( $link_key, 'target', '_blank' );
				}

				if ( $item['button_link']['nofollow'] ) {
					$this->add_render_attribute( $link_key, 'rel', 'nofollow' );
				}

				$settings['image']      = $item['btn_image'];
				$settings['image_size'] = 'full';
				$btn_image              = Group_Control_Image_Size::get_attachment_image_html( $settings );

				$link = '';
				if ( $item['button_link']['url'] ) {
					$this->add_render_attribute( $link_key, 'href', $item['button_link']['url'] );
					$link = sprintf( '<a %s>%s</a>', $this->get_render_attribute_string( $link_key ), $btn_image );
				} else {
					$link = sprintf( '<span %s>%s</span>', $this->get_render_attribute_string( $link_key ), $btn_image );
				}

				$output[] = sprintf( '%s', $link );
			}
		}

		$btn_html = sprintf( '<div class="btn-area">%s</div>', implode( '', $output ) );

		$bf_title = $settings['content_image'] ? sprintf( '<div class="content-image">%s</div>', Group_Control_Image_Size::get_attachment_image_html( $settings, 'content_image' ) ) : '';

		?>
		<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
			<div class="container">
				<div class="row-flex">
					<?php echo $image_html ?>
					<div class="form-area col-flex-md-6 col-flex-xs-12 col-flex-sm-12 content-<?php echo esc_attr( $settings['content_align'] ) ?>">
						<div class="form-area--inner">
							<?php echo $bf_title; ?>
							<?php if ( $settings['title'] ) : ?>
								<h3 class="form-title"><?php echo $settings['title']; ?></h3>
							<?php endif; ?>
							<?php if ( $settings['desc'] ) : ?>
								<div class="form-desc"><?php echo $settings['desc']; ?></div>
							<?php endif; ?>
							<?php echo $btn_html; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected function _content_template() {
	}

}