<?php

namespace TeckzoneAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Icon Box widget
 */
class Video extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'techzone-video';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Teckzone - Video', 'teckzone' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'icon-play';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'teckzone' ];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->section_content();
		$this->section_style();
	}

	/**
	 * Section Content
	 */
	protected function section_content() {
		$this->start_controls_section(
			'section_content',
			[ 'label' => esc_html__( 'Content', 'teckzone' ) ]
		);

		$this->add_control(
			'type_click',
			[
				'label'        => esc_html__( 'Type Click', 'teckzone' ),
				'type'         => Controls_Manager::SELECT,
				'options'      => [
					'1' => esc_html__( 'Popup Video', 'teckzone' ),
					'2' => esc_html__( 'Open New Window', 'teckzone' ),
					'3' => esc_html__( 'Stay In Window', 'teckzone' ),
				],
				'default'      => '1',
				'toggle'       => false,
			]
		);

		$this->add_control(
			'type_icon',
			[
				'label'   => __( 'Type', 'teckzone' ),
				'type'    => Controls_Manager::CHOOSE,
				'options' => [
					'icon'  => [
						'title' => __( 'Icon', 'teckzone' ),
						'icon'  => 'fa fa-star',
					],
					'image' => [
						'title' => __( 'Image', 'teckzone' ),
						'icon'  => 'fa fa-picture-o',
					],
				],
				'default' => 'icon',
				'toggle'  => true,
			]
		);

		$this->add_control(
			'icon',
			[
				'label'     => esc_html__( 'Icon', 'teckzone' ),
				'type'      => Controls_Manager::ICONS,
				'default'          => [
					'value'   => 'icon-play-circle',
					'library' => 'linearicons',
				],
				'condition' => [
					'type_icon' => 'icon',
				],
			]
		);

		$this->add_control(
			'image', [
				'label'     => esc_html__( 'Choose Image', 'teckzone' ),
				'type'      => Controls_Manager::MEDIA,
				'default'   => [
					'url' => 'https://via.placeholder.com/270/f8f8f8?text=110x110+Image',
				],
				'condition' => [
					'type_icon' => 'image',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name'      => 'image',
				'default'   => 'full',
				'separator' => 'none',
				'condition' => [
					'type_icon' => 'image',
				],
			]
		);

		$this->add_control(
			'url',
			[
				'label'       => esc_html__( 'Url', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => '',
				'label_block' => true,
				'separator'   => 'before',
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Section Style
	 */
	protected function section_style() {
		$this->section_general_style();
		$this->section_icon_style();
	}

	protected function section_general_style() {
		$this->start_controls_section(
			'section_general_style',
			[
				'label' => __( 'General', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'align',
			[
				'label' => __( 'Alignment', 'teckzone' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'teckzone' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'teckzone' ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'teckzone' ),
						'icon' => 'fa fa-align-right',
					],
					'justify' => [
						'title' => __( 'Justified', 'teckzone' ),
						'icon' => 'fa fa-align-justify',
					],
				],
				'selectors' => [
					'{{WRAPPER}} .techzone-video' => 'text-align: {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();
	}

	protected function section_icon_style() {
		$this->start_controls_section(
			'section_icon_style',
			[
				'label' => __( 'Icon', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'icon_font_size',
			[
				'label'     => __( 'Font Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 30,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .techzone-video .teckzone-icon' => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .techzone-video .teckzone-icon svg' => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'icon_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-video .teckzone-icon' => 'color: {{VALUE}};',
					'{{WRAPPER}} .techzone-video .teckzone-icon svg' => 'fill: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$this->add_render_attribute(
			'wrapper', 'class', [
				'teckzone-video',
				'techzone-video',
			]
		);
		$img = $icon ='';
		if ( $settings['type_icon'] == 'image' && $settings['image'] ) {
			$img = Group_Control_Image_Size::get_attachment_image_html( $settings );
		}

		if ( $settings['type_icon'] == 'icon' && $settings['icon'] ) {
			if ( $settings['icon'] && \Elementor\Icons_Manager::is_migration_allowed() ) {
				ob_start();
				\Elementor\Icons_Manager::render_icon( $settings['icon'], [ 'aria-hidden' => 'true' ] );
				$icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
			}
		}

		$add_class = $settings['type_click'] == '1' ? 'play-banner' : 'no-effect';
		$target = $settings['type_click'] == '2' ? 'target="_blank"' : '';

		$icon_html = sprintf(
			'<a href="%s" class="%s" %s>' .
			'%s' .
			'%s' .
			'</a>',
			esc_attr($settings['url']),
			esc_attr($add_class),
			$target,
			$img,
			$icon
		);

		echo sprintf(
			'<div %s>%s</div>',
			$this->get_render_attribute_string( 'wrapper' ),
			$icon_html
		);

	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected
	function _content_template() {
	}
}