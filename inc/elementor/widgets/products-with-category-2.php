<?php

namespace TeckzoneAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;
use TeckzoneAddons\Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Icon Box widget
 */
class Products_With_Category_2 extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'techzone-product-with-category-2';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Product with Category 2', 'teckzone' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-products';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'teckzone' ];
	}

	public function get_script_depends() {
		return [
			'techzone-elementor'
		];
	}


	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {

		// Content Tab
		$this->register_heading_controls();
		$this->register_sidebar_controls();
		$this->register_products_controls();

		// Style Tab
		$this->register_heading_style_controls();
		$this->register_sidebar_style_controls();
		$this->register_products_style_controls();
	}

	/**
	 * Register the widget heading controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function register_heading_controls() {

		$this->start_controls_section(
			'section_heading',
			[ 'label' => esc_html__( 'Heading', 'teckzone' ) ]
		);

		$this->start_controls_tabs( 'heading_content_settings' );

		$this->start_controls_tab( 'title_tab', [ 'label' => esc_html__( 'Title', 'teckzone' ) ] );

		$this->add_control(
			'title',
			[
				'label'       => esc_html__( 'Title', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'Heading Title', 'teckzone' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'c_link', [
				'label'         => esc_html__( 'Link', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);
		$this->add_control(
			'title_icon',
			[
				'label'   => esc_html__( 'Icon', 'teckzone' ),
				'type'    => Controls_Manager::ICONS,
				'default' => [
					'value'   => '',
					'library' => 'fa-solid',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'view_all_tab', [ 'label' => esc_html__( 'Link', 'teckzone' ) ] );

		$this->add_control(
			'view_all_text',
			[
				'label'   => esc_html__( 'Text', 'teckzone' ),
				'type'    => Controls_Manager::TEXT,
				'default' => esc_html__( 'See all', 'teckzone' ),
			]
		);

		$this->add_control(
			'view_all_link', [
				'label'         => esc_html__( 'Link', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);
		$this->add_control(
			'view_all_icon',
			[
				'label'   => esc_html__( 'Icon', 'teckzone' ),
				'type'    => Controls_Manager::ICONS,
				'default' => [
					'value'   => 'icon-chevron-right',
					'library' => 'fa-solid',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	/**
	 * Register the widget sidebar controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function register_sidebar_controls() {

		$this->start_controls_section(
			'section_sidebar_content',
			[
				'label' => esc_html__( 'Sidebar', 'teckzone' ),
			]
		);
		$this->add_control(
			'banners_settings',
			[
				'label' => esc_html__( 'Banners', 'teckzone' ),
				'type'  => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'banners_carousel',
			[
				'label'     => esc_html__( 'Enable', 'teckzone' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => esc_html__( 'Yes', 'teckzone' ),
				'label_off' => esc_html__( 'No', 'teckzone' ),
				'default'   => 'yes',
			]
		);

		$this->start_controls_tabs( 'sidebar_content_settings' );

		$this->start_controls_tab( 'banner_images_tab', [ 'label' => esc_html__( 'Images', 'teckzone' ) ] );

		$repeater = new \Elementor\Repeater();
		$repeater->add_control(
			'image',
			[
				'label'   => esc_html__( 'Choose Image', 'teckzone' ),
				'type'    => Controls_Manager::MEDIA,
				'default' => [
					'url' => 'https://via.placeholder.com/455x328/e7eff1?text=455x328+Banner',
				],
			]
		);

		$repeater->add_control(
			'image_link', [
				'label'         => esc_html__( 'Link', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);

		$this->add_control(
			'banners',
			[
				'label'         => esc_html__( 'Images', 'teckzone' ),
				'type'          => Controls_Manager::REPEATER,
				'fields'        => $repeater->get_controls(),
				'default'       => [
					[
						'image'      => [
							'url' => 'https://via.placeholder.com/455x328/e7eff1?text=455x328+Banner'
						],
						'image_link' => [
							'url' => '#'
						]
					],
					[
						'image'      => [
							'url' => 'https://via.placeholder.com/455x328/e7eff1?text=455x328+Banner+2'
						],
						'image_link' => [
							'url' => '#'
						]
					]
				],
				'prevent_empty' => false
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'carousel_settings_tab', [ 'label' => esc_html__( 'Carousel Settings', 'teckzone' ) ] );

		$this->add_responsive_control(
			'navigation',
			[
				'label'   => esc_html__( 'Navigation', 'teckzone' ),
				'type'    => Controls_Manager::SELECT,
				'options' => [
					'both'   => esc_html__( 'Arrows and Dots', 'teckzone' ),
					'arrows' => esc_html__( 'Arrows', 'teckzone' ),
					'dots'   => esc_html__( 'Dots', 'teckzone' ),
					'none'   => esc_html__( 'None', 'teckzone' ),
				],
				'default' => 'both',
				'toggle'  => false,
			]
		);

		$this->add_control(
			'infinite',
			[
				'label'     => __( 'Infinite Loop', 'teckzone' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_off' => __( 'Off', 'teckzone' ),
				'label_on'  => __( 'On', 'teckzone' ),
				'default'   => 'yes'
			]
		);

		$this->add_control(
			'autoplay',
			[
				'label'     => __( 'Autoplay', 'teckzone' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_off' => __( 'Off', 'teckzone' ),
				'label_on'  => __( 'On', 'teckzone' ),
				'default'   => 'yes'
			]
		);

		$this->add_control(
			'autoplay_speed',
			[
				'label'   => __( 'Autoplay Speed (in ms)', 'teckzone' ),
				'type'    => Controls_Manager::NUMBER,
				'default' => 3000,
				'min'     => 100,
				'step'    => 100,
			]
		);

		$this->add_control(
			'speed',
			[
				'label'       => __( 'Speed', 'teckzone' ),
				'type'        => Controls_Manager::NUMBER,
				'default'     => 800,
				'min'         => 100,
				'step'        => 100,
				'description' => esc_html__( 'Slide animation speed (in ms)', 'teckzone' ),
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->add_control(
			'quicklink_settings',
			[
				'label'     => esc_html__( 'Quick Links', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before'
			]
		);
		$this->add_control(
			'quicklink_section',
			[
				'label'     => esc_html__( 'Enable', 'teckzone' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_on'  => esc_html__( 'Yes', 'teckzone' ),
				'label_off' => esc_html__( 'No', 'teckzone' ),
				'default'   => 'yes',
			]
		);
		$this->add_control(
			'quicklink_columns',
			[
				'label'   => esc_html__( 'Columns', 'teckzone' ),
				'type'    => Controls_Manager::SELECT,
				'options' => [
					'2' => esc_html__( '2 Columns', 'teckzone' ),
					'1' => esc_html__( '1 Column', 'teckzone' ),
				],
				'default' => '2',
				'toggle'  => false,
			]
		);
		$this->start_controls_tabs( 'quicklink_settings_tabs' );

		$this->start_controls_tab( 'quicklink_1_tab', [ 'label' => esc_html__( 'Quick Link 1', 'teckzone' ) ] );
		$this->add_control(
			'quicklink_title_1',
			[
				'label'       => esc_html__( 'Title', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'Quick link title #1', 'teckzone' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'quicklink_url_1', [
				'label'         => esc_html__( 'URL', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);
		$repeater = new \Elementor\Repeater();
		$repeater->add_control(
			'link_text_1', [
				'label'       => esc_html__( 'Title', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => '',
				'label_block' => true,
			]
		);

		$repeater->add_control(
			'link_url_1', [
				'label'         => esc_html__( 'Link', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);

		$this->add_control(
			'links_group_1',
			[
				'label'         => esc_html__( 'Items', 'teckzone' ),
				'type'          => Controls_Manager::REPEATER,
				'fields'        => $repeater->get_controls(),
				'default'       => [
					[
						'link_text_1' => esc_html__( 'Category #1', 'teckzone' ),
						'link_url_1'  => '#',
					],
					[
						'link_text_1' => esc_html__( 'Category #2', 'teckzone' ),
						'link_url_1'  => '#',
					],
					[
						'link_text_1' => esc_html__( 'Category #3', 'teckzone' ),
						'link_url_1'  => '#',
					],

				],
				'title_field'   => '{{{ link_text_1 }}}',
				'prevent_empty' => false
			]
		);
		$this->end_controls_tab();

		$this->start_controls_tab( 'quicklink_2_tab', [ 'label' => esc_html__( 'Quick Link 2', 'teckzone' ) ] );
		$this->add_control(
			'quicklink_title_2',
			[
				'label'       => esc_html__( 'Title', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'Quick link title #2', 'teckzone' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'quicklink_url_2', [
				'label'         => esc_html__( 'URL', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);
		$repeater = new \Elementor\Repeater();
		$repeater->add_control(
			'link_text_2', [
				'label'       => esc_html__( 'Title', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => '',
				'label_block' => true,
			]
		);

		$repeater->add_control(
			'link_url_2', [
				'label'         => esc_html__( 'Link', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);

		$this->add_control(
			'links_group_2',
			[
				'label'         => esc_html__( 'Items', 'teckzone' ),
				'type'          => Controls_Manager::REPEATER,
				'fields'        => $repeater->get_controls(),
				'default'       => [
					[
						'link_text_2' => esc_html__( 'Category #1', 'teckzone' ),
						'link_url_2'  => '#',
					],
					[
						'link_text_2' => esc_html__( 'Category #2', 'teckzone' ),
						'link_url_2'  => '#',
					],
					[
						'link_text_2' => esc_html__( 'Category #3', 'teckzone' ),
						'link_url_2'  => '#',
					],

				],
				'title_field'   => '{{{ link_text_2 }}}',
				'prevent_empty' => false
			]
		);
		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	/**
	 * Register the widget products controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function register_products_controls() {

		$this->start_controls_section(
			'section_product_content',
			[
				'label' => esc_html__( 'Products', 'teckzone' ),
			]
		);

		$this->add_control(
			'products',
			[
				'label'   => esc_html__( 'Products', 'teckzone' ),
				'type'    => Controls_Manager::SELECT,
				'options' => [
					'recent'       => esc_html__( 'Recent', 'teckzone' ),
					'featured'     => esc_html__( 'Featured', 'teckzone' ),
					'best_selling' => esc_html__( 'Best Selling', 'teckzone' ),
					'top_rated'    => esc_html__( 'Top Rated', 'teckzone' ),
					'sale'         => esc_html__( 'On Sale', 'teckzone' ),
				],
				'default' => 'recent',
				'toggle'  => false,
			]
		);

		$this->add_control(
			'product_cats',
			[
				'label'       => esc_html__( 'Product Categories', 'teckzone' ),
				'type'        => Controls_Manager::SELECT2,
				'options'     => Elementor::get_taxonomy(),
				'default'     => '',
				'multiple'    => true,
				'label_block' => true,
			]
		);

		$this->add_control(
			'product_tags',
			[
				'label'       => esc_html__( 'Product Tags', 'teckzone' ),
				'type'        => Controls_Manager::SELECT2,
				'options'     => Elementor::get_taxonomy( 'product_tag' ),
				'default'     => '',
				'multiple'    => true,
				'label_block' => true,
			]
		);

		$this->add_control(
			'per_page',
			[
				'label'   => esc_html__( 'Products per view', 'teckzone' ),
				'type'    => Controls_Manager::NUMBER,
				'default' => 8,
				'min'     => 2,
				'max'     => 50,
				'step'    => 1,
			]
		);

		$this->add_control(
			'columns',
			[
				'label'        => esc_html__( 'Product Columns', 'teckzone' ),
				'type'         => Controls_Manager::SELECT,
				'options'      => [
					'1' => esc_html__( '1 Column', 'teckzone' ),
					'2' => esc_html__( '2 Columns', 'teckzone' ),
					'3' => esc_html__( '3 Columns', 'teckzone' ),
					'4' => esc_html__( '4 Columns', 'teckzone' ),
					'5' => esc_html__( '5 Columns', 'teckzone' ),
					'6' => esc_html__( '6 Columns', 'teckzone' ),
				],
				'default'      => '4',
				'toggle'       => false,
				'prefix_class' => 'columns-',
			]
		);

		$this->add_control(
			'orderby',
			[
				'label'     => esc_html__( 'Order By', 'teckzone' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					''           => esc_html__( 'Default', 'teckzone' ),
					'date'       => esc_html__( 'Date', 'teckzone' ),
					'title'      => esc_html__( 'Title', 'teckzone' ),
					'menu_order' => esc_html__( 'Menu Order', 'teckzone' ),
					'rand'       => esc_html__( 'Random', 'teckzone' ),
				],
				'default'   => '',
				'condition' => [
					'products' => [ 'top_rated', 'sale', 'featured' ],
				],
			]
		);

		$this->add_control(
			'order',
			[
				'label'     => esc_html__( 'Order', 'teckzone' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					''     => esc_html__( 'Default', 'teckzone' ),
					'asc'  => esc_html__( 'Ascending', 'teckzone' ),
					'desc' => esc_html__( 'Descending', 'teckzone' ),
				],
				'default'   => '',
				'condition' => [
					'products' => [ 'top_rated', 'sale', 'featured' ],
				],
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Register the widget heading style controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function register_heading_style_controls() {

		$this->start_controls_section(
			'section_style_heading',
			[
				'label' => esc_html__( 'Heading', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'heading_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'heading_background_color',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'     => 'heading_border',
				'label'    => esc_html__( 'Border', 'teckzone' ),
				'selector' => '{{WRAPPER}} .tz-product-with-category-2 .header-cat',
			]
		);

		$this->add_control(
			'heading_style_divider',
			[
				'label' => '',
				'type'  => Controls_Manager::DIVIDER,
			]
		);

		$this->start_controls_tabs( 'heading_title_style_settings' );

		$this->start_controls_tab( 'heading_title_style_tab', [ 'label' => esc_html__( 'Title', 'teckzone' ) ] );

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'heading_title_typography',
				'selector' => '{{WRAPPER}} .tz-product-with-category-2 .header-cat h2',
			]
		);

		$this->add_control(
			'heading_title_color',
			[
				'label'     => esc_html__( 'Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat .cat-title'   => 'color: {{VALUE}}',
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat .cat-title a' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'heading_title_hover_color',
			[
				'label'     => esc_html__( 'Hover Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat .cat-title:hover'   => 'color: {{VALUE}}',
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat .cat-title:hover a' => 'color: {{VALUE}}',
				],
			]
		);

		// Icon
		$this->add_control(
			'title_icon_style',
			[
				'label'        => __( 'Icon', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);
		$this->start_popover();

		$this->add_control(
			'title_icon_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat h2 .teckzone-icon' => 'color: {{VALUE}}',
				],
			]
		);
		$this->add_responsive_control(
			'title_icon_font_size',
			[
				'label'      => esc_html__( 'Font size', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat h2 .teckzone-icon' => 'font-size: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'title_icon_right_spacing',
			[
				'label'      => esc_html__( 'Right Spacing', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat h2 .teckzone-icon' => 'margin-right: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->end_popover();

		$this->end_controls_tab();

		$this->start_controls_tab( 'heading_links_style_tab', [ 'label' => esc_html__( 'Links', 'teckzone' ) ] );

		$this->add_control(
			'heading_links_color',
			[
				'label'     => esc_html__( 'Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat .header-link span' => 'color: {{VALUE}}',

				],
			]
		);

		$this->add_control(
			'heading_links_hover_color',
			[
				'label'     => esc_html__( 'Hover Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat .header-link:hover span'       => 'color: {{VALUE}}',
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat .header-link:hover .link-text' => 'box-shadow: inset 0 0 0 transparent, inset 0 -1px 0 {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'heading_links_typography',
				'selector' => '{{WRAPPER}} .tz-product-with-category-2 .header-cat .header-link .link-text',
			]
		);

		// Icon
		$this->add_control(
			'header_link_icon_style',
			[
				'label'        => __( 'Icon', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
				'separator'    => 'before',
			]
		);

		$this->start_popover();

		$this->add_responsive_control(
			'header_link_icon_font_size',
			[
				'label'     => __( 'Font Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat .header-link .teckzone-icon'     => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat .header-link .teckzone-icon svg' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->add_responsive_control(
			'header_link_icon_spacing',
			[
				'label'     => __( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 20,
						'min' => 0,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .header-cat .header-link .teckzone-icon' => 'padding-left: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_popover();

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();

	}

	/**
	 * Register the widget sidebar style controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function register_sidebar_style_controls() {
		$this->start_controls_section(
			'section_style_sidebar',
			[
				'label' => esc_html__( 'Sidebar', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'sidebar_width',
			[
				'label'      => esc_html__( 'Width', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ '%', 'px' ],
				'range'      => [
					'%'  => [
						'min' => 0,
						'max' => 100
					],
					'px' => [
						'min' => 0,
						'max' => 1000
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-with-category-2 .sidebar-box' => 'max-width: {{SIZE}}{{UNIT}}; flex: 0 0 {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'sidebar_position',
			[
				'label'                => esc_html__( 'Position', 'teckzone' ),
				'type'                 => Controls_Manager::SELECT,
				'options'              => [
					'1' => esc_html__( 'Left', 'teckzone' ),
					'3' => esc_html__( 'Right', 'teckzone' ),
				],
				'default'              => '1',
				'toggle'               => false,
				'selectors'            => [
					'{{WRAPPER}} .tz-product-with-category-2 .sidebar-box' => '{{VALUE}}',
				],
				'selectors_dictionary' => [
					'1' => 'border-right: 1px solid #eee; order: 1;',
					'3' => 'border-left: 1px solid #eee; order: 3;',
				],
			]
		);
		$this->add_control(
			'sidebar_border_color',
			[
				'label'     => esc_html__( 'Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .sidebar-box' => 'border-color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();

		// Banner
		$this->start_controls_section(
			'section_style_banners',
			[
				'label' => esc_html__( 'Banners', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'arrows_style_divider',
			[
				'label' => esc_html__( 'Arrows', 'teckzone' ),
				'type'  => Controls_Manager::HEADING,
			]
		);

		// Arrows
		$this->add_control(
			'arrows_style',
			[
				'label'        => __( 'Options', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();

		$this->add_responsive_control(
			'sliders_arrows_size',
			[
				'label'     => __( 'Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 100,
						'min' => 0,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-arrow' => 'font-size: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'sliders_arrow_width',
			[
				'label'     => esc_html__( 'Width', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [
					'unit' => 'px',
				],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 500,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-arrow' => 'width: {{SIZE}}{{UNIT}}',
				],
				'separator' => 'before',
			]
		);

		$this->add_control(
			'sliders_arrow_height',
			[
				'label'     => esc_html__( 'Height', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [
					'unit' => 'px',
				],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 500,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-arrow' => 'height: {{SIZE}}{{UNIT}};line-height: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'sliders_arrows_offset',
			[
				'label'     => esc_html__( 'Horizontal Offset', 'teckzone' ),
				'type'      => Controls_Manager::NUMBER,
				'step'      => 1,
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-prev-arrow' => 'left: {{VALUE}}px;',
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-next-arrow' => 'right: {{VALUE}}px;',
				],
			]
		);

		$this->end_popover();

		$this->start_controls_tabs( 'sliders_normal_settings' );

		$this->start_controls_tab( 'sliders_normal', [ 'label' => esc_html__( 'Normal', 'teckzone' ) ] );

		$this->add_control(
			'sliders_arrow_background',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-arrow' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'sliders_arrow_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-arrow' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'sliders_arrow_border_color',
			[
				'label'     => esc_html__( 'Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-arrow' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'sliders_hover', [ 'label' => esc_html__( 'Hover', 'teckzone' ) ] );

		$this->add_control(
			'sliders_arrow_hover_background',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-arrow:hover' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'sliders_arrow_hover_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-arrow:hover' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_control(
			'sliders_arrow_hover_border_color',
			[
				'label'     => esc_html__( 'Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-arrow:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		// Dots
		$this->add_control(
			'dots_style_divider',
			[
				'label'     => esc_html__( 'Dots', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		$this->add_control(
			'dots_style',
			[
				'label'        => __( 'Options', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();

		$this->add_control(
			'sliders_dots_width',
			[
				'label'     => esc_html__( 'Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [
					'unit' => 'px',
				],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-dots li button'        => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}}',
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-dots li button:before' => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}}',
				],

			]
		);
		$this->add_responsive_control(
			'sliders_dots_offset',
			[
				'label'     => esc_html__( 'Vertical Offset', 'teckzone' ),
				'type'      => Controls_Manager::NUMBER,
				'step'      => 1,
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-dots' => 'bottom: {{VALUE}}px;',
				],
			]
		);
		$this->add_control(
			'sliders_dots_background',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-dots li button:before' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'sliders_dots_active_background',
			[
				'label'     => esc_html__( 'Active Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-dots li.slick-active button:before' => 'background-color: {{VALUE}};',
					'{{WRAPPER}} .tz-product-with-category-2 .images-slider .slick-dots li button:hover:before' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->end_popover();

		$this->end_controls_section();

		// Quick Link
		$this->start_controls_section(
			'section_style_quicklink',
			[
				'label' => esc_html__( 'Quick Link', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'quicklink_background',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .categories-box' => 'background-color: {{VALUE}};',
				],
			]
		);
		$this->add_responsive_control(
			'quicklink_padding',
			[
				'label'     => esc_html__( 'Padding', 'teckzone' ),
				'type'      => Controls_Manager::DIMENSIONS,
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .categories-box' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'quicklink_style_divider',
			[
				'label' => '',
				'type'  => Controls_Manager::DIVIDER,
			]
		);
		$this->start_controls_tabs( 'quicklink_style_settings' );
		$this->start_controls_tab( 'quicklink_title_style_tab', [ 'label' => esc_html__( 'Title', 'teckzone' ) ] );
		$this->add_responsive_control(
			'quicklink_title_spacing',
			[
				'label'      => esc_html__( 'Margin Bottom', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-with-category-2 .categories-box h4' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->add_control(
			'quicklink_title_color',
			[
				'label'     => esc_html__( 'Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .categories-box h4'   => 'color: {{VALUE}}',
					'{{WRAPPER}} .tz-product-with-category-2 .categories-box h4 a' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'quicklink_title_hover_color',
			[
				'label'     => esc_html__( 'Hover Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .categories-box h4:hover'   => 'color: {{VALUE}}',
					'{{WRAPPER}} .tz-product-with-category-2 .categories-box h4:hover a' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'quicklink_title_typography',
				'selector' => '{{WRAPPER}} .tz-product-with-category-2 .categories-box h4',
			]
		);
		$this->end_controls_tab();

		// Link
		$this->start_controls_tab( 'quicklink_item_style_tab', [ 'label' => esc_html__( 'Items', 'teckzone' ) ] );
		$this->add_responsive_control(
			'quicklink_items_padding',
			[
				'label'              => esc_html__( 'Padding', 'elementor' ),
				'type'               => Controls_Manager::DIMENSIONS,
				'size_units'         => [ 'px', '%' ],
				'allowed_dimensions' => 'vertical',
				'placeholder'        => [
					'top'    => '',
					'right'  => 'auto',
					'bottom' => '',
					'left'   => 'auto',
				],
				'selectors'          => [
					'{{WRAPPER}} .tz-product-with-category-2 .categories-box ul.extra-links li'             => 'padding-top: {{TOP}}{{UNIT}}; padding-bottom: {{BOTTOM}}{{UNIT}};',
					'{{WRAPPER}} .tz-product-with-category-2 .categories-box ul.extra-links li:first-child' => 'padding-top: 0;',
					'{{WRAPPER}} .tz-product-with-category-2 .categories-box ul.extra-links li:last-child'  => 'padding-bottom: 0;',
				],
			]
		);
		$this->add_control(
			'quicklink_items_color',
			[
				'label'     => esc_html__( 'Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .categories-box ul.extra-links a' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'quicklink_items_hover_color',
			[
				'label'     => esc_html__( 'Hover Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 .categories-box ul.extra-links a:hover' => 'color: {{VALUE}}; box-shadow: inset 0 0 0 transparent, inset 0 -1px 0 {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'quicklink_link_typography',
				'selector' => '{{WRAPPER}} .tz-product-with-category-2 .categories-box ul.extra-links',
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();
		$this->end_controls_section();
	}

	/**
	 * Register the widget products style controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function register_products_style_controls() {

		$this->start_controls_section(
			'section_style_products',
			[
				'label' => esc_html__( 'Products', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'products_border_color',
			[
				'label'     => esc_html__( 'Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 ul.products li.product' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'products_item_border_color',
			[
				'label'     => esc_html__( 'Items Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-with-category-2 ul.products li.product .product-inner:hover'   => 'border-color: {{VALUE}};',
					'{{WRAPPER}} .tz-product-with-category-2 ul.products li.product .product-details-hover' => 'border-color: {{VALUE}};',
				],
			]
		);
		$this->add_responsive_control(
			'action_button',
			[
				'label'       => esc_html__( 'Wishlist/Compare Text', 'teckzone' ),
				'label_block' => true,
				'type'        => Controls_Manager::SELECT,
				'options'     => [
					'inline-block' => esc_html__( 'Show', 'teckzone' ),
					'none'         => esc_html__( 'Hide', 'teckzone' ),
				],
				'default'     => 'inline-block',
				'selectors'   => [
					'{{WRAPPER}} .tz-product-with-category-2 ul.products li.product .product-button .group a span' => 'display: {{VALUE}}',
				],
			]
		);
		$this->add_responsive_control(
			'swatches',
			[
				'label'       => esc_html__( 'Swatches', 'teckzone' ),
				'label_block' => true,
				'type'        => Controls_Manager::SELECT,
				'options'     => [
					'block' => esc_html__( 'Show', 'teckzone' ),
					'none'  => esc_html__( 'Hide', 'teckzone' ),
				],
				'default'     => 'block',
				'selectors'   => [
					'{{WRAPPER}} .tz-product-with-category-2 ul.products li.product .product-thumbnail .tz-attr-swatches' => 'display: {{VALUE}}',
				],
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$this->add_render_attribute(
			'wrapper', 'class', [
				'tz-product-with-category-2 woocommerce'
			]
		);

		$icon = $view_all_icon = '';
		if ( $settings['title_icon'] && ! empty( $settings['title_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['title_icon'], [ 'aria-hidden' => 'true' ] );
			$icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		if ( $settings['view_all_icon'] && ! empty( $settings['view_all_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['view_all_icon'], [ 'aria-hidden' => 'true' ] );
			$view_all_icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		$is_rtl    = is_rtl();
		$direction = $is_rtl ? 'rtl' : 'ltr';
		$this->add_render_attribute( 'wrapper', 'dir', $direction );

		$slides = [ 'slidesToShow', 'slidesToScroll', 'slidesToShow_tablet', 'slidesToScroll_tablet', 'slidesToShow_mobile', 'slidesToScroll_mobile' ];

		foreach ( $slides as $slide ) {
			$settings[$slide] = 1;
		}

		$this->add_render_attribute( 'images_slider', 'class', [ 'images-list' ] );
		$this->add_render_attribute( 'images_slider', 'data-slick', wp_json_encode( Elementor::get_data_slick( $settings ) ) );
		?>
		<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
			<div class="header-cat">
				<h2><?php echo $this->get_link_control( 'link', $settings['c_link'], $icon . $settings['title'], [ 'class' => 'cat-title' ] ) ?></h2>
				<?php
				$link_text = '<span class="link-text">' . $settings['view_all_text'] . '</span>';
				echo $this->get_link_control( 'footer_link', $settings['view_all_link'], $link_text . $view_all_icon, [ 'class' => 'header-link' ] );
				?>
			</div>
			<div class="content-wrapper">
				<?php if ( $settings['banners_carousel'] == 'yes' || $settings['quicklink_section'] == 'yes' ) : ?>
					<div class="sidebar-box">
						<?php if ( $settings['banners_carousel'] == 'yes' ) : ?>
							<div class="images-slider">
								<div <?php echo $this->get_render_attribute_string( 'images_slider' ); ?>>
									<?php
									$banners = $settings['banners'];
									if ( $banners ) {
										foreach ( $banners as $index => $item ) {
											if ( empty( $item['image'] ) ) {
												continue;
											}
											$link_key               = 'banner_link' . $index;
											$settings['image']      = $item['image'];
											$settings['image_size'] = 'full';
											$image_url              = Group_Control_Image_Size::get_attachment_image_html( $settings );
											echo $this->get_link_control( $link_key, $item['image_link'], $image_url, [ 'class' => 'image-item' ] );
										}
									}
									?>
								</div>
							</div>
						<?php endif; ?>

						<?php if ( $settings['quicklink_section'] == 'yes' ) : ?>
							<div class="categories-box categories-box--columns-<?php echo $settings['quicklink_columns']; ?>">
								<?php for ( $i = 1; $i < 3; $i ++ ) : ?>
									<div class="category-box category-box--<?php echo esc_attr( $i ) ?>">
										<?php echo $settings['quicklink_title_' . $i] ? sprintf( '<h4>%s</h4>', $this->get_link_control( 'quicklink_' . $i, $settings['quicklink_url_' . $i], $settings['quicklink_title_' . $i] ) ) : ''; ?>
										<ul class="extra-links">
											<?php
											$links = $settings['links_group_' . $i];
											if ( $links ) {
												foreach ( $links as $index => $item ) {
													$link_key = 'extra_link_' . $i . $index;
													echo sprintf( '<li>%s</li>', $this->get_link_control( $link_key, $item['link_url_' . $i], $item['link_text_' . $i], [ 'class' => 'extra-link' ] ) );
												}
											}
											?>
										</ul>
									</div>
								<?php endfor; ?>
							</div>
						<?php endif; ?>
					</div>
				<?php endif; ?>
				<div class="products-box">
					<?php echo Elementor::get_products( $settings ); ?>
				</div>
			</div>
		</div>
		<?php
	}

	/**
	 * Get the link control
	 *
	 * @return string.
	 */
	protected function get_link_control( $link_key, $url, $content, $attr = [ ] ) {
		$attr_default = [
			'href' => $url['url'] ? $url['url'] : '#',
		];

		if ( $url['is_external'] ) {
			$attr_default['target'] = '_blank';
		}

		if ( $url['nofollow'] ) {
			$attr_default['rel'] = 'nofollow';
		}

		$attr = wp_parse_args( $attr, $attr_default );

		$this->add_render_attribute( $link_key, $attr );

		return sprintf( '<a %1$s>%2$s</a>', $this->get_render_attribute_string( $link_key ), $content );
	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected function _content_template() {
	}
}