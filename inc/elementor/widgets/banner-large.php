<?php

namespace TeckzoneAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Background;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Banner Small widget
 */
class Banner_Large extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'techzone-advertisement';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Teckzone - Banner Large', 'teckzone' );
	}


	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-banner';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'teckzone' ];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->section_content();
		$this->section_style();
	}

	// Tab Content
	protected function section_content() {
		$this->start_controls_section(
			'section_banner',
			[ 'label' => esc_html__( 'Section Banner', 'teckzone' ) ]
		);

		$this->add_responsive_control(
			'height',
			[
				'label'     => esc_html__( 'Height', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 100,
						'max' => 600,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-large--style-1 .banner-content' => 'height: {{SIZE}}{{UNIT}};',
				],
				'separator' => 'after',
			]
		);

		$this->start_controls_tabs( 'banner_content_settings' );

		$this->start_controls_tab( 'content', [ 'label' => esc_html__( 'Content', 'teckzone' ) ] );

		$this->add_control(
			'title',
			[
				'label'       => esc_html__( 'Title', 'teckzone' ),
				'type'        => Controls_Manager::TEXTAREA,
				'default'     => __( 'This is the title', 'teckzone' ),
				'placeholder' => esc_html__( 'Enter your title', 'teckzone' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'desc',
			[
				'label'       => esc_html__( 'Description', 'teckzone' ),
				'type'        => Controls_Manager::WYSIWYG,
				'default'     => __( 'This is the description', 'teckzone' ),
				'placeholder' => esc_html__( 'Enter your description', 'teckzone' ),
				'label_block' => true,
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'button', [ 'label' => esc_html__( 'Button', 'teckzone' ) ] );

		$this->add_control(
			'button_text', [
				'label'         => esc_html__( 'Button Text', 'teckzone' ),
				'type'          => Controls_Manager::TEXT,
				'show_external' => true,
				'default'       => esc_html__( 'Shop Now', 'teckzone' ),
			]
		);

		$this->add_control(
			'button_link', [
				'label'         => esc_html__( 'Button URL', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);

		$this->add_control(
			'icon',
			[
				'label'   => esc_html__( 'Icon', 'teckzone' ),
				'type'    => Controls_Manager::ICONS,
				'default' => [
					'value'   => 'icon-chevron-right',
					'library' => 'linearicons',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'background', [ 'label' => esc_html__( 'Background', 'teckzone' ) ] );

		$this->add_group_control(
			Group_Control_Background::get_type(),
			[
				'name'           => 'banners_background',
				'label'          => __( 'Background', 'teckzone' ),
				'types'          => [ 'classic', 'gradient' ],
				'selector'       => '{{WRAPPER}} .techzone-banner-large--style-1 .banner-featured-image',
				'fields_options' => [
					'background' => [
						'default' => 'classic',
					],
					'image'      => [
						'default' => [
							'url' => 'https://via.placeholder.com/1170X120/f8f8f8?text=1170x120+Image',
						],
					],
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	// Tab Style
	protected function section_style() {
		$this->section_style_banner();
		$this->section_style_title();
		$this->section_style_desc();
		$this->section_style_button();
		$this->section_style_icon();
	}

	protected function section_style_banner() {
		// Banner
		$this->start_controls_section(
			'section_style_banner',
			[
				'label' => esc_html__( 'Content Banner', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'banner_content_display',
			[
				'label'   => esc_html__( 'Display', 'teckzone' ),
				'type'    => Controls_Manager::SELECT,
				'options' => [
					'block' => esc_html__( 'Block', 'teckzone' ),
					'flex'  => esc_html__( 'Flex', 'teckzone' ),
				],
				'default' => 'flex',
			]
		);

		$this->add_control(
			'banner_text_align',
			[
				'label'       => esc_html__( 'Align', 'teckzone' ),
				'type'        => Controls_Manager::CHOOSE,
				'label_block' => false,
				'options'     => [
					'end'      => [
						'title' => esc_html__( 'Left', 'teckzone' ),
						'icon'  => 'fa fa-align-left',
					],
					'center'   => [
						'title' => esc_html__( 'Center', 'teckzone' ),
						'icon'  => 'fa fa-align-center',
					],
					'flex-end' => [
						'title' => esc_html__( 'Right', 'teckzone' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				'default'     => '',
				'selectors'   => [
					'{{WRAPPER}} .techzone-banner-large--style-1 .banner-content' => 'justify-content: {{VALUE}}',
				],
				'conditions'  => [
					'terms' => [
						[
							'name'  => 'banner_content_display',
							'value' => 'flex',
						],
					],
				],
			]
		);

		$this->add_responsive_control(
			'horizontal_position',
			[
				'label'       => esc_html__( 'Vertical Position', 'teckzone' ),
				'type'        => Controls_Manager::CHOOSE,
				'label_block' => false,
				'options'     => [
					'end'      => [
						'title' => esc_html__( 'Top', 'teckzone' ),
						'icon'  => 'eicon-v-align-top',
					],
					'center'   => [
						'title' => esc_html__( 'Middle', 'teckzone' ),
						'icon'  => 'eicon-v-align-middle',
					],
					'flex-end' => [
						'title' => esc_html__( 'Borrom', 'teckzone' ),
						'icon'  => 'eicon-v-align-bottom',
					],
				],
				'default'     => '',
				'selectors'   => [
					'{{WRAPPER}} .techzone-banner-large--style-1 .banner-content' => 'align-items:{{VALUE}}',
				],
				'conditions'  => [
					'terms' => [
						[
							'name'  => 'banner_content_display',
							'value' => 'flex',
						],
					],
				],
			]
		);

		$this->add_responsive_control(
			'ver_position',
			[
				'label'       => esc_html__( 'Vertical Position', 'teckzone' ),
				'type'        => Controls_Manager::CHOOSE,
				'label_block' => false,
				'options'     => [
					'end'      => [
						'title' => esc_html__( 'Top', 'teckzone' ),
						'icon'  => 'eicon-v-align-top',
					],
					'center'   => [
						'title' => esc_html__( 'Middle', 'teckzone' ),
						'icon'  => 'eicon-v-align-middle',
					],
					'flex-end' => [
						'title' => esc_html__( 'Borrom', 'teckzone' ),
						'icon'  => 'eicon-v-align-bottom',
					],
				],
				'default'     => '',
				'selectors'   => [
					'{{WRAPPER}} .techzone-banner-large--style-1 .banner-content' => 'justify-content:{{VALUE}}',
				],
				'conditions'  => [
					'terms' => [
						[
							'name'  => 'banner_content_display',
							'value' => 'block',
						],
					],
				],
			]
		);

		$this->add_responsive_control(
			'banner_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'default'    => [ ],
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .techzone-banner-large--style-1 .banner-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator'  => 'after',
			]
		);

		$this->add_control(
			'border_style',
			[
				'label'        => __( 'Border', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();

		$this->add_control(
			'content_border_style',
			[
				'label'     => esc_html__( 'Border Style', 'teckzone' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'dotted' => esc_html__( 'Dotted', 'teckzone' ),
					'dashed' => esc_html__( 'Dashed', 'teckzone' ),
					'solid'  => esc_html__( 'Solid', 'teckzone' ),
					'none'   => esc_html__( 'None', 'teckzone' ),
				],
				'default'   => '',
				'toggle'    => false,
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-large--style-1' => 'border-style: {{VALUE}};',
				],
			]
		);

		$this->add_responsive_control(
			'content_border_width',
			[
				'label'     => __( 'Border Width', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 7,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-large--style-1' => 'border-width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'content_border_color',
			[
				'label'     => __( 'Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-large--style-1' => 'border-color: {{VALUE}};',
				],
			]
		);
		$this->end_popover();

		$this->end_controls_section();
	}

	protected function section_style_title() {
		// Title
		$this->start_controls_section(
			'section_style_title',
			[
				'label' => esc_html__( 'Title', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'heading_spacing',
			[
				'label'     => esc_html__( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-large--style-1 .banner-title' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'heading_color',
			[
				'label'     => esc_html__( 'Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-large--style-1 .banner-title' => 'color: {{VALUE}}',

				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'heading_typography',
				'selector' => '{{WRAPPER}} .techzone-banner-large--style-1 .banner-title',
			]
		);

		$this->end_controls_section();
	}

	protected function section_style_desc() {
		// Description
		$this->start_controls_section(
			'section_style_description',
			[
				'label' => esc_html__( 'Description', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'description_spacing',
			[
				'label'     => esc_html__( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-large--style-1 .banner-desc' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'description_color',
			[
				'label'     => esc_html__( 'Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-large--style-1 .banner-desc' => 'color: {{VALUE}}',

				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'description_typography',
				'selector' => '{{WRAPPER}} .techzone-banner-large--style-1 .banner-desc',
			]
		);

		$this->end_controls_section();
	}

	protected function section_style_button() {
		// Button
		$this->start_controls_section(
			'section_style_button',
			[
				'label' => esc_html__( 'Button', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'show_button',
			[
				'label'     => esc_html__( 'Show Button', 'teckzone' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'flex' => esc_html__( 'Show', 'teckzone' ),
					'none' => esc_html__( 'Hide', 'teckzone' ),
				],
				'default'   => 'flex',
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-large--style-1 .btn-button' => 'display: {{VALUE}}',

				],
			]
		);

		$this->add_control(
			'button_spacing',
			[
				'label'      => esc_html__( 'Spacing', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'default'    => [ ],
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .techzone-banner-large--style-1.techzone-banner-large--display-flex .btn-button'  => 'margin-left: {{SIZE}}{{UNIT}}',
					'{{WRAPPER}} .techzone-banner-large--style-1.techzone-banner-large--display-block .btn-button' => 'margin-top: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'button_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .techzone-banner-large--style-1 .btn-button' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'button_typography',
				'selector' => '{{WRAPPER}} .techzone-banner-large--style-1 .btn-button',
			]
		);

		$this->add_control(
			'button_border_radius',
			[
				'label'      => esc_html__( 'Border Radius', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .techzone-banner-large--style-1 .btn-button' => 'border-radius: {{SIZE}}{{UNIT}};',
				],
				'separator'  => 'after',
			]
		);

		$this->start_controls_tabs( 'button_tabs' );

		$this->start_controls_tab( 'normal', [ 'label' => esc_html__( 'Normal', 'teckzone' ) ] );

		$this->add_control(
			'button_text_color',
			[
				'label'     => esc_html__( 'Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-large--style-1 .btn-button' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'button_background_color',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-large--style-1 .btn-button' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'button_border_color',
			[
				'label'     => esc_html__( 'Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-large--style-1 .btn-button' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'hover', [ 'label' => esc_html__( 'Hover', 'teckzone' ) ] );

		$this->add_control(
			'button_hover_text_color',
			[
				'label'     => esc_html__( 'Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-large--style-1 .btn-button:hover' => 'color: {{VALUE}};',
				],

			]
		);

		$this->add_control(
			'button_hover_background_color',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-large--style-1 .btn-button:hover' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'button_hover_border_color',
			[
				'label'     => esc_html__( 'Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-large--style-1 .btn-button:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function section_style_icon() {
		$this->start_controls_section(
			'section_icon_style',
			[
				'label' => __( 'Icon', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'icon_spacing',
			[
				'label'     => __( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 50,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-large--style-1 .teckzone-icon' => 'margin-left: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'icon_font_size',
			[
				'label'     => __( 'Font Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 30,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-large--style-1 .teckzone-icon'     => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .techzone-banner-large--style-1 .teckzone-icon svg' => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}};',

				],
			]
		);
		$this->add_control(
			'icon_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-banner-large--style-1 .teckzone-icon'     => 'color: {{VALUE}};',
					'{{WRAPPER}} .techzone-banner-large--style-1 .teckzone-icon svg' => 'fill: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$display = $settings['banner_content_display'] ? $settings['banner_content_display'] : 'flex';

		$this->add_render_attribute(
			'wrapper', 'class', [
			'teckzone-banner-large techzone-banner-large',
			'teckzone-banner-large--style-1 techzone-banner-large--style-1',
			'teckzone-banner-large--display-' . $display,
			'techzone-banner-large--display-' . $display,
		]
		);

		$links = $settings['button_link']['url'] ? $this->get_link_control( 'link', $settings['button_link'], '', 'link' ) : '';

		$icon = '';
		if ( $settings['icon'] && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['icon'], [ 'aria-hidden' => 'true' ] );
			$icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		$btn_html = $settings['button_text'] . $icon;

		$btn = $settings['button_text'] ? $this->get_link_control( 'link', $settings['button_link'], $btn_html, 'btn-button' ) : '';

		?>
		<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
			<div class="banner-featured-image"></div>
			<?php echo $links ?>
			<div class="banner-content">
				<div class="banner-text">
					<h2 class="banner-title"><?php echo $settings['title']; ?></h2>

					<div class="banner-desc"><?php echo $settings['desc']; ?></div>
				</div>
				<?php echo $btn ?>
			</div>
		</div>
		<?php
	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected
	function _content_template() {

	}

	/**
	 * Get the link control
	 *
	 * @return string.
	 */
	protected function get_link_control( $link_key, $url, $content, $class_css ) {

		if ( $url['is_external'] ) {
			$this->add_render_attribute( $link_key, 'target', '_blank' );
		}

		if ( $url['nofollow'] ) {
			$this->add_render_attribute( $link_key, 'rel', 'nofollow' );
		}

		$attr = 'span';
		if ( $url['url'] ) {
			$this->add_render_attribute( $link_key, 'href', $url['url'] );
			$attr = 'a';
		}

		return sprintf( '<%1$s class="%4$s" %2$s>%3$s</%1$s>', $attr, $this->get_render_attribute_string( $link_key ), $content, $class_css );
	}
}