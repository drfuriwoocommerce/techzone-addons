<?php

namespace TeckzoneAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Tab List widget
 */
class Tab_List extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'techzone-tab-list';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Teckzone - Tab List', 'teckzone' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-tabs';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'teckzone' ];
	}

	public function get_script_depends() {
		return [
			'techzone-elementor'
		];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->section_content();
		$this->section_style();
	}

	/**
	 * Section Content
	 */
	protected function section_content() {

		$this->start_controls_section(
			'section_content',
			[ 'label' => esc_html__( 'Tab List', 'teckzone' ) ]
		);
		$repeater = new \Elementor\Repeater();

		$repeater->start_controls_tabs(
			'content_wrapper_tabs'
		);

		$repeater->start_controls_tab(
			'content_wrapper_tab',
			[
				'label' => __( 'Tab Content', 'teckzone' ),
			]
		);

		$repeater->add_control(
			'content_title',
			[
				'label'       => esc_html__( 'Title', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'This is title', 'teckzone' ),
				'label_block' => true,
			]
		);

		$repeater -> add_control(
			'desc',
			[
				'label'       => esc_html__( 'Content', 'teckzone' ),
				'type'        => Controls_Manager::WYSIWYG ,
				'default'     => esc_html__( 'Event Note', 'teckzone' ),
				'label_block' => true,
			]
		);

		$repeater->end_controls_tab();

		$repeater->start_controls_tab(
			'content_tab',
			[
				'label' => __( 'Tab Nav', 'teckzone' ),
			]
		);

		$repeater->add_control(
			'icon',
			[
				'label'     => esc_html__( 'Icon', 'martfury' ),
				'type'      => Controls_Manager::ICONS,
				'default'          => [
					'value'   => 'fas fa-star',
					'library' => 'fa-solid',
				],
			]
		);

		$repeater->add_control(
			'nav_title',
			[
				'label'       => esc_html__( 'Title', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'This is title', 'teckzone' ),
				'label_block' => true,
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->add_control(
			'element',
			[
				'label'   => '',
				'type'    => Controls_Manager::REPEATER,
				'fields'  => $repeater->get_controls(),
				'default' => [
					[
						'content_title'         => esc_html__( 'This is title', 'teckzone' ),
						'nav_title'             => esc_html__( 'This is title', 'teckzone' ),
						'desc'                  => esc_html__( 'This is description', 'teckzone' ),
						'icon'                  => 'fa fa-star',
					],
					[
						'content_title'         => esc_html__( 'This is title', 'teckzone' ),
						'nav_title'             => esc_html__( 'This is title', 'teckzone' ),
						'desc'                  => esc_html__( 'This is description', 'teckzone' ),
						'icon'                  => 'fa fa-star',
					],
					[
						'content_title'         => esc_html__( 'This is title', 'teckzone' ),
						'nav_title'             => esc_html__( 'This is title', 'teckzone' ),
						'desc'                  => esc_html__( 'This is description', 'teckzone' ),
						'icon'                  => 'fa fa-star',
					],
					[
						'content_title'         => esc_html__( 'This is title', 'teckzone' ),
						'nav_title'             => esc_html__( 'This is title', 'teckzone' ),
						'desc'                  => esc_html__( 'This is description', 'teckzone' ),
						'icon'                  => 'fa fa-star',
					],
					[
						'content_title'         => esc_html__( 'This is title', 'teckzone' ),
						'nav_title'             => esc_html__( 'This is title', 'teckzone' ),
						'desc'                  => esc_html__( 'This is description', 'teckzone' ),
						'icon'                  => 'fa fa-star',
					],


				],
			]
		);

		$this->end_controls_section();

	}

	/**
	 * Section Style
	 */
	protected function section_style() {
		$this->section_content_style();
		$this->section_nav_style();
	}

	/**
	 * Element in Tab Style
	 *
	 * Title
	 */
	protected function section_content_style() {
		// Content
		$this->start_controls_section(
			'section_content_style',
			[
				'label' => __( 'Content', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'general_padding',
			[
				'label'      => __( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .techzone-tab-list .box-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);

		$this->add_control(
			'content_bk_color',
			[
				'label'        => esc_html__( 'Background Color', 'teckzone' ),
				'type'         => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-tab-list .box-content' => 'background-color: {{VALUE}};',
					'{{WRAPPER}} .techzone-tab-list .box-nav:after' => 'border-top-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'content_border_style',
			[
				'label'     => esc_html__( 'Border Style', 'teckzone' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'dotted' => esc_html__( 'Dotted', 'teckzone' ),
					'dashed' => esc_html__( 'Dashed', 'teckzone' ),
					'solid'  => esc_html__( 'Solid', 'teckzone' ),
					'none'   => esc_html__( 'None', 'teckzone' ),
				],
				'default'   => 'solid',
				'toggle'    => false,
				'selectors' => [
					'{{WRAPPER}} .techzone-tab-list .box-content' => 'border-style: {{VALUE}};',
				],
			]
		);

		$this->add_responsive_control(
			'content_border_width',
			[
				'label'     => __( 'Border Width', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 7,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-tab-list .box-content' => 'border-width: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .techzone-tab-list .box-nav:after' => 'margin-top: -{{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'content_border_color',
			[
				'label'     => __( 'Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-tab-list .box-content' => 'border-color: {{VALUE}};',
					'{{WRAPPER}} .techzone-tab-list .box-nav:before' => 'border-top-color: {{VALUE}};',
				],
			]
		);

		$this->start_controls_tabs(
			'style_tabs'
		);

		// Number
		$this->start_controls_tab(
			'content_number',
			[
				'label' => __( 'Number', 'teckzone' ),
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'number_typography',
				'selector' => '{{WRAPPER}} .techzone-tab-list .number',
			]
		);

		$this->add_control(
			'number_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-tab-list .number' => 'color: {{VALUE}};',
					'{{WRAPPER}} .techzone-tab-list .number:after' => 'background-color: {{VALUE}};',
				],
			]

		);

		$this->end_controls_tab();

		// Title
		$this->start_controls_tab(
			'content_style_title',
			[
				'label' => __( 'Title', 'teckzone' ),
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'title_typography',
				'selector' => '{{WRAPPER}} .techzone-tab-list .box-content .title',
			]
		);

		$this->add_control(
			'title_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-tab-list .box-content .title' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		// Desc
		$this->start_controls_tab(
			'content_desc',
			[
				'label' => __( 'Description', 'teckzone' ),
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'note_typography',
				'selector' => '{{WRAPPER}} .techzone-tab-list .box-note',
			]
		);

		$this->add_responsive_control(
			'note_spacing',
			[
				'label'     => __( 'Margin Top', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 100,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-tab-list .box-note' => 'margin-top: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();

	}

	protected function section_nav_style() {

		$this->start_controls_section(
			'section_nav_style',
			[
				'label' => __( 'Nav', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'box_nav_top',
			[
				'label'     => __( 'Padding Top', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 100,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-tab-list .box-nav' => 'padding-top: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'line_top',
			[
				'label'     => __( 'Line Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 100,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-tab-list .tab__nav:before' => 'top: {{SIZE}}{{UNIT}};',
				],
				'separator'   => 'after',
			]
		);

		$this->start_controls_tabs(
			'style_nav_tabs'
		);

		// Title
		$this->start_controls_tab(
			'nav_title',
			[
				'label' => __( 'Title', 'teckzone' ),
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'nav_title_typography',
				'selector' => '{{WRAPPER}} .techzone-tab-list .box-nav .nav-title',
			]
		);

		$this->add_control(
			'nav_title_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-tab-list .box-nav .nav-title' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'nav_ac_title_color',
			[
				'label'     => __( 'Active Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-tab-list .box-nav.active .nav-title' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		// Icon
		$this->start_controls_tab(
			'nav_icon',
			[
				'label' => __( 'Icon', 'teckzone' ),
			]
		);

		$this->add_responsive_control(
			'icon_size',
			[
				'label'     => __( 'Font Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 100,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-tab-list .teckzone-icon' => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .techzone-tab-list  .teckzone-icon svg' => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}};',

				],
			]
		);

		$this->add_control(
			'icon_radius',
			[
				'label'     => __( 'Border Radius', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'%' => [
						'max' => 50,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-tab-list .teckzone-icon' => 'border-radius: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'icon_height',
			[
				'label'     => __( 'Height', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 100,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-tab-list .teckzone-icon' => 'height: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'icon_line_height',
			[
				'label'     => __( 'Line Height', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 150,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-tab-list .teckzone-icon' => 'line-height: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'icon_width',
			[
				'label'     => __( 'Width', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 100,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-tab-list .teckzone-icon' => 'width: {{SIZE}}{{UNIT}};',
				],
			]
		);


		$this->add_control(
			'icon_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-tab-list .teckzone-icon' => 'color: {{VALUE}};',
					'{{WRAPPER}} .techzone-tab-list .teckzone-icon svg' => 'fill: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'icon_bk_color',
			[
				'label'     => __( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-tab-list .teckzone-icon' => 'background-color: {{VALUE}};',
				],
			]

		);

		$this->add_control(
			'ac_icon_color',
			[
				'label'     => __( 'Active Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-tab-list .box-nav.active .field-icon i' => 'color: {{VALUE}};',
				],
			]

		);

		$this->add_control(
			'ac_icon_bk_color',
			[
				'label'     => __( 'Actice Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-tab-list .box-nav.active .field-icon' => 'background-color: {{VALUE}};',
				],
			]

		);

		$this->end_controls_tab();

		// Line
		$this->start_controls_tab(
			'nav_line',
			[
				'label' => __( 'Line', 'teckzone' ),
			]
		);

		$this->add_control(
			'line_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-tab-list .tab__nav:before' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_responsive_control(
			'line_height',
			[
				'label'     => __( 'Height', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 10,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-tab-list .tab__nav:before' => 'height: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();

	}

	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$classes = [
			'teckzone-tab-list',
			'techzone-tab-list',
		];

		$this->add_render_attribute( 'wrapper', 'class', $classes );

		$output = $output_list = $output_events = $note = array();
		$i =0;

		foreach (  $settings["element"]  as $index => $item ) {
			$i ++;
			$num = '0'.$i;
			$nav_title     = $item["nav_title"] ? '<div class="nav-title">'. $item["nav_title"] . '</div>' : '';
			$title     = $item["content_title"] ? '<h5 class="title"><span class="number">'.$num.'</span>' . $item["content_title"] . '</h5>' : '';
			$desc = $item['desc'] ? sprintf( '<div class="box-note">%s</div>', $item["desc"]  ) : '';
			$icon = '';

			if ( $item["nav_title"] ) {
				if ( $item['icon'] && \Elementor\Icons_Manager::is_migration_allowed() ) {
					ob_start();
					\Elementor\Icons_Manager::render_icon( $item['icon'], [ 'aria-hidden' => 'true' ] );
					$icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
				}

				$output_list[] = sprintf( '<li class="box-nav">%s %s</li>' ,$icon,$nav_title );
			}

			$output_events[] = $title == '' && $desc == '' ? '' : sprintf( '<div class="box-content">%s %s</div>' ,$title, $desc  );

		}

		if ( $output_events ) {
			$output[] = sprintf( '<div class="tab__content">%s</div>',implode( '', $output_events)  );
		}

		if ( $output_list ) {
			$output[] = sprintf( '<ul class="tab__nav">%s</ul>', implode( '', $output_list) );
		}


		echo sprintf(
			'<div %s>%s</div>',
			$this->get_render_attribute_string( 'wrapper' ),
			implode( '', $output )
		);
	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected
	function _content_template() {
	}
}