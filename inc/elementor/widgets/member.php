<?php

namespace TeckzoneAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;
use Elementor\Group_Control_Image_Size;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Icon Box widget
 */
class Member extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'techzone-member';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Teckzone - Member', 'teckzone' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-posts-grid';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'teckzone' ];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->section_content();
		$this->section_style();
	}

	/**
	 * Section Content
	 */
	protected function section_content() {
		$this->start_controls_section(
			'section_content',
			[ 'label' => esc_html__( 'Content', 'teckzone' ) ]
		);

		$this->add_control(
			'image', [
				'label'   => esc_html__( 'Choose Image', 'teckzone' ),
				'type'    => Controls_Manager::MEDIA,
				'default' => [
					'url' => 'https://via.placeholder.com/270/f8f8f8?text=270x270+Image',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name'      => 'image',
				// Usage: `{name}_size` and `{name}_custom_dimension`, in this case `image_size` and `image_custom_dimension`.
				'default'   => 'large',
				'separator' => 'none',
			]
		);

		$this->add_control(
			'name',
			[
				'label'       => esc_html__( 'Name', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'Member\'s Name', 'teckzone' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'member_url', [
				'label'         => esc_html__( 'Member URL', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);

		$this->add_control(
			'job',
			[
				'label'       => esc_html__( 'Job', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'Member\'s Job', 'teckzone' ),
				'label_block' => true,
			]
		);

		$repeater = new \Elementor\Repeater();
		$repeater->add_control(
			'icon',
			[
				'label'   => esc_html__( 'Icon', 'teckzone' ),
				'type'    => Controls_Manager::ICONS,
				'default'          => [
					'value'   => 'fas fa-star',
					'library' => 'fa-solid',
				],
			]
		);
		$repeater->add_control(
			'link', [
				'label'         => esc_html__( 'Link', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);
		$this->add_control(
			'socials',
			[
				'label'         => esc_html__( 'Socials', 'teckzone' ),
				'type'          => Controls_Manager::REPEATER,
				'fields'        => $repeater->get_controls(),
				'default'       => [
					[
						'icon' => 'fa fa-twitter',
						'link'       => [
							'url'         => '#',
							'is_external' => false,
							'nofollow'    => false,
						],
					],
					[
						'icon' => 'fa fa-facebook',
						'link'       => [
							'url'         => '#',
							'is_external' => false,
							'nofollow'    => false,
						],
					],
					[
						'icon' => 'fa fa-linkedin-square',
						'link'       => [
							'url'         => '#',
							'is_external' => false,
							'nofollow'    => false,
						],
					]
				],
				'prevent_empty' => false
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Section Style
	 */
	protected function section_style() {
		$this->section_avatar_style();
		$this->section_socials_style();
		$this->section_title_style();
		$this->section_job_style();
	}

	/**
	 * Element in Tab Style
	 *
	 * Avatar
	 */
	protected function section_avatar_style(){
		$this->start_controls_section(
			'section_avatar_style',
			[
				'label' => __( 'Avatar', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'avatar_spacing',
			[
				'label'     => __( 'Bottom Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 50,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-member .member-avatar' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'avatar_background_color',
			[
				'label'     => __( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .techzone-member .member-avatar .member-socials' => 'background-color: {{VALUE}};',
				],
				'default'   => '',
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Element in Tab Style
	 *
	 * Socials
	 */
	protected function section_socials_style() {
		$this->start_controls_section(
			'section_socials_style',
			[
				'label' => __( 'Socials', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'socials_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-member .teckzone-icon' => 'color: {{VALUE}};',
					'{{WRAPPER}} .techzone-member .teckzone-icon svg' => 'fill: {{VALUE}};',
				],
			]
		);
		$this->add_responsive_control(
			'socials_font_size',
			[
				'label'     => __( 'Font Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 30,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-member .teckzone-icon' => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .techzone-member .teckzone-icon svg' => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'socials_padding',
			[
				'label'      => __( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .techzone-member .member-socials a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'socials_margin',
			[
				'label'      => __( 'Margin', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .techzone-member .member-socials a' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Element in Tab Style
	 *
	 * Title
	 */
	protected function section_title_style() {
		$this->start_controls_section(
			'section_title_style',
			[
				'label' => __( 'Title', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'title_typography',
				'selector' => '{{WRAPPER}} .techzone-member h4.name',
			]
		);
		$this->start_controls_tabs( 'tabs_title_style' );
		$this->start_controls_tab(
			'title_normal',
			[
				'label' => __( 'Normal', 'martfury' ),
			]
		);
		$this->add_control(
			'title_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-member h4.name a' => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_tab();

		$this->start_controls_tab(
			'title_hover',
			[
				'label' => __( 'Hover', 'martfury' ),
			]
		);
		$this->add_control(
			'title_hover_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-member h4.name a:hover' => 'color: {{VALUE}};',
					'{{WRAPPER}} .techzone-member h4.name a:focus' => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_tab();

		$this->end_controls_tabs();
		$this->end_controls_section();
	}

	/**
	 * Element in Tab Style
	 *
	 * Job
	 */
	protected function section_job_style() {
		$this->start_controls_section(
			'section_job_style',
			[
				'label' => __( 'Job', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'job_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-member .job' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'job_typography',
				'selector' => '{{WRAPPER}} .techzone-member .job',
			]
		);
		$this->add_responsive_control(
			'job_spacing',
			[
				'label'     => __( 'Top Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 30,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-member .job' => 'margin-top: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$classes = [
			'teckzone-member',
			'techzone-member',
		];

		$this->add_render_attribute( 'wrapper', 'class', $classes );

		$output = [];

		if ( $settings['name'] ) {
			$output[] = sprintf( '<h4 class="name">%s</h4>', $this->get_link_control( 'member_url', $settings['member_url'], $settings['name'], '' ) );
		}

		if ( $settings['job'] ) {
			$output[] = sprintf( '<div class="job">%s</div>', $settings['job'] );
		}

		$socials = $settings['socials'];

		$socials_html = [];

		if ( ! empty ( $socials ) ) {
			$socials_html[] = '<div class="member-socials">';
			foreach ( $socials as $index => $item ) {
				$link_key = 'link_' . $index;
				$icon     = '';
				if ( $item['icon'] ) {
					if ( $item['icon'] && \Elementor\Icons_Manager::is_migration_allowed() ) {
						ob_start();
						\Elementor\Icons_Manager::render_icon( $item['icon'], [ 'aria-hidden' => 'true' ] );
						$icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
					}
				}

				$link = $this->get_link_control( $link_key, $item['link'], $icon, 'link' );

				$socials_html[] = sprintf( '%s', $link );
			}
			$socials_html[] = '</div>';
		}

		$image = '';
		if ( $settings['image'] ) {
			$image = Group_Control_Image_Size::get_attachment_image_html( $settings );
		}

		echo sprintf(
			'<div %s><div class="member-avatar">%s%s</div><div class="member-content">%s</div></div>',
			$this->get_render_attribute_string( 'wrapper' ),
			$image,
			implode( '', $socials_html ),
			implode( '', $output )
		);
	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected function _content_template() {
	}

	/**
	 * Get the link control
	 *
	 * @return string.
	 */
	protected function get_link_control( $link_key, $url, $content, $class_css ) {

		if ( $url['is_external'] ) {
			$this->add_render_attribute( $link_key, 'target', '_blank' );
		}

		if ( $url['nofollow'] ) {
			$this->add_render_attribute( $link_key, 'rel', 'nofollow' );
		}

		$attr = 'span';
		if ( $url['url'] ) {
			$this->add_render_attribute( $link_key, 'href', $url['url'] );
			$attr = 'a';
		}

		return sprintf( '<%1$s class="%4$s" %2$s>%3$s</%1$s>', $attr, $this->get_render_attribute_string( $link_key ), $content, $class_css );
	}
}