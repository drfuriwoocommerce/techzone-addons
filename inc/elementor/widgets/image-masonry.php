<?php

namespace TeckzoneAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Image_Size;
use Elementor\Utils;
use Elementor\Widget_Base;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Icon Box widget
 */
class Image_Masonry extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'techzone-image-masonry';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Teckzone - Image Masonry', 'teckzone' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-gallery-masonry';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'teckzone' ];
	}

	public function get_script_depends() {
		return [
			'imagesloaded',
			'isotope',
			'techzone-elementor'
		];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->section_content();
		$this->section_style();
	}

	/**
	 * Section Content
	 */
	protected function section_content() {
		$this->section_images_content();
		$this->section_lightbox_content();
	}

	/**
	 * Section Content
	 *
	 * Images
	 */
	protected function section_images_content() {
		$this->start_controls_section(
			'section_content',
			[ 'label' => esc_html__( 'Content', 'teckzone' ) ]
		);

		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'image', [
				'label'   => esc_html__( 'Choose Image', 'teckzone' ),
				'type'    => Controls_Manager::MEDIA,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
			]
		);
		$repeater->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name'      => 'image', // Usage: `{name}_size` and `{name}_custom_dimension`, in this case `image_size` and `image_custom_dimension`.
				'default'   => 'full',
				'separator' => 'none',
			]
		);
		$repeater->add_control(
			'link', [
				'label'         => esc_html__( 'Link', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'description'   => esc_html__( 'Just works if the value of Lightbox is No', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);
		$this->add_control(
			'images',
			[
				'label'         => esc_html__( 'Images', 'teckzone' ),
				'type'          => Controls_Manager::REPEATER,
				'fields'        => $repeater->get_controls(),
				'default'       => [
					[
						'image' => [
							'url' => 'https://via.placeholder.com/383x470?text=383x470'
						],
						'link'  => [
							'url'         => '#',
							'is_external' => false,
							'nofollow'    => false,
						],
					],
					[
						'image' => [
							'url' => 'https://via.placeholder.com/383x220?text=383x220'
						],
						'link'  => [
							'url'         => '#',
							'is_external' => false,
							'nofollow'    => false,
						],
					],
					[
						'image' => [
							'url' => 'https://via.placeholder.com/383x368?text=383x368'
						],
						'link'  => [
							'url'         => '#',
							'is_external' => false,
							'nofollow'    => false,
						],
					],
					[
						'image' => [
							'url' => 'https://via.placeholder.com/383x470?text=383x470'
						],
						'link'  => [
							'url'         => '#',
							'is_external' => false,
							'nofollow'    => false,
						],
					],
					[
						'image' => [
							'url' => 'https://via.placeholder.com/383x322?text=383x322'
						],
						'link'  => [
							'url'         => '#',
							'is_external' => false,
							'nofollow'    => false,
						],
					],
					[
						'image' => [
							'url' => 'https://via.placeholder.com/383x220?text=383x220'
						],
						'link'  => [
							'url'         => '#',
							'is_external' => false,
							'nofollow'    => false,
						],
					]
				],
				'prevent_empty' => false
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Section Content
	 *
	 * Light Box
	 */
	protected function section_lightbox_content() {
		$this->start_controls_section(
			'section_lightbox',
			[ 'label' => esc_html__( 'Light Box', 'teckzone' ) ]
		);
		$this->add_control(
			'open_lightbox',
			[
				'label'   => esc_html__( 'Light Box', 'teckzone' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'no',
				'options' => [
					'yes' => esc_html__( 'Yes', 'teckzone' ),
					'no'  => esc_html__( 'No', 'teckzone' ),
				],
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Section Style
	 */
	protected function section_style() {
		$this->section_item_style();
	}

	/**
	 * Element in Tab Style
	 *
	 * Item
	 */
	protected function section_item_style() {
		$this->start_controls_section(
			'section_item_style',
			[
				'label' => __( 'Item', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'columns',
			[
				'label'        => esc_html__( 'Columns', 'teckzone' ),
				'type'         => Controls_Manager::SELECT,
				'options'      => [
					'2' => esc_html__( '2 Columns', 'teckzone' ),
					'3' => esc_html__( '3 Columns', 'teckzone' ),
					'4' => esc_html__( '4 Columns', 'teckzone' ),
					'5' => esc_html__( '5 Columns', 'teckzone' ),
					'6' => esc_html__( '6 Columns', 'teckzone' ),
				],
				'default'      => '3',
				'toggle'       => false,
				'prefix_class' => 'columns-%s',
			]
		);
		$this->add_responsive_control(
			'gaps',
			[
				'label'      => esc_html__( 'Gaps', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min'  => 0,
						'max'  => 100,
						'step' => 2,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-image-masonry .gallery-wrapper'    => 'margin: calc({{SIZE}}{{UNIT}}/2 * -1);',
					'{{WRAPPER}} .teckzone-image-masonry .image-masonry-item' => 'padding: calc({{SIZE}}{{UNIT}}/2);',
				],
			]
		);
		$this->end_controls_section();
	}


	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$classes = [
			'teckzone-image-masonry',
			'techzone-image-masonry',
		];

		$this->add_render_attribute( 'wrapper', 'class', $classes );

		$images = $settings['images'];

		$output   = [ ];
		$output[] = '<div class="grid-sizer"></div>';

		if ( ! empty ( $images ) ) {

			$item_class = [ 'image-masonry-item' ];

			$this->add_render_attribute( 'gallery-wrapper', 'class', $item_class );

			foreach ( $images as $index => $item ) {
				$image = Group_Control_Image_Size::get_attachment_image_html( $item );

				$key = 'image_' . $index;

				$this->add_render_attribute(
					$key, [
						'href'                              => $settings['open_lightbox'] == 'yes' ? $item['image']['url'] : $item['link']['url'],
						'data-elementor-open-lightbox'      => $settings['open_lightbox'],
						'data-elementor-lightbox-slideshow' => $this->get_id(),
						'data-elementor-lightbox-index'     => $index,
					]
				);

				$image = '<a ' . $this->get_render_attribute_string( $key ) . '>' . $image . '</a>';

				$output[] = sprintf(
					'<div %s>%s</div>',
					$this->get_render_attribute_string( 'gallery-wrapper' ),
					$image
				);
			}
		}

		echo sprintf(
			'<div %s><div class="gallery-wrapper">%s</div></div>',
			$this->get_render_attribute_string( 'wrapper' ),
			implode( '', $output )
		);
	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected
	function _content_template() {
	}
}