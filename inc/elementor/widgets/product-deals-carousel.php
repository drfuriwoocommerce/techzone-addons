<?php

namespace TeckzoneAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;
use TeckzoneAddons\Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Icon Box widget
 */
class Product_Deals_Carousel extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'techzone-product-deals-carousel';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Product Deals Carousel', 'teckzone' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-post-slider';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'teckzone' ];
	}

	public function get_script_depends() {
		return [
			'techzone-elementor'
		];
	}


	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {

		// Content
		$this->register_heading_settings_controls();
		$this->register_product_settings_controls();
		$this->register_carousel_settings_controls();

		// Style
		$this->register_heading_style_controls();
		$this->register_products_style_controls();
		$this->register_carousel_style_controls();
	}

	/**
	 * Register the widget content controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function register_heading_settings_controls() {
		$this->start_controls_section(
			'section_heading',
			[ 'label' => esc_html__( 'Heading', 'teckzone' ) ]
		);

		$this->start_controls_tabs( 'deals_heading_settings' );

		// Title
		$this->start_controls_tab( 'tab_title', [ 'label' => esc_html__( 'Title', 'teckzone' ) ] );
		$this->add_control(
			'title',
			[
				'label'       => esc_html__( 'Title', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'Today Flash Sale', 'teckzone' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'icon',
			[
				'label'   => esc_html__( 'Icon', 'teckzone' ),
				'type'    => Controls_Manager::ICONS,
				'default' => [
					'value'   => '',
					'library' => 'fa-solid',
				],
			]
		);

		$this->end_controls_tab();

		// Countdown
		$this->start_controls_tab( 'countdown', [ 'label' => esc_html__( 'Countdown', 'teckzone' ) ] );

		$this->add_control(
			'ends_in',
			[
				'label'       => esc_html__( 'Ends In Text', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'Ending in', 'teckzone' ),
				'label_block' => true,
			]
		);

		$this->end_controls_tab();

		// Description
		$this->start_controls_tab( 'description', [ 'label' => esc_html__( 'Description', 'teckzone' ) ] );

		$this->add_control(
			'short_desc',
			[
				'label'       => esc_html__( 'Short Description', 'teckzone' ),
				'type'        => Controls_Manager::TEXTAREA,
				'label_block' => true,
			]
		);

		$this->end_controls_tab();

		// View all
		$this->start_controls_tab( 'view_all', [ 'label' => esc_html__( 'View All', 'teckzone' ) ] );

		$this->add_control(
			'view_all_text',
			[
				'label'   => esc_html__( 'View All Text', 'teckzone' ),
				'type'    => Controls_Manager::TEXT,
				'default' => esc_html__( 'See all deals', 'teckzone' ),
			]
		);

		$this->add_control(
			'view_all_link', [
				'label'         => esc_html__( 'View All Link', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);

		$this->add_control(
			'view_all_icon',
			[
				'label'   => esc_html__( 'Icon', 'teckzone' ),
				'type'    => Controls_Manager::ICONS,
				'default' => [
					'value'   => 'icon-chevron-right',
					'library' => 'linearicons',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function register_product_settings_controls() {
		$this->start_controls_section(
			'section_product',
			[ 'label' => esc_html__( 'Product', 'teckzone' ) ]
		);

		$this->add_control(
			'product_type',
			[
				'label'   => esc_html__( 'Products', 'teckzone' ),
				'type'    => Controls_Manager::SELECT,
				'options' => [
					'day'   => esc_html__( 'Deals of the day', 'teckzone' ),
					'week'  => esc_html__( 'Deals of the week', 'teckzone' ),
					'month' => esc_html__( 'Deals of the month', 'teckzone' ),
					'sale'  => esc_html__( 'On Sale', 'teckzone' ),
					'deals' => esc_html__( 'Product Deals', 'teckzone' ),
				],
				'default' => 'deals',
				'toggle'  => false,
			]
		);

		$this->add_control(
			'product_cats',
			[
				'label'       => esc_html__( 'Product Categories', 'teckzone' ),
				'type'        => Controls_Manager::SELECT2,
				'options'     => Elementor::get_taxonomy(),
				'default'     => '',
				'multiple'    => true,
				'label_block' => true,
			]
		);
		$this->add_control(
			'per_page',
			[
				'label'   => esc_html__( 'Products per view', 'teckzone' ),
				'type'    => Controls_Manager::NUMBER,
				'default' => 12,
				'min'     => 2,
				'max'     => 50,
				'step'    => 1,
			]
		);

		$this->add_control(
			'orderby',
			[
				'label'   => esc_html__( 'Order By', 'teckzone' ),
				'type'    => Controls_Manager::SELECT,
				'options' => [
					''           => esc_html__( 'Default', 'teckzone' ),
					'date'       => esc_html__( 'Date', 'teckzone' ),
					'title'      => esc_html__( 'Title', 'teckzone' ),
					'menu_order' => esc_html__( 'Menu Order', 'teckzone' ),
					'rand'       => esc_html__( 'Random', 'teckzone' ),
				],
				'default' => '',
				'toggle'  => false,
			]
		);

		$this->add_control(
			'order',
			[
				'label'   => esc_html__( 'Order', 'teckzone' ),
				'type'    => Controls_Manager::SELECT,
				'options' => [
					''     => esc_html__( 'Default', 'teckzone' ),
					'asc'  => esc_html__( 'Ascending', 'teckzone' ),
					'desc' => esc_html__( 'Descending', 'teckzone' ),
				],
				'default' => '',
				'toggle'  => false,
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Register the widget carousel settings controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function register_carousel_settings_controls() {

		$this->start_controls_section(
			'section_carousel_settings',
			[
				'label' => esc_html__( 'Carousel Settings', 'teckzone' ),
			]
		);

		$this->add_responsive_control(
			'slidesToShow',
			[
				'label'   => esc_html__( 'Slides to show', 'teckzone' ),
				'type'    => Controls_Manager::NUMBER,
				'min'     => 1,
				'max'     => 10,
				'default' => 5,
			]
		);
		$this->add_responsive_control(
			'slidesToScroll',
			[
				'label'   => esc_html__( 'Slides to scroll', 'teckzone' ),
				'type'    => Controls_Manager::NUMBER,
				'min'     => 1,
				'max'     => 10,
				'default' => 1,
			]
		);
		$this->add_responsive_control(
			'navigation',
			[
				'label'   => esc_html__( 'Navigation', 'teckzone' ),
				'type'    => Controls_Manager::SELECT,
				'options' => [
					'both'   => esc_html__( 'Arrows and Dots', 'teckzone' ),
					'arrows' => esc_html__( 'Arrows', 'teckzone' ),
					'dots'   => esc_html__( 'Dots', 'teckzone' ),
					'none'   => esc_html__( 'None', 'teckzone' ),
				],
				'default' => 'arrows',
				'toggle'  => false,
			]
		);

		$this->add_control(
			'infinite',
			[
				'label'     => __( 'Infinite Loop', 'teckzone' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_off' => __( 'Off', 'teckzone' ),
				'label_on'  => __( 'On', 'teckzone' ),
				'default'   => 'yes'
			]
		);

		$this->add_control(
			'autoplay',
			[
				'label'     => __( 'Autoplay', 'teckzone' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_off' => __( 'Off', 'teckzone' ),
				'label_on'  => __( 'On', 'teckzone' ),
				'default'   => 'yes'
			]
		);

		$this->add_control(
			'autoplay_speed',
			[
				'label'   => __( 'Autoplay Speed (in ms)', 'teckzone' ),
				'type'    => Controls_Manager::NUMBER,
				'default' => 3000,
				'min'     => 100,
				'step'    => 100,
			]
		);

		$this->add_control(
			'speed',
			[
				'label'       => __( 'Speed', 'teckzone' ),
				'type'        => Controls_Manager::NUMBER,
				'default'     => 800,
				'min'         => 100,
				'step'        => 100,
				'description' => esc_html__( 'Slide animation speed (in ms)', 'teckzone' ),
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Register the widget heading style controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function register_heading_style_controls() {
		$this->start_controls_section(
			'section_style_heading',
			[
				'label' => esc_html__( 'Heading', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'heading_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-deals-day .cat-header' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'heading_background_color',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-day .cat-header' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'     => 'heading_border',
				'label'    => __( 'Border', 'teckzone' ),
				'selector' => '{{WRAPPER}} .tz-product-deals-day .cat-header',
			]
		);

		$this->add_control(
			'heading_style_divider',
			[
				'label' => '',
				'type'  => Controls_Manager::DIVIDER,
			]
		);

		$this->start_controls_tabs( 'heading_title_style_settings' );

		$this->start_controls_tab( 'heading_title_style_tab', [ 'label' => esc_html__( 'Title', 'teckzone' ) ] );

		$this->add_responsive_control(
			'heading_title_margin',
			[
				'label'     => esc_html__( 'Margin', 'teckzone' ),
				'type'      => Controls_Manager::DIMENSIONS,
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-day .header-content .cat-title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'heading_title_color',
			[
				'label'     => esc_html__( 'Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-day .header-content .cat-title' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'heading_title_typography',
				'selector' => '{{WRAPPER}} .tz-product-deals-day .header-content .cat-title',
			]
		);
		$this->add_control(
			'title_icon_style',
			[
				'label'        => __( 'Icon', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);
		$this->start_popover();

		$this->add_control(
			'icon_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}}  .tz-product-deals-day .cat-header .cat-title .teckzone-icon' => 'color: {{VALUE}}',
				],
			]
		);
		$this->add_responsive_control(
			'icon_font_size',
			[
				'label'      => esc_html__( 'Font size', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-deals-day .cat-header .cat-title .teckzone-icon' => 'font-size: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'icon_right_spacing',
			[
				'label'      => esc_html__( 'Right Spacing', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-deals-day .cat-header .cat-title .teckzone-icon' => 'margin-right: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->end_popover();

		$this->end_controls_tab();

		// Countdown
		$this->start_controls_tab( 'heading_countdown_style_tab', [ 'label' => esc_html__( 'Countdown', 'teckzone' ) ] );

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'countdown_typography',
				'label'    => esc_html__( 'Text Typography', 'teckzone' ),
				'selector' => '{{WRAPPER}} .tz-product-deals-day .cat-header .teckzone-countdown',
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'countdown_number_typography',
				'label'    => esc_html__( 'Number Typography', 'teckzone' ),
				'selector' => '{{WRAPPER}} .tz-product-deals-day .cat-header .teckzone-countdown .digits',
			]
		);

		$this->add_control(
			'countdown_options',
			[
				'label'        => __( 'More Options', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);
		$this->start_popover();
		$this->add_responsive_control(
			'countdown_padding',
			[
				'label'     => esc_html__( 'Padding', 'teckzone' ),
				'type'      => Controls_Manager::DIMENSIONS,
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-day .cat-header .header-countdown' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'countdown_left_spacing',
			[
				'label'      => esc_html__( 'Left Spacing', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 200,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-deals-day .cat-header .countdown-wrapper' => 'margin-left: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'countdown_background_color',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-day .cat-header .header-countdown' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'countdown_color',
			[
				'label'     => esc_html__( 'Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-day .cat-header .teckzone-countdown' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'show_countdown_text',
			[
				'label'     => esc_html__( 'Countdown Text', 'teckzone' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'inline-block' => esc_html__( 'Show', 'teckzone' ),
					'none'         => esc_html__( 'Hide', 'teckzone' ),
				],
				'default'   => 'inline-block',
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-day .cat-header .teckzone-countdown .text' => 'display: {{VALUE}}',
				],
			]
		);
		$this->end_popover();

		// End In
		$this->add_control(
			'ends_in_style',
			[
				'label'     => __( 'Ends In', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'ends_text_typography',
				'selector' => '{{WRAPPER}} .tz-product-deals-day .cat-header .ends-text',
			]
		);
		$this->add_control(
			'ends_in_more_options',
			[
				'label'        => __( 'More Options', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();
		$this->add_responsive_control(
			'show_ends_in',
			[
				'label'     => esc_html__( 'Ends In Text', 'teckzone' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'inline-block' => esc_html__( 'Show', 'teckzone' ),
					'none'         => esc_html__( 'Hide', 'teckzone' ),
				],
				'default'   => 'inline-block',
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-day .cat-header .ends-text' => 'display: {{VALUE}}',
				],
			]
		);
		$this->add_control(
			'ends_in_position',
			[
				'label'   => esc_html__( 'Position', 'teckzone' ),
				'type'    => Controls_Manager::SELECT,
				'options' => [
					'outside' => esc_html__( 'Outside', 'teckzone' ),
					'inside'  => esc_html__( 'Inside', 'teckzone' ),
				],
				'default' => 'outside',
				'toggle'  => false,
			]
		);

		$this->add_responsive_control(
			'ends_in_spacing',
			[
				'label'      => esc_html__( 'Spacing', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-deals-day .cat-header .ends-text' => 'margin-right: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->add_control(
			'ends_in_color',
			[
				'label'     => esc_html__( 'Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-day .cat-header .ends-text' => 'color: {{VALUE}}',
				],
			]
		);
		$this->end_popover();
		$this->end_controls_tab();

		// Description
		$this->start_controls_tab( 'heading_short_desc_style_tab', [ 'label' => esc_html__( 'Description', 'teckzone' ) ] );
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'short_desc_typography',
				'selector' => '{{WRAPPER}} .tz-product-deals-day .cat-header .short-desc',
			]
		);
		$this->add_control(
			'desc_style_more_options',
			[
				'label'        => __( 'More Options', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();
		$this->add_responsive_control(
			'show_short_desc',
			[
				'label'     => esc_html__( 'Display', 'teckzone' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'inline-block' => esc_html__( 'Show', 'teckzone' ),
					'none'         => esc_html__( 'Hide', 'teckzone' ),
				],
				'default'   => 'inline-block',
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-day .cat-header .short-desc' => 'display: {{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'short_desc_spacing',
			[
				'label'      => esc_html__( 'Spacing', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-deals-day .cat-header .short-desc' => 'margin-left: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->add_control(
			'short_desc_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-day .cat-header .short-desc' => 'color: {{VALUE}}',
				],
			]
		);
		$this->end_popover();
		$this->end_controls_tab();

		// View All
		$this->start_controls_tab( 'heading_view_all_style_tab', [ 'label' => esc_html__( 'View All', 'teckzone' ) ] );

		$this->add_responsive_control(
			'show_heading_view_all',
			[
				'label'     => esc_html__( 'Display', 'teckzone' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'flex' => esc_html__( 'Show', 'teckzone' ),
					'none' => esc_html__( 'Hide', 'teckzone' ),
				],
				'default'   => 'flex',
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-day .cat-header .header-link' => 'display: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'heading_view_all_typography',
				'selector' => '{{WRAPPER}} .tz-product-deals-day .header-link',
			]
		);

		$this->add_control(
			'heading_view_all_color',
			[
				'label'     => esc_html__( 'Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-day .header-link span' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'heading_view_all_hover_color',
			[
				'label'     => esc_html__( 'Hover Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-day .header-link:hover span'       => 'color: {{VALUE}}',
					'{{WRAPPER}} .tz-product-deals-day .header-link:hover .link-text' => 'box-shadow: inset 0 0 0 transparent, inset 0 -1px 0 {{VALUE}}',
				],
			]
		);

		// Icon
		$this->add_control(
			'header_link_icon_style',
			[
				'label'        => __( 'Icon', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();

		$this->add_responsive_control(
			'header_link_icon_font_size',
			[
				'label'     => __( 'Font Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-day .cat-header .header-link .teckzone-icon'     => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .tz-product-deals-day .cat-header .header-link .teckzone-icon svg' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->add_responsive_control(
			'header_link_icon_spacing',
			[
				'label'     => __( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 20,
						'min' => 0,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-day .cat-header .header-link .teckzone-icon' => 'padding-left: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_popover();

		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	/**
	 * Register the widget heading style controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function register_products_style_controls() {
		$this->start_controls_section(
			'section_style_products',
			[
				'label' => esc_html__( 'Products', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);


		$this->add_responsive_control(
			'products_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-deals-day .products-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'products_style_divider',
			[
				'type' => Controls_Manager::DIVIDER,
			]
		);

		$this->start_controls_tabs( 'more_options_tabs' );

		$this->start_controls_tab(
			'progress_bar_tab',
			[
				'label' => __( 'Progress Bar', 'teckzone' ),
			]
		);

		$this->add_control(
			'show_progress_bar',
			[
				'label'     => esc_html__( 'Progress Bar', 'teckzone' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'block' => esc_html__( 'Show', 'teckzone' ),
					'none'  => esc_html__( 'Hide', 'teckzone' ),
				],
				'default'   => 'block',
				'selectors' => [
					'{{WRAPPER}} ul.products li.product .deal-progress' => 'display: {{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'progress_bar_height',
			[
				'label'      => esc_html__( 'Height', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-deals-day ul.products li.product .deal-progress .progress-bar'   => 'height: {{SIZE}}{{UNIT}}',
					'{{WRAPPER}} .tz-product-deals-day ul.products li.product .deal-progress .progress-value' => 'height: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'progress_bar_bg_color',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-day .deal-progress .progress-bar' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'progress_bar_active_bg_color',
			[
				'label'     => esc_html__( 'Active Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-day .deal-progress .progress-value' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'more_options_tab',
			[
				'label' => __( 'More Options', 'teckzone' ),
			]
		);
		$this->add_responsive_control(
			'action_button',
			[
				'label'       => esc_html__( 'Wishlist/Compare Text', 'teckzone' ),
				'label_block' => true,
				'type'        => Controls_Manager::SELECT,
				'options'     => [
					'inline-block' => esc_html__( 'Show', 'teckzone' ),
					'none'         => esc_html__( 'Hide', 'teckzone' ),
				],
				'default'     => 'inline-block',
				'selectors'   => [
					'{{WRAPPER}} .tz-product-deals-day ul.products li.product .product-button .group a span' => 'display: {{VALUE}}',
				],
			]
		);
		$this->add_responsive_control(
			'swatches',
			[
				'label'       => esc_html__( 'Swatches', 'teckzone' ),
				'label_block' => true,
				'type'        => Controls_Manager::SELECT,
				'options'     => [
					'block' => esc_html__( 'Show', 'teckzone' ),
					'none'  => esc_html__( 'Hide', 'teckzone' ),
				],
				'default'     => 'block',
				'selectors'   => [
					'{{WRAPPER}} .tz-product-deals-day ul.products li.product .product-thumbnail .tz-attr-swatches' => 'display: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	/**
	 * Register the widget carousel style controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function register_carousel_style_controls() {

		$this->start_controls_section(
			'section_style_carousel',
			[
				'label' => esc_html__( 'Carousel Style', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_control(
			'arrows_style_divider',
			[
				'label'     => __( 'Arrows', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		$this->add_control(
			'arrows_style',
			[
				'label'        => __( 'Options', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();

		$this->add_responsive_control(
			'sliders_arrows_size',
			[
				'label'     => __( 'Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 100,
						'min' => 0,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-day .slick-arrow' => 'font-size: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'sliders_arrow_width',
			[
				'label'      => esc_html__( 'Width', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 500,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-deals-day .slick-arrow' => 'width: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'sliders_arrow_height',
			[
				'label'      => esc_html__( 'Height', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 500,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-deals-day .slick-arrow' => 'height: {{SIZE}}{{UNIT}};line-height: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'sliders_arrows_offset',
			[
				'label'     => esc_html__( 'Horizontal Offset', 'teckzone' ),
				'type'      => Controls_Manager::NUMBER,
				'step'      => 1,
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-day .slick-prev-arrow' => 'left: {{VALUE}}px;',
					'{{WRAPPER}} .tz-product-deals-day .slick-next-arrow' => 'right: {{VALUE}}px;',
				],
			]
		);
		$this->end_popover();

		$this->start_controls_tabs( 'sliders_normal_settings' );

		$this->start_controls_tab( 'sliders_normal', [ 'label' => esc_html__( 'Normal', 'teckzone' ) ] );

		$this->add_control(
			'sliders_arrow_background',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-day .slick-prev-arrow, .tz-product-deals-day .slick-next-arrow' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'sliders_arrow_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-day .slick-prev-arrow, .tz-product-deals-day .slick-next-arrow' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'sliders_arrow_border_color',
			[
				'label'     => esc_html__( 'Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-day .slick-prev-arrow, .tz-product-deals-day .slick-next-arrow' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'sliders_hover', [ 'label' => esc_html__( 'Hover', 'teckzone' ) ] );

		$this->add_control(
			'sliders_arrow_hover_background',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-day .slick-prev-arrow:hover, .tz-product-deals-day .slick-next-arrow:hover' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'sliders_arrow_hover_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-day .slick-prev-arrow:hover, .tz-product-deals-day .slick-next-arrow:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'sliders_arrow_hover_border_color',
			[
				'label'     => esc_html__( 'Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-day .slick-prev-arrow:hover, .tz-product-deals-day .slick-next-arrow:hover' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();
		$this->add_control(
			'dots_style_divider',
			[
				'label'     => __( 'Dots', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		$this->add_control(
			'dots_style',
			[
				'label'        => __( 'Options', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);
		$this->start_popover();
		$this->add_responsive_control(
			'sliders_dots_gap',
			[
				'label'     => __( 'Gap', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 50,
						'min' => 0,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-day .slick-dots li' => 'margin: 0 {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'sliders_dots_width',
			[
				'label'      => esc_html__( 'Size', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-deals-day .slick-dots li button'        => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}}',
					'{{WRAPPER}} .tz-product-deals-day .slick-dots li button:before' => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}}',
				],
				'separator'  => 'before',
			]
		);
		$this->add_responsive_control(
			'sliders_dots_offset',
			[
				'label'     => esc_html__( 'Vertical Offset', 'teckzone' ),
				'type'      => Controls_Manager::NUMBER,
				'step'      => 1,
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-day .slick-dots' => 'bottom: {{VALUE}}px;',
				],
			]
		);
		$this->add_control(
			'sliders_dots_background',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-day .slick-dots li button:before' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'sliders_dots_active_background',
			[
				'label'     => esc_html__( 'Active Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-day .slick-dots li.slick-active button:before' => 'background-color: {{VALUE}};',
					'{{WRAPPER}} .tz-product-deals-day .slick-dots li button:hover:before'        => 'background-color: {{VALUE}};',
				],
			]
		);
		$this->end_popover();
		$this->end_controls_section();
	}

	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$this->add_render_attribute(
			'wrapper', 'class', [
				'tz-product-deals-day tz-elementor-product-deals-carousel tz-elementor-product-carousel woocommerce',
				'tz-product-deals--query-' . $settings['product_type']
			]
		);

		$icon = $view_all_icon = $link = '';

		if ( $settings['icon'] && ! empty( $settings['icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['icon'], [ 'aria-hidden' => 'true' ] );
			$icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		if ( $settings['view_all_icon'] && ! empty( $settings['view_all_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['view_all_icon'], [ 'aria-hidden' => 'true' ] );
			$view_all_icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		if ( $settings['view_all_text'] ) {
			$text = '<span class="link-text">' . $settings['view_all_text'] . '</span>' . $view_all_icon;
			$link = $this->get_link_control( 'header_link', $settings['view_all_link'], $text, [ 'class' => 'header-link' ] );
		}

		$is_rtl    = is_rtl();
		$direction = $is_rtl ? 'rtl' : 'ltr';
		$this->add_render_attribute( 'wrapper', 'dir', $direction );

		$now         = strtotime( current_time( 'Y-m-d H:i:s' ) );
		$expire_date = strtotime( '00:00 +1 day' );
		if ( $settings['product_type'] == 'week' ) {
			$expire_date = strtotime( '00:00 next monday' );
		} elseif ( $settings['product_type'] == 'month' ) {
			$expire_date = strtotime( '00:00 first day of next month' );
		}
		$expire = $expire_date - $now;

		$this->add_render_attribute( 'wrapper', 'data-settings', wp_json_encode( Elementor::get_data_slick( $settings ) ) );

		$deals = array(
			'days'    => esc_html__( 'days', 'teckzone' ),
			'hours'   => esc_html__( 'hours', 'teckzone' ),
			'minutes' => esc_html__( 'mins', 'teckzone' ),
			'seconds' => esc_html__( 'secs', 'teckzone' ),
		);

		$settings['columns']    = intval( $settings['slidesToShow'] );
		$settings['pagination'] = '';
		$product_deals          = Elementor::get_product_deals( $settings );

		if ( empty( $product_deals ) ) {
			return;
		}
		?>
		<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
			<div class="cat-header">
				<div class="header-content">
					<h2 class="cat-title"><?php echo $icon . esc_html( $settings['title'] ); ?></h2>

					<div class="countdown-wrapper">
						<?php if ( $settings['ends_in_position'] == 'outside' ) : ?>
							<span class="ends-text"><?php echo esc_html( $settings['ends_in'] ) ?></span>
						<?php endif; ?>
						<div class="header-countdown">
							<?php if ( $settings['ends_in_position'] == 'inside' ) : ?>
								<span class="ends-text"><?php echo esc_html( $settings['ends_in'] ) ?></span>
							<?php endif; ?>

							<div class="teckzone-countdown" data-expire="<?php echo esc_attr( $expire ); ?>" data-text="<?php echo esc_attr( wp_json_encode( $deals ) ) ?>"></div>
						</div>
					</div>
					<div class="short-desc"><?php echo esc_html( $settings['short_desc'] ) ?></div>
				</div>
				<?php echo $link; ?>
			</div>
			<div class="products-content">
				<?php echo $product_deals; ?>
			</div>
		</div>
		<?php

	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected function _content_template() {
	}

	/**
	 * Get the link control
	 *
	 * @return string.
	 */
	protected function get_link_control( $link_key, $url, $content, $attr = [ ] ) {
		$attr_default = [
			'href' => $url['url'] ? $url['url'] : '#',
		];

		if ( $url['is_external'] ) {
			$attr_default['target'] = '_blank';
		}

		if ( $url['nofollow'] ) {
			$attr_default['rel'] = 'nofollow';
		}

		$attr = wp_parse_args( $attr, $attr_default );

		$this->add_render_attribute( $link_key, $attr );

		return sprintf( '<a %1$s>%2$s</a>', $this->get_render_attribute_string( $link_key ), $content );
	}
}