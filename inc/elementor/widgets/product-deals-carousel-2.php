<?php

namespace TeckzoneAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;
use TeckzoneAddons\Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Icon Box widget
 */
class Product_Deals_Carousel_2 extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'techzone-product-deals-carousel-2';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Product Deals Carousel 2', 'teckzone' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-post-slider';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'teckzone' ];
	}

	public function get_script_depends() {
		return [
			'flexslider',
			'imagesLoaded',
			'tawc-deals',
			'techzone-elementor'
		];
	}


	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {

		// Content
		$this->register_content_settings_controls();
		$this->register_carousel_settings_controls();

		// Style
		$this->register_heading_style_controls();
		$this->register_products_style_controls();
		$this->register_products_carousel_style_controls();
	}

	/**
	 * Register the widget content controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function register_content_settings_controls() {

		$this->start_controls_section(
			'section_heading',
			[ 'label' => esc_html__( 'Heading', 'teckzone' ) ]
		);
		$this->add_control(
			'title',
			[
				'label'       => esc_html__( 'Title', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'Deals Of The Day', 'teckzone' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'title_icon',
			[
				'label'   => esc_html__( 'Icon', 'teckzone' ),
				'type'    => Controls_Manager::ICONS,
				'default' => [
					'value'   => '',
					'library' => 'fa-solid',
				],
			]
		);
		$this->end_controls_section();

		$this->start_controls_section(
			'section_products',
			[ 'label' => esc_html__( 'Products', 'teckzone' ) ]
		);
		$this->add_control(
			'product_type',
			[
				'label'   => esc_html__( 'Products', 'teckzone' ),
				'type'    => Controls_Manager::SELECT,
				'options' => [
					'day'   => esc_html__( 'Deals of the day', 'teckzone' ),
					'week'  => esc_html__( 'Deals of the week', 'teckzone' ),
					'month' => esc_html__( 'Deals of the month', 'teckzone' ),
					'sale'  => esc_html__( 'On Sale', 'teckzone' ),
					'deals' => esc_html__( 'Product Deals', 'teckzone' ),
				],
				'default' => 'deals',
				'toggle'  => false,
			]
		);

		$this->add_control(
			'product_cats',
			[
				'label'       => esc_html__( 'Product Categories', 'teckzone' ),
				'type'        => Controls_Manager::SELECT2,
				'options'     => Elementor::get_taxonomy(),
				'default'     => '',
				'multiple'    => true,
				'label_block' => true,
			]
		);
		$this->add_control(
			'per_page',
			[
				'label'   => esc_html__( 'Total products', 'teckzone' ),
				'type'    => Controls_Manager::NUMBER,
				'default' => 4,
				'min'     => 1,
				'max'     => 50,
				'step'    => 1,
			]
		);

		$this->add_control(
			'orderby',
			[
				'label'   => esc_html__( 'Order By', 'teckzone' ),
				'type'    => Controls_Manager::SELECT,
				'options' => [
					''           => esc_html__( 'Default', 'teckzone' ),
					'date'       => esc_html__( 'Date', 'teckzone' ),
					'title'      => esc_html__( 'Title', 'teckzone' ),
					'menu_order' => esc_html__( 'Menu Order', 'teckzone' ),
					'rand'       => esc_html__( 'Random', 'teckzone' ),
				],
				'default' => '',
				'toggle'  => false,
			]
		);

		$this->add_control(
			'order',
			[
				'label'   => esc_html__( 'Order', 'teckzone' ),
				'type'    => Controls_Manager::SELECT,
				'options' => [
					''     => esc_html__( 'Default', 'teckzone' ),
					'asc'  => esc_html__( 'Ascending', 'teckzone' ),
					'desc' => esc_html__( 'Descending', 'teckzone' ),
				],
				'default' => '',
				'toggle'  => false,
			]
		);
		$this->add_control(
			'expire_text',
			[
				'label'     => __( 'Deal Expire Text', 'teckzone' ),
				'type'      => Controls_Manager::TEXTAREA,
				'default'   => '',
				'separator' => 'before',
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Register the widget carousel settings controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function register_carousel_settings_controls() {

		$this->start_controls_section(
			'section_carousel_settings',
			[
				'label' => esc_html__( 'Carousel Settings', 'teckzone' ),
			]
		);

		$this->add_responsive_control(
			'navigation',
			[
				'label'   => esc_html__( 'Navigation', 'teckzone' ),
				'type'    => Controls_Manager::SELECT,
				'options' => [
					'both'   => esc_html__( 'Arrows and Dots', 'teckzone' ),
					'arrows' => esc_html__( 'Arrows', 'teckzone' ),
					'dots'   => esc_html__( 'Dots', 'teckzone' ),
					'none'   => esc_html__( 'None', 'teckzone' ),
				],
				'default' => 'arrows',
				'toggle'  => false,
			]
		);

		$this->add_control(
			'infinite',
			[
				'label'     => __( 'Infinite Loop', 'teckzone' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_off' => __( 'Off', 'teckzone' ),
				'label_on'  => __( 'On', 'teckzone' ),
				'default'   => 'yes'
			]
		);

		$this->add_control(
			'autoplay',
			[
				'label'     => __( 'Autoplay', 'teckzone' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_off' => __( 'Off', 'teckzone' ),
				'label_on'  => __( 'On', 'teckzone' ),
				'default'   => 'yes'
			]
		);

		$this->add_control(
			'autoplay_speed',
			[
				'label'   => __( 'Autoplay Speed (in ms)', 'teckzone' ),
				'type'    => Controls_Manager::NUMBER,
				'default' => 3000,
				'min'     => 100,
				'step'    => 100,
			]
		);

		$this->add_control(
			'speed',
			[
				'label'       => __( 'Speed', 'teckzone' ),
				'type'        => Controls_Manager::NUMBER,
				'default'     => 800,
				'min'         => 100,
				'step'        => 100,
				'description' => esc_html__( 'Slide animation speed (in ms)', 'teckzone' ),
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Register the widget heading style controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function register_heading_style_controls() {
		$this->start_controls_section(
			'section_style_heading',
			[
				'label' => esc_html__( 'Heading', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'heading_general_style',
			[
				'label' => __( 'General', 'teckzone' ),
				'type'  => Controls_Manager::HEADING,
			]
		);
		$this->add_responsive_control(
			'heading_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-header' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'heading_background_color',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-header' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'     => 'border',
				'label'    => __( 'Border', 'teckzone' ),
				'selector' => '{{WRAPPER}} .tz-product-deals-carousel-2 .product-header',
			]
		);

		// Title
		$this->add_control(
			'heading_title_style',
			[
				'label'     => __( 'Title', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->add_control(
			'heading_title_color',
			[
				'label'     => esc_html__( 'Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-header__title' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'heading_title_typography',
				'selector' => '{{WRAPPER}} .tz-product-deals-carousel-2 .product-header__title',
			]
		);

		$this->add_control(
			'title_icon_style',
			[
				'label'        => __( 'Icon', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);
		$this->start_popover();

		$this->add_control(
			'title_icon_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}}  .tz-product-deals-carousel-2 .product-header .product-header__title .teckzone-icon' => 'color: {{VALUE}}',
				],
			]
		);
		$this->add_responsive_control(
			'title_icon_font_size',
			[
				'label'      => esc_html__( 'Font size', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-header .product-header__title .teckzone-icon' => 'font-size: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'title_icon_right_spacing',
			[
				'label'      => esc_html__( 'Right Spacing', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-header .product-header__title .teckzone-icon' => 'margin-right: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->end_popover();


		// Arrows
		$this->add_control(
			'heading_arrows_style',
			[
				'label'     => __( 'Arrows', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'heading_arrows_typography',
				'selector' => '{{WRAPPER}} .tz-product-deals-carousel-2 .product-header .slick-arrow',
			]
		);
		$this->add_control(
			'arrows_more_options',
			[
				'label'        => __( 'More Options', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();

		$this->add_control(
			'sliders_arrow_width',
			[
				'label'      => esc_html__( 'Arrow Width', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .slick-prev-arrow, .tz-product-deals-carousel-2 .slick-next-arrow' => 'width: {{SIZE}}{{UNIT}}',
				],
				'separator'  => 'before',
			]
		);

		$this->add_control(
			'sliders_arrow_height',
			[
				'label'      => esc_html__( 'Arrow Height', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .slick-prev-arrow, .tz-product-deals-carousel-2 .slick-next-arrow' => 'height: {{SIZE}}{{UNIT}};line-height: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'sliders_arrow_background',
			[
				'label'     => esc_html__( 'Arrow Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .slick-prev-arrow, .tz-product-deals-carousel-2 .slick-next-arrow' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'sliders_arrow_color',
			[
				'label'     => esc_html__( 'Arrow Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-header .slick-arrows span' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'sliders_arrow_border_color',
			[
				'label'     => esc_html__( 'Arrow Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .slick-prev-arrow, .tz-product-deals-carousel-2 .slick-next-arrow' => 'border-color: {{VALUE}};',
				],
			]
		);

		$this->end_popover();

		$this->end_controls_section();
	}

	/**
	 * Register the widget heading style controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function register_products_style_controls() {
		$this->start_controls_section(
			'section_style_products',
			[
				'label' => esc_html__( 'Products', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		// Content
		$this->add_control(
			'content_style',
			[
				'label' => __( 'Content', 'teckzone' ),
				'type'  => Controls_Manager::HEADING,
			]
		);

		$this->add_responsive_control(
			'content_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'ribbons',
			[
				'label'     => esc_html__( 'Ribbons', 'teckzone' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'block' => esc_html__( 'Show', 'teckzone' ),
					'none'  => esc_html__( 'Hide', 'teckzone' ),
				],
				'default'   => 'block',
				'selectors' => [
					'{{WRAPPER}} .woocommerce div.product .woocommerce-product-gallery .ribbons' => 'display: {{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'content_image_width',
			[
				'label'      => esc_html__( 'Images Section Width', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'default'    => [ ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 1000,
					],
					'%'  => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-content div.product .woocommerce-product-gallery' => 'max-width: {{SIZE}}{{UNIT}}; flex: 0 0 {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'content_summary_padding_left',
			[
				'label'      => esc_html__( 'Summary Padding Left', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'default'    => [ ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 300,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-content div.product div.entry-summary' => 'padding-left: {{SIZE}}{{UNIT}}',
				],
			]
		);

		// Progress Bar
		$this->add_control(
			'progress_bar_style',
			[
				'label'     => __( 'Progress Bar', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'countdown_progress_bar',
				'selector' => '{{WRAPPER}} .tz-product-deals-carousel-2 .product-content div.product .tawc-deal .deal-text',
			]
		);
		$this->add_control(
			'progress_bar_more_options',
			[
				'label'        => __( 'More Options', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();
		$this->add_control(
			'show_progress_bar',
			[
				'label'     => esc_html__( 'Show', 'teckzone' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'block' => esc_html__( 'Show', 'teckzone' ),
					'none'  => esc_html__( 'Hide', 'teckzone' ),
				],
				'default'   => 'block',
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .deal-sold' => 'display: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'progress_bar_height',
			[
				'label'      => esc_html__( 'Height', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'default'    => [ ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-content div.product .tawc-deal .deal-progress .progress-bar'   => 'height: {{SIZE}}{{UNIT}}',
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-content div.product .tawc-deal .deal-progress .progress-value' => 'height: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'progress_bar_bg_color',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-content div.product .tawc-deal .deal-progress .progress-bar' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'progress_bar_active_bg_color',
			[
				'label'     => esc_html__( 'Active Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-content div.product .tawc-deal .deal-progress .progress-value' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'progress_bar_border_width',
			[
				'label'      => esc_html__( 'Border Width', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'default'    => [ ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 10,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-content div.product .tawc-deal .deal-sold' => 'border-bottom-width: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'progress_bar_border_color',
			[
				'label'     => esc_html__( 'Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-content div.product .tawc-deal .deal-sold' => 'border-bottom-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'progress_bar_padding_bottom',
			[
				'label'      => esc_html__( 'Padding Bottom', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'default'    => [ ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-content div.product .tawc-deal .deal-sold' => 'padding-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'progress_bar_margin_bottom',
			[
				'label'      => esc_html__( 'Margin Bottom', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'default'    => [ ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-content div.product .tawc-deal .deal-sold' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->end_popover();

		$this->add_control(
			'countdown_style',
			[
				'label'     => __( 'Countdown', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->add_responsive_control(
			'countdown_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-content div.product .tawc-deal .deal-expire-countdown' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'countdown_typography',
				'selector' => '{{WRAPPER}} .tz-product-deals-carousel-2 .product-content div.product .tawc-deal .deal-expire-countdown',
			]
		);

		// Countdown
		$this->add_control(
			'countdown_more_options',
			[
				'label'        => __( 'More Options', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();
		$this->add_control(
			'countdown_expire_text',
			[
				'label'     => esc_html__( 'Expire Text', 'teckzone' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'block' => esc_html__( 'Show', 'teckzone' ),
					'none'  => esc_html__( 'Hide', 'teckzone' ),
				],
				'default'   => 'block',
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-content div.product .tawc-deal .deal-expire-text' => 'display: {{VALUE}}',
				],
			]
		);
		$this->add_control(
			'countdown_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-content div.product .tawc-deal .deal-expire-countdown .timer'       => 'color: {{VALUE}};',
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-content div.product .tawc-deal .deal-expire-countdown .timer .text' => 'color: {{VALUE}};',
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-content div.product .tawc-deal .deal-expire-countdown .divider'     => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'countdown_bg_color',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-content div.product .tawc-deal .deal-expire-countdown' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->end_popover();

		$this->end_controls_section();
	}

	protected function register_products_carousel_style_controls() {
		// Carousel Style
		$this->start_controls_section(
			'carousel_settings_style',
			[
				'label' => esc_html__( 'Carousel Settings', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'arrows_style_divider',
			[
				'label' => __( 'Arrows', 'teckzone' ),
				'type'  => Controls_Manager::HEADING,
			]
		);
		$this->add_control(
			'arrows_style',
			[
				'label'        => __( 'Options', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();

		$this->add_responsive_control(
			'arrows_size',
			[
				'label'     => __( 'Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 150,
						'min' => 0,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-header .slick-arrows .slick-arrow' => 'font-size: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'arrows_width',
			[
				'label'      => __( 'Width', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'max' => 500,
						'min' => 0,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-header .slick-arrows .slick-arrow' => 'width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'arrows_height',
			[
				'label'      => __( 'Height', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'max' => 500,
						'min' => 0,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-header .slick-arrows .slick-arrow' => 'height: {{SIZE}}{{UNIT}}; line-height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->end_popover();

		$this->start_controls_tabs( 'products_normal_settings' );

		$this->start_controls_tab( 'products_normal', [ 'label' => esc_html__( 'Normal', 'teckzone' ) ] );

		$this->add_control(
			'product_arrows_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-header .slick-arrows .slick-arrow' => 'color: {{VALUE}}',
				],
			]
		);
		$this->add_control(
			'product_arrows_bg_color',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-header .slick-arrows .slick-arrow' => 'background-color: {{VALUE}}',
				],
			]
		);
		$this->add_control(
			'product_arrows_border_color',
			[
				'label'     => esc_html__( 'Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-header .slick-arrows .slick-arrow' => 'border-color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'products_hover', [ 'label' => esc_html__( 'Hover', 'teckzone' ) ] );

		$this->add_control(
			'product_arrows_hover_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-header .slick-arrows .slick-arrow:hover' => 'color: {{VALUE}}',
				],
			]
		);
		$this->add_control(
			'product_arrows_hover_bg_color',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-header .slick-arrows .slick-arrow:hover' => 'background-color: {{VALUE}}',
				],
			]
		);
		$this->add_control(
			'product_arrows_hover_border_color',
			[
				'label'     => esc_html__( 'Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .product-header .slick-arrows .slick-arrow:hover' => 'border-color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		// Dots
		$this->add_control(
			'dots_divider',
			[
				'label'     => __( 'Dots', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		$this->add_control(
			'dots_style',
			[
				'label'        => __( 'Options', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();

		$this->add_responsive_control(
			'dots_gap',
			[
				'label'     => __( 'Gap', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 50,
						'min' => 0,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .slick-dots li' => 'margin: 0 {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'dots_size',
			[
				'label'     => __( 'Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 30,
						'min' => 0,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .slick-dots li button'        => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .tz-product-deals-carousel-2 .slick-dots li button:before' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'dots_offset',
			[
				'label'     => esc_html__( 'Vertical Offset', 'teckzone' ),
				'type'      => Controls_Manager::NUMBER,
				'step'      => 1,
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .slick-dots' => 'bottom: {{VALUE}}px;',
				],
			]
		);
		$this->add_control(
			'dots_bg_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .slick-dots li button:before' => 'background-color: {{VALUE}}',
				],
			]
		);
		$this->add_control(
			'dots_active_bg_color',
			[
				'label'     => esc_html__( 'Active Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .tz-product-deals-carousel-2 .slick-dots li.slick-active button:before' => 'background-color: {{VALUE}}',
					'{{WRAPPER}} .tz-product-deals-carousel-2 .slick-dots li button:hover:before'        => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->end_popover();
		$this->end_controls_section(); // End Carousel Style
	}

	public function deal_expire_text( $text ) {
		$settings = $this->get_settings_for_display();
		if ( $settings['expire_text'] ) {
			$text = $settings['expire_text'];
		}

		return $text;
	}

	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$this->add_render_attribute(
			'wrapper', 'class', [
				'tz-product-deals-carousel-2 tz-elementor-product-carousel woocommerce',
				'tz-product-deals-carousel-2--query-' . $settings['product_type'],
			]
		);

		add_filter( 'single_product_deals_expire_text', [ $this, 'deal_expire_text' ] );

		$title_icon = '';
		if ( $settings['title_icon'] && ! empty( $settings['title_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['title_icon'], [ 'aria-hidden' => 'true' ] );
			$title_icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		$slides                 = [ 'slidesToShow', 'slidesToScroll', 'slidesToShow_tablet', 'slidesToScroll_tablet', 'slidesToShow_mobile', 'slidesToScroll_mobile' ];
		$settings['pagination'] = '';

		foreach ( $slides as $slide ) {
			$settings[$slide] = 1;
		}

		$this->add_render_attribute( 'wrapper', 'data-settings', wp_json_encode( Elementor::get_data_slick( $settings ) ) );

		$product_deals = Elementor::get_product_deals( $settings, 'single-product-deal' );
		if ( empty( $product_deals ) ) {
			return;
		}

		?>
		<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
			<div class="product-header">
				<h2 class="product-header__title"><?php echo $title_icon . esc_html( $settings['title'] ); ?></h2>

				<div class="slick-arrows"></div>
			</div>
			<div class="product-content">
				<?php echo $product_deals; ?>
			</div>
		</div>
		<?php

	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected
	function _content_template() {
	}

	/**
	 * Get the link control
	 *
	 * @return string.
	 */
	protected function get_link_control( $link_key, $url, $content, $attr = [ ] ) {
		$attr_default = [
			'href' => $url['url'] ? $url['url'] : '#',
		];

		if ( $url['is_external'] ) {
			$attr_default['target'] = '_blank';
		}

		if ( $url['nofollow'] ) {
			$attr_default['rel'] = 'nofollow';
		}

		$attr = wp_parse_args( $attr, $attr_default );

		$this->add_render_attribute( $link_key, $attr );

		return sprintf( '<a %1$s>%2$s</a>', $this->get_render_attribute_string( $link_key ), $content );
	}
}