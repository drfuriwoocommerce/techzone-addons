<?php

namespace TeckzoneAddons\Elementor\Widgets\Headers;

use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Border;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Icon Box widget
 */
class Promotion extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'techzone-promotion';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return __( 'Teckzone - Promotion', 'teckzone' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-form-vertical';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'site_header' ];
	}

	public function get_script_depends() {
		return [
			'techzone-elementor'
		];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->section_content();

		// Style
		$this->section_promotion_style();
		$this->section_sale_box_style();
		$this->section_heading_style();
		$this->section_price_style();
		$this->section_close_button_style();
	}


	protected function section_content() {
		$this->start_controls_section(
			'section_promotion',
			[ 'label' => esc_html__( 'Promotion', 'teckzone' ) ]
		);

		$this->start_controls_tabs( 'promotion_content_settings' );

		$this->start_controls_tab( 'content', [ 'label' => esc_html__( 'Content', 'teckzone' ) ] );

		$this->add_control(
			'sale_box',
			[
				'label' => esc_html__( 'Sale Box', 'teckzone' ),
				'type'  => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'sale_text',
			[
				'label'       => esc_html__( 'Text', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => esc_html__( 'Enter your text', 'teckzone' ),
				'label_block' => true,
				'default'     => __( 'Sale<br> Up To', 'teckzone' ),
			]
		);

		$this->add_control(
			'sale_number',
			[
				'label'       => esc_html__( 'Number', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => esc_html__( 'Enter your text', 'teckzone' ),
				'label_block' => true,
				'default'     => __( '50', 'teckzone' ),
			]
		);

		$this->add_control(
			'sale_unit',
			[
				'label'       => esc_html__( 'Unit', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'placeholder' => esc_html__( 'Enter your text', 'teckzone' ),
				'label_block' => true,
				'default'     => __( '%', 'teckzone' ),
			]
		);

		$this->add_control(
			'title',
			[
				'label'       => esc_html__( 'Title', 'teckzone' ),
				'type'        => Controls_Manager::TEXTAREA,
				'default'     => __( 'This is the title', 'teckzone' ),
				'placeholder' => esc_html__( 'Enter your title', 'teckzone' ),
				'label_block' => true,
				'separator'   => 'before',
			]
		);

		$this->add_control(
			'desc',
			[
				'label'       => esc_html__( 'Description', 'teckzone' ),
				'type'        => Controls_Manager::WYSIWYG,
				'default'     => __( 'This is the description', 'teckzone' ),
				'placeholder' => esc_html__( 'Enter your description', 'teckzone' ),
				'label_block' => true,
			]
		);
		$this->add_control(
			'price_html',
			[
				'label'     => esc_html__( 'Price', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		$this->add_control(
			'regular_price',
			[
				'label'       => esc_html__( 'Regular Price', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => '$79.99',
				'label_block' => true,
			]
		);
		$this->add_control(
			'sale_price',
			[
				'label'       => esc_html__( 'Sale Price', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => '$39.99',
				'label_block' => true,
			]
		);


		$this->end_controls_tab();

		$this->start_controls_tab( 'background', [ 'label' => esc_html__( 'Background', 'teckzone' ) ] );

		$this->add_control(
			'background_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-promotion .promotion-featured-image' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_responsive_control(
			'image',
			[
				'label'     => esc_html__( 'Image', 'teckzone' ),
				'type'      => Controls_Manager::MEDIA,
				'selectors' => [
					'{{WRAPPER}} .teckzone-promotion .promotion-featured-image' => 'background-image: url("{{URL}}");',
				],
			]
		);


		$this->add_responsive_control(
			'promotion_bg_position',
			[
				'label'     => esc_html__( 'Background Position', 'teckzone' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'left top'      => esc_html__( 'Left Top', 'teckzone' ),
					'left center'   => esc_html__( 'Left Center', 'teckzone' ),
					'left bottom'   => esc_html__( 'Left Bottom', 'teckzone' ),
					'right top'     => esc_html__( 'Right Top', 'teckzone' ),
					'right center'  => esc_html__( 'Right Center', 'teckzone' ),
					'right bottom'  => esc_html__( 'Right Bottom', 'teckzone' ),
					'center top'    => esc_html__( 'Center Top', 'teckzone' ),
					''              => esc_html__( 'Center Center', 'teckzone' ),
					'center bottom' => esc_html__( 'Center Bottom', 'teckzone' ),
				],
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-promotion .promotion-featured-image' => 'background-position: {{VALUE}};',
				],

			]
		);

		$this->add_responsive_control(
			'background_position_xy',
			[
				'label'              => esc_html__( 'Custom Background Position', 'teckzone' ),
				'type'               => Controls_Manager::DIMENSIONS,
				'allowed_dimensions' => [ 'top', 'left' ],
				'size_units'         => [ 'px', '%' ],
				'default'            => [],
				'selectors'          => [
					'{{WRAPPER}} .teckzone-promotion .promotion-featured-image' => 'background-position: {{LEFT}}{{UNIT}} {{TOP}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'promotion_bg_size',
			[
				'label'     => esc_html__( 'Background Size', 'teckzone' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'cover'   => esc_html__( 'Cover', 'teckzone' ),
					''        => esc_html__( 'Auto', 'teckzone' ),
					'contain' => esc_html__( 'Contain', 'teckzone' ),
				],
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-promotion .promotion-featured-image' => 'background-size: {{VALUE}};',
				],

			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->add_control(
			'button_link', [
				'label'         => esc_html__( 'Link URL', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '',
					'is_external' => false,
					'nofollow'    => false,
				],
				'separator'     => 'before',
			]
		);

		$this->add_responsive_control(
			'height',
			[
				'label'     => esc_html__( 'Height', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 20,
						'max' => 600,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-promotion .promotion-content' => 'height: {{SIZE}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);


		$this->end_controls_section();
	}

	protected function section_promotion_style() {
		$this->start_controls_section(
			'section_style_promotion',
			[
				'label' => esc_html__( 'Promotion', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'     => 'promotion_border',
				'label'    => esc_html__( 'Border', 'teckzone' ),
				'selector' => '{{WRAPPER}} .teckzone-promotion',
			]
		);

		$this->add_control(
			'promotion_content_width',
			[
				'label'      => esc_html__( 'Content Width', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ '%', 'px' ],
				'separator'  => 'before',
				'range'      => [
					'%'  => [
						'min' => 10,
						'max' => 100,
					],
					'px' => [
						'min' => 100,
						'max' => 2000,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-promotion .promotion-content, {{WRAPPER}} .teckzone-promotion .pro-close-button' => 'max-width: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'promotion_content_padding',
			[
				'label'      => esc_html__( 'Content Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-promotion .promotion-content, {{WRAPPER}} .teckzone-promotion .pro-close-button' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);


		$this->end_controls_section();
	}

	protected function section_sale_box_style() {
		$this->start_controls_section(
			'section_style_sale_box',
			[
				'label' => esc_html__( 'Sale Box', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'show_sale_box',
			[
				'label'     => esc_html__( 'Show Sale Box', 'martfury' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'flex' => esc_html__( 'Show', 'martfury' ),
					'none'  => esc_html__( 'Hide', 'martfury' ),
				],
				'default'   => 'flex',
				'selectors' => [
					'{{WRAPPER}} .teckzone-promotion .sale-box' => 'display: {{VALUE}}',

				],
			]
		);


		$this->add_control(
			'sale_box_spacing',
			[
				'label'      => esc_html__( 'Spacing', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ '%', 'px' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-promotion .sale-box' => 'margin-right: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'sale_box_bg_color',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-promotion .sale-box' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'sale_box_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-promotion .sale-box' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'sale_box_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-promotion .sale-box' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'sale_box_border_radius',
			[
				'label'      => esc_html__( 'Border Radius', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-promotion .sale-box' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'     => 'sale_box_border',
				'label'    => esc_html__( 'Border', 'teckzone' ),
				'selector' => '{{WRAPPER}} .teckzone-promotion .sale-box',
			]
		);

		$this->add_responsive_control(
			'sale_box_heading',
			[
				'label' => '',
				'type'  => Controls_Manager::DIVIDER,
			]
		);

		$this->start_controls_tabs( 'sale_tabs' );

		$this->start_controls_tab( 'sale_text_tab', [ 'label' => esc_html__( 'Text', 'teckzone' ) ] );

		$this->add_control(
			'sale_text_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-promotion .sale-text' => 'color: {{VALUE}}',

				],
			]
		);

		$this->add_control(
			'sale_text_spacing',
			[
				'label'      => esc_html__( 'Spacing', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ '%', 'px' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-promotion .sale-text' => 'margin-right: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'sale_text_typography',
				'selector' => '{{WRAPPER}} .teckzone-promotion .sale-text',
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'sale_number_tab', [ 'label' => esc_html__( 'Number', 'teckzone' ) ] );

		$this->add_control(
			'sale_number_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-promotion .sale-number' => 'color: {{VALUE}}',

				],
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'sale_number_typography',
				'selector' => '{{WRAPPER}} .teckzone-promotion .sale-number',
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'sale_unit_tab', [ 'label' => esc_html__( 'Unit', 'teckzone' ) ] );

		$this->add_control(
			'sale_unit_position_top',
			[
				'label'      => esc_html__( 'Top', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ '%', 'px' ],
				'range'      => [
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-promotion .sale-unit' => 'top: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'sale_unit_position_right',
			[
				'label'      => esc_html__( 'Right', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ '%', 'px' ],
				'range'      => [
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-promotion .sale-unit' => 'right: {{SIZE}}{{UNIT}}',
				],
			]
		);


		$this->add_control(
			'sale_unit_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-promotion .sale-unit' => 'color: {{VALUE}}',

				],
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'sale_unit_typography',
				'selector' => '{{WRAPPER}} .teckzone-promotion .sale-unit',
			]
		);

		$this->end_controls_tab();


		$this->end_controls_section();
	}

	protected function section_heading_style() {
		$this->start_controls_section(
			'section_style_heading',
			[
				'label' => esc_html__( 'Title & Description', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'title_heading',
			[
				'label' => esc_html__( 'Title', 'teckzone' ),
				'type'  => Controls_Manager::HEADING,
			]
		);

		$this->add_control(
			'title_spacing',
			[
				'label'      => esc_html__( 'Spacing', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ '%', 'px' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-promotion .pro-title' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'title_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-promotion .pro-title' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'title_typography',
				'selector' => '{{WRAPPER}} .teckzone-promotion .pro-title',
			]
		);

		$this->add_control(
			'description_heading',
			[
				'label'     => esc_html__( 'Description', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->add_control(
			'description_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-promotion .pro-desc' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'description_typography',
				'selector' => '{{WRAPPER}} .teckzone-promotion .pro-desc',
			]
		);


		$this->end_controls_section();
	}

	protected function section_price_style() {
		$this->start_controls_section(
			'section_style_price',
			[
				'label' => esc_html__( 'Price', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'show_price',
			[
				'label'     => esc_html__( 'Show Price', 'martfury' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'block' => esc_html__( 'Show', 'martfury' ),
					'none'  => esc_html__( 'Hide', 'martfury' ),
				],
				'default'   => 'block',
				'selectors' => [
					'{{WRAPPER}} .teckzone-promotion .p-price' => 'display: {{VALUE}}',

				],
			]
		);

		$this->add_control(
			'price_spacing',
			[
				'label'      => esc_html__( 'Spacing', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ '%', 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 1000,
					],
					'%'  => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-promotion .p-price' => 'padding-right: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'regular_price_heading',
			[
				'label'     => esc_html__( 'Regular Price', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->add_control(
			'regular_price_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-promotion .p-price del' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'regular_price_typography',
				'selector' => '{{WRAPPER}} .teckzone-promotion .p-price del',
			]
		);

		$this->add_control(
			'sale_price_heading',
			[
				'label'     => esc_html__( 'Sale Price', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->add_control(
			'sale_price_spacing',
			[
				'label'      => esc_html__( 'Spacing', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ '%', 'px' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-promotion .p-price ins' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'sale_price_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-promotion .p-price ins' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'sale_price_typography',
				'selector' => '{{WRAPPER}} .teckzone-promotion .p-price ins',
			]
		);


		$this->end_controls_section();
	}

	protected function section_close_button_style() {
		$this->start_controls_section(
			'section_style_close',
			[
				'label' => esc_html__( 'Close Button', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'close_button_position_top',
			[
				'label'      => esc_html__( 'Top', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ '%', 'px' ],
				'range'      => [
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-promotion .pro-close' => 'top: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'close_button_position_right',
			[
				'label'      => esc_html__( 'Right', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ '%', 'px' ],
				'range'      => [
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-promotion .pro-close' => 'right: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'close_button_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-promotion .pro-close' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator'  => 'before',
			]
		);


		$this->add_control(
			'close_button_bg_color',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-promotion .pro-close' => 'background-color: {{VALUE}}',
				],
				'separator' => 'before',
			]
		);

		$this->add_control(
			'close_button_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-promotion .pro-close' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'close_button_typography',
				'selector' => '{{WRAPPER}} .teckzone-promotion .pro-close',
			]
		);


		$this->end_controls_section();
	}

	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$this->add_render_attribute( 'wrapper', 'class', [
			'teckzone-promotion',
		] );

		if ( $settings['button_link']['is_external'] ) {
			$this->add_render_attribute( 'link', 'target', '_blank' );
		}

		if ( $settings['button_link']['nofollow'] ) {
			$this->add_render_attribute( 'link', 'rel', 'nofollow' );
		}

		if ( $settings['button_link']['url'] ) {
			$this->add_render_attribute( 'link', 'href', $settings['button_link']['url'] );
		}

		$sale_box = $settings['sale_text'] ? sprintf( '<div class="sale-text">%s</div>', $settings['sale_text'] ) : '';
		$sale_box .= $settings['sale_number'] ? sprintf( '<div class="sale-number">%s</div>', $settings['sale_number'] ) : '';
		$sale_box .= $settings['sale_unit'] ? sprintf( '<div class="sale-unit">%s</div>', $settings['sale_unit'] ) : '';

		$price_html = '';
		if ( ! empty( $settings['sale_price'] ) ) {
			$price_html .= sprintf( '<ins>%s</ins>', $settings['sale_price'] );
			$price_html .= ! empty( $settings['regular_price'] ) ? sprintf( '<del>%s</del>', $settings['regular_price'] ) : '';
		} else {
			$price_html .= ! empty( $settings['regular_price'] ) ? sprintf( '<ins>%s</ins>', $settings['regular_price'] ) : '';
		}

		?>
        <div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
            <div class="promotion-featured-image"></div>
			<?php if ( $settings['button_link']['url'] ) { ?>
                <a class="btn-link" <?php echo $this->get_render_attribute_string( 'link' ); ?>></a>
			<?php } ?>
            <div class="promotion-content">
                <div class="promotion-left">
                    <div class="sale-box">
						<?php echo $sale_box; ?>
                    </div>
                    <div class="inner-content">
                        <h2 class="pro-title"><?php echo $settings['title']; ?></h2>
                        <div class="pro-desc"><?php echo $settings['desc']; ?></div>
                    </div>
                </div>
                <div class="promotion-right">
                    <div class="p-price">
						<?php echo $price_html; ?>
                    </div>
                </div>
            </div>
            <div class="pro-close-button">
                <a href="#" class="pro-close"><i class="icon-cross"></i></a>
            </div>
        </div>
		<?php

	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected function _content_template() {
	}
}