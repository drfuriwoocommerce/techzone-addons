<?php

namespace TeckzoneAddons\Elementor\Widgets\Headers;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;
use Elementor\Group_Control_Image_Size;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Logo widget
 */
class Logo extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'techzone-logo';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Teckzone - Logo', 'teckzone' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-logo';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'site_header' ];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->section_content();
		$this->section_style();
	}

	/**
	 * Section Content
	 */
	protected function section_content() {
		$this->start_controls_section(
			'section_content',
			[ 'label' => esc_html__( 'Content', 'teckzone' ) ]
		);

		$this->add_control(
			'type_icon',
			[
				'label'   => __( 'Type Logo', 'teckzone' ),
				'type'    => Controls_Manager::CHOOSE,
				'options' => [
					'icon'  => [
						'title' => __( 'Icon', 'teckzone' ),
						'icon'  => 'fa fa-star',
					],
					'image' => [
						'title' => __( 'Image', 'teckzone' ),
						'icon'  => 'fa fa-picture-o',
					],
				],
				'default' => 'image',
				'toggle'  => true,
			]
		);

		$this->add_control(
			'icon',
			[
				'label'     => esc_html__( 'Icon', 'teckzone' ),
				'type'      => Controls_Manager::ICONS,
				'default'          => [
					'value'   => 'fas fa-star',
					'library' => 'fa-solid',
				],
				'condition' => [
					'type_icon' => 'icon',
				],
			]
		);

		$this->add_control(
			'image', [
				'label'     => esc_html__( 'Choose Image', 'teckzone' ),
				'type'      => Controls_Manager::MEDIA,
				'condition' => [
					'type_icon' => 'image',
				],
			]
		);

		$this->add_control(
			'link', [
				'label'         => esc_html__( 'Link', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Section Style
	 */
	protected function section_style() {
		$this->section_general_style();
		$this->section_icon_style();
	}

	protected function section_general_style() {
		$this->start_controls_section(
			'style_general',
			[
				'label' => __( 'General', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'margin',
			[
				'label'      => esc_html__( 'Margin', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-logo' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator'   => 'after',
			]
		);

		$this->add_control(
			'border_style',
			[
				'label'     => esc_html__( 'Border Style', 'teckzone' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'dotted' => esc_html__( 'Dotted', 'teckzone' ),
					'dashed' => esc_html__( 'Dashed', 'teckzone' ),
					'solid'  => esc_html__( 'Solid', 'teckzone' ),
					'none'   => esc_html__( 'None', 'teckzone' ),
				],
				'default'   => '',
				'toggle'    => false,
				'selectors' => [
					'{{WRAPPER}} .teckzone-logo' => 'border-style: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'border_color',
			[
				'label'     => esc_html__( 'Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-logo' => 'border-color: {{VALUE}}',

				],
			]
		);

		$this->add_control(
			'border_width',
			[
				'label' => __( 'Border Width', 'teckzone' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 20,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-logo' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Element in Tab Style
	 *
	 * Icon
	 */
	protected function section_icon_style() {
		$this->start_controls_section(
			'section_icon_style',
			[
				'label' => __( 'Icon', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'icon_font_size',
			[
				'label'     => __( 'Font Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [],
				'range'     => [
					'px' => [
						'min' => 30,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-logo .teckzone-icon'     => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .teckzone-logo .teckzone-icon svg' => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'icon_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-logo .teckzone-icon'     => 'color: {{VALUE}};',
					'{{WRAPPER}} .teckzone-logo .teckzone-icon svg' => 'fill: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$this->add_render_attribute(
			'wrapper', 'class', [
				'teckzone-logo',
			]
		);

		if ( $settings['type_icon'] == 'image' ) {
			$logo = sprintf('<img src="%s" alt="%s">',esc_url(get_theme_file_uri( '/images/logo.png' )),esc_attr( get_bloginfo( 'name' )));

			if ( ! empty($settings['image']['url'])){
				$logo = Group_Control_Image_Size::get_attachment_image_html( $settings );
				$logo = $logo ? sprintf( '<span class="box-img">%s</span>', $logo ) : '';
			}
		} else {
			if ( $settings['icon'] && \Elementor\Icons_Manager::is_migration_allowed() ) {
				ob_start();
				\Elementor\Icons_Manager::render_icon( $settings['icon'], [ 'aria-hidden' => 'true' ] );
				$logo = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
			}
		}

		$url        = ! empty( $settings['link']['url'] ) ? $settings['link']['url'] : esc_url(  home_url( '/' )  );
		$output     = sprintf(
			'<a class="logo" href="%s">%s</a>',
			$url,
			$logo
		);

		echo sprintf(
			'<div %s>%s</div>',
			$this->get_render_attribute_string( 'wrapper' ),
			$output
		);

	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected
	function _content_template() {
	}


}