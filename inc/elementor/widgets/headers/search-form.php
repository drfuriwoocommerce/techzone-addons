<?php

namespace TeckzoneAddons\Elementor\Widgets\Headers;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Search_Form extends Widget_Base {

	public function get_name() {
		return 'techzone-search-form';
	}

	public function get_title() {
		return esc_html__( 'Teckzone - Search Form', 'teckzone' );
	}

	public function get_icon() {
		return 'eicon-site-search';
	}

	public function get_keywords() {
		return [ 'search', 'form' ];
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'site_header' ];
	}

	public function get_script_depends() {
		return [
			'techzone-elementor'
		];
	}

	protected function _register_controls() {

		$this->start_controls_section(
			'search_form_section',
			[
				'label' => esc_html__( 'Search Form', 'teckzone' ),
			]
		);

		$this->add_control(
			'search_for',
			[
				'label'   => esc_html__( 'Search For', 'teckzone' ),
				'type'    => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					''         => esc_html__( 'Everything', 'teckzone' ),
					'products' => esc_html__( 'Only Products', 'teckzone' ),
				],
			]
		);

		$this->add_control(
			'cats_text',
			[
				'label'     => esc_html__( 'Categories Text', 'teckzone' ),
				'type'      => Controls_Manager::TEXT,
				'separator' => 'before',
				'default'   => __( 'All', 'teckzone' ),
			]
		);

		$this->add_control(
			'placeholder',
			[
				'label'   => esc_html__( 'Placeholder', 'teckzone' ),
				'type'    => Controls_Manager::TEXT,
				'default' => esc_html__( 'Search', 'teckzone' ) . '...',
			]
		);

		$this->add_control(
			'heading_button_content',
			[
				'label'     => esc_html__( 'Button', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->add_control(
			'button_text',
			[
				'label'   => esc_html__( 'Text', 'teckzone' ),
				'type'    => Controls_Manager::TEXT,
				'default' => esc_html__( 'Search', 'teckzone' ),
			]
		);

		$this->add_control(
			'button_icon',
			[
				'label'   => esc_html__( 'Icon', 'teckzone' ),
				'type'    => Controls_Manager::ICONS,
				'default' => [ ],
			]
		);

		$this->add_control(
			'button_icon_position',
			[
				'label'   => esc_html__( 'Icon Position', 'teckzone' ),
				'type'    => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					''       => esc_html__( 'After Text', 'teckzone' ),
					'before' => esc_html__( 'Before Text', 'teckzone' ),
				],
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'hot_words_section',
			[
				'label' => esc_html__( 'Hot Words', 'teckzone' ),
			]
		);
		$this->add_control(
			'enable_hot_words',
			[
				'label'     => esc_html__( 'Enable Hot Words', 'teckzone' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_off' => esc_html__( 'Off', 'teckzone' ),
				'label_on'  => esc_html__( 'On', 'teckzone' ),
				'default'   => 'yes'
			]
		);

		$this->add_control(
			'hot_words_title',
			[
				'label'       => esc_html__( 'Text', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'Trending Now', 'teckzone' ),
				'label_block' => true,
			]
		);

		$repeater = new \Elementor\Repeater();
		$repeater->add_control(
			'text',
			[
				'label'       => esc_html__( 'Text', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'Hot Word', 'teckzone' ),
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'link', [
				'label'         => esc_html__( 'Link', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],

			]
		);
		$this->add_control(
			'hot_words_content',
			[
				'label'         => esc_html__( 'Words', 'teckzone' ),
				'type'          => Controls_Manager::REPEATER,
				'fields'        => $repeater->get_controls(),
				'default'       => [
					[
						'text' => esc_html__( 'Word 1', 'teckzone' ),
						'link' => '#'
					],
					[
						'text' => esc_html__( 'Word 2', 'teckzone' ),
						'link' => '#'
					],
					[
						'text' => esc_html__( 'Word 3', 'teckzone' ),
						'link' => '#'
					]
				],
				'title_field'   => '{{{ text }}}',
				'prevent_empty' => false
			]
		);

		$this->end_controls_section();

		$this->start_controls_section(
			'categories_section',
			[
				'label' => esc_html__( 'Categories DropDown', 'teckzone' ),
			]
		);
		$this->add_control(
			'cats_depth',
			[
				'label'   => esc_html__( 'Categories Depth', 'teckzone' ),
				'type'    => Controls_Manager::NUMBER,
				'default' => '',
			]
		);
		$this->add_control(
			'cats_include',
			[
				'label'       => esc_html__( 'Categories Include', 'teckzone' ),
				'type'        => Controls_Manager::TEXTAREA,
				'description' => esc_html__( 'Enter Category IDs to include. Divide every category by comma(,)', 'teckzone' ),
				'default'     => '',
			]
		);
		$this->add_control(
			'cats_exclude',
			[
				'label'       => esc_html__( 'Categories Exclude', 'teckzone' ),
				'type'        => Controls_Manager::TEXTAREA,
				'description' => esc_html__( 'Enter Category IDs to exclude. Divide every category by comma(,)', 'teckzone' ),
				'default'     => '',
			]
		);
		$this->end_controls_section();

		$this->start_controls_section(
			'ajax_search_section',
			[
				'label' => esc_html__( 'AJAX Search', 'teckzone' ),
			]
		);
		$this->add_control(
			'enable_ajax_search',
			[
				'label'     => esc_html__( 'Enable AJAX Search', 'teckzone' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_off' => esc_html__( 'Off', 'teckzone' ),
				'label_on'  => esc_html__( 'On', 'teckzone' ),
				'default'   => 'no'
			]
		);
		$this->add_control(
			'ajax_search_number',
			[
				'label'       => esc_html__( 'AJAX Search Results Number', 'teckzone' ),
				'type'        => Controls_Manager::NUMBER,
				'description' => esc_html__( 'The number of maximum results in the search box', 'teckzone' ),
				'default'     => 6,
			]
		);
		$this->end_controls_section();

		$this->section_style();
	}

	protected function section_style() {
		$this->section_general_style();
		$this->section_box_cat_style();
		$this->section_hot_words_style();
		$this->section_search_form_style();
		$this->section_search_results_style();
	}

	protected function section_general_style() {
		$this->start_controls_section(
			'style_general',
			[
				'label' => __( 'General', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'general_height',
			[
				'label'     => esc_html__( 'Height', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 30,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-products-search .product-cat'   => 'height: {{SIZE}}{{UNIT}};line-height: {{SIZE}}{{UNIT}}',
					'{{WRAPPER}} .teckzone-products-search .search-field'  => 'height: {{SIZE}}{{UNIT}}',
					'{{WRAPPER}} .teckzone-products-search .search-submit' => 'height: {{SIZE}}{{UNIT}};line-height: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->end_controls_section();
	}

	protected function section_box_cat_style() {
		$this->start_controls_section(
			'style_name',
			[
				'label' => __( 'Categories DropDown', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'style_name_typography',
				'selector' => '{{WRAPPER}} .teckzone-products-search  .product-cat-label,{{WRAPPER}} .teckzone-products-search  .product-cat-dd option',
			]
		);

		$this->add_control(
			'cat_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-products-search .product-cat-label'     => 'color: {{VALUE}};',
					'{{WRAPPER}} .teckzone-products-search .product-cat-dd option' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'cat_bk_color',
			[
				'label'     => __( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-products-search .product-cat-label' => 'background-color: {{VALUE}};',
				],
				'separator' => 'after',
			]
		);

		$this->add_control(
			'cat_border_style',
			[
				'label'     => esc_html__( 'Border Style', 'teckzone' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'dotted' => esc_html__( 'Dotted', 'teckzone' ),
					'dashed' => esc_html__( 'Dashed', 'teckzone' ),
					'solid'  => esc_html__( 'Solid', 'teckzone' ),
					'none'   => esc_html__( 'None', 'teckzone' ),
				],
				'default'   => '',
				'toggle'    => false,
				'selectors' => [
					'{{WRAPPER}} .teckzone-products-search .product-cat-label' => 'border-style: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'cat_border_color',
			[
				'label'     => esc_html__( 'Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-products-search .product-cat-label' => 'border-color: {{VALUE}}',

				],
			]
		);

		$this->add_control(
			'cat_border_width',
			[
				'label'      => __( 'Border Width', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 20,
					],
					'%'  => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-products-search .product-cat-label' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'cat_border_radius',
			[
				'label'      => __( 'Border Radius', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-products-search .product-cat-label' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();

	}

	protected function section_hot_words_style() {
		$this->start_controls_section(
			'style_hot_words',
			[
				'label' => __( 'Hot Words', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'hot_words_spacing',
			[
				'label'     => esc_html__( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-products-search .hot-words' => 'margin-top: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'hot_words_padding',
			[
				'label'              => esc_html__( 'Padding', 'teckzone' ),
				'type'               => Controls_Manager::DIMENSIONS,
				'allowed_dimensions' => [ 'left', 'right' ],
				'size_units'         => [ 'px', '%' ],
				'default'            => [ ],
				'selectors'          => [
					'{{WRAPPER}} .teckzone-products-search .hot-words li' => 'padding-left: {{LEFT}}{{UNIT}};padding-right: {{RIGHT}}{{UNIT}}',
				],
			]
		);

		$this->start_controls_tabs( 'settings_hot_word' );

		$this->start_controls_tab( 'style_title_word', [ 'label' => esc_html__( 'Title', 'teckzone' ) ] );

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'style_title_word_typography',
				'selector' => '{{WRAPPER}} .teckzone-products-search  .title-words',
			]
		);

		$this->add_control(
			'title_word_color',
			[
				'label'     => esc_html__( 'Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-products-search .title-words' => 'color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'style_key_word', [ 'label' => esc_html__( 'Key Words', 'teckzone' ) ] );

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'style_hot_key_typography',
				'selector' => '{{WRAPPER}} .teckzone-products-search .hot-words li:not(.title-words)',
			]
		);

		$this->add_control(
			'key_word_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-products-search .hot-words li:not(.title-words) a' => 'color: {{VALUE}}',
					'{{WRAPPER}} .teckzone-products-search .hot-words li:not(.title-words)'   => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'key_word_hover_color',
			[
				'label'     => esc_html__( 'Hover Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-products-search .hot-words li:not(.title-words) a:hover' => 'color: {{VALUE}}',
					'{{WRAPPER}} .teckzone-products-search .hot-words li:not(.title-words):hover'   => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'key_deco_hover_color',
			[
				'label'     => esc_html__( 'Hover text decoration', 'teckzone' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'underline' => esc_html__( 'Underline', 'teckzone' ),
					'none'      => esc_html__( 'None', 'teckzone' ),
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-products-search .hot-words li:not(.title-words) a:hover' => 'text-decoration: {{VALUE}}',
					'{{WRAPPER}} .teckzone-products-search .hot-words li:not(.title-words):hover'   => 'text-decoration: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->end_controls_section();

	}

	protected function section_search_form_style() {
		$this->start_controls_section(
			'style_search_form_',
			[
				'label' => __( 'Search Form', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'form_border_style',
			[
				'label'     => esc_html__( 'Border Style', 'teckzone' ),
				'type'      => Controls_Manager::SELECT,
				'options'   => [
					'dotted' => esc_html__( 'Dotted', 'teckzone' ),
					'dashed' => esc_html__( 'Dashed', 'teckzone' ),
					'solid'  => esc_html__( 'Solid', 'teckzone' ),
					'none'   => esc_html__( 'None', 'teckzone' ),
				],
				'default'   => '',
				'toggle'    => false,
				'selectors' => [
					'{{WRAPPER}} .teckzone-products-search .search-field' => 'border-style: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'form_border_color',
			[
				'label'     => esc_html__( 'Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-products-search .search-field' => 'border-color: {{VALUE}}',

				],
			]
		);

		$this->add_control(
			'form_border_width',
			[
				'label'      => __( 'Border Width', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 20,
					],
					'%'  => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-products-search .search-field' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->start_controls_tabs( 'settings_search_form' );

		$this->start_controls_tab( 'style_placeholder', [ 'label' => esc_html__( 'Placeholder', 'teckzone' ) ] );

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'style_title_placeholder',
				'selector' => '{{WRAPPER}} .teckzone-products-search .search-field',
			]
		);

		$this->add_control(
			'search_field_color',
			[
				'label'     => esc_html__( 'Text Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-products-search .search-field'               => 'color: {{VALUE}}',
					'{{WRAPPER}} .teckzone-products-search .search-field::placeholder ' => 'color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'style_button_form', [ 'label' => esc_html__( 'Button', 'teckzone' ) ] );

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'style_button_typography',
				'selector' => '{{WRAPPER}} .teckzone-products-search .button-label',
			]
		);

		$this->add_responsive_control(
			'button_padding',
			[
				'label'              => esc_html__( 'Padding', 'teckzone' ),
				'type'               => Controls_Manager::DIMENSIONS,
				'allowed_dimensions' => [ 'left', 'right' ],
				'size_units'         => [ 'px', '%' ],
				'default'            => [ ],
				'selectors'          => [
					'{{WRAPPER}} .teckzone-products-search .search-submit' => 'padding-left: {{LEFT}}{{UNIT}};padding-right: {{RIGHT}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'button_border_radius',
			[
				'label'      => __( 'Border Radius', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-products-search .search-submit' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'button_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-products-search .search-submit' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'button_bk_color',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-products-search .search-submit' => 'background-color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'icon_spacing',
			[
				'label'     => esc_html__( 'Spacing Icon', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-products-search .tz-icon-before-text .teckzone-icon' => 'margin-right: {{SIZE}}{{UNIT}}',
					'{{WRAPPER}} .teckzone-products-search .tz-icon-after-text .teckzone-icon'  => 'margin-left: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'icon_font_size',
			[
				'label'     => __( 'Font Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 30,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-products-search .teckzone-icon'     => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .teckzone-products-search .teckzone-icon svg' => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->end_controls_section();

	}

	protected function section_search_results_style() {
		$this->start_controls_section(
			'style_search_results_form_',
			[
				'label' => __( 'Search Results', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'results_padding',
			[
				'label'              => esc_html__( 'Box Results Padding', 'teckzone' ),
				'type'               => Controls_Manager::DIMENSIONS,
				'allowed_dimensions' => [ 'top', 'bottom' ],
				'size_units'         => [ 'px', '%' ],
				'default'            => [ ],
				'selectors'          => [
					'{{WRAPPER}} .teckzone-products-search .search-wrapper .search-results ul li' => 'padding-top: {{TOP}}{{UNIT}};padding-bottom: {{BOTTOM}}{{UNIT}}',
				],
			]
		);

		$this->start_controls_tabs( 'settings_search_results' );

		$this->start_controls_tab( 'style_woocommerce', [ 'label' => esc_html__( 'Woocommerce', 'teckzone' ) ] );

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'style_title_woo',
				'selector' => '{{WRAPPER}} .teckzone-products-search .search-wrapper .search-results ul li .title-item',
			]
		);

		$this->add_control(
			'title_woo_spacing',
			[
				'label'     => esc_html__( 'Title Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-products-search .search-wrapper .search-results ul li .title-item' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'title_woo_color',
			[
				'label'     => esc_html__( 'Title Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-products-search .search-wrapper .search-results ul li .title-item' => 'color: {{VALUE}}',
				],
				'separator' => 'after',
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'style_price_woo',
				'selector' => '{{WRAPPER}} .teckzone-products-search .search-wrapper .search-results ul li .price-item',
			]
		);

		$this->add_control(
			'price_woo_spacing',
			[
				'label'     => esc_html__( 'Price Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-products-search .search-wrapper .search-results ul li .price-item' => 'margin-top: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_control(
			'sale_woo_color',
			[
				'label'     => esc_html__( 'Sale Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-products-search .search-wrapper .search-results ul li ins' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'price_woo_color',
			[
				'label'     => esc_html__( 'Price Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-products-search .search-wrapper .search-results ul li del' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'price_not_sale_color',
			[
				'label'     => esc_html__( 'Price Color (not sale)', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-products-search .search-wrapper .search-results ul li .price-item' => 'color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();

	}

	protected function render() {
		$settings = $this->get_settings();

		$this->add_render_attribute(
			'wrapper', 'class', [
				'teckzone-products-search',
				empty( $settings['search_for'] ) ? 'search-every-thing' : 'search-products',
				empty( $settings['button_text'] ) ? 'no-margin' : ''
			]
		);

		$data_settings = [
			'ajax_search' => $settings['enable_ajax_search'],
			'search_for'  => $settings['search_for'],
		];

		if ( $settings['enable_ajax_search'] == 'yes' ) {
			$data_settings['ajax_search_number'] = $settings['ajax_search_number'];
		}

		$this->add_render_attribute(
			'wrapper', 'data-settings', wp_json_encode( $data_settings )
		);

		$cats_dropdown = $this->get_product_categories( $settings );

		$post_type_html = '';
		if ( $settings['search_for'] == 'products' ) {
			$post_type_html = '<input type="hidden" name="post_type" value="product">';
		}

		$lang = defined( 'ICL_LANGUAGE_CODE' ) ? ICL_LANGUAGE_CODE : false;
		if ( $lang ) {
			$post_type_html .= '<input type="hidden" name="lang" value="' . $lang . '"/>';
		}

		$button = $settings['button_text'];

		$button = $button ? '<span class="button-label">' . $button . '</span>' : '';

		if ( $settings['button_icon'] && ! empty( $settings['button_icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['button_icon'], [ 'aria-hidden' => 'true' ] );
			if ( $settings['button_icon']['value'] ) {
				$button .= '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
			}
		}

		$this->add_render_attribute(
			'button', 'class', [
				'search-submit',
				! empty( $settings['button_icon_position'] ) ? 'tz-icon-before-text' : ''
			]
		);

		// Hot words
		$element_key = $settings['hot_words_content'];
		$output_key  = [ ];
		if ( ! empty ( $element_key ) && $settings['enable_hot_words'] ) {
			$output_key[] = '<ul class="hot-words">';
			$output_key[] = '<li class="title-words">' . $settings['hot_words_title'] . ':</li>';
			foreach ( $element_key as $index => $item ) {
				$key = $item['text'] ? sprintf( '<span class="word">%s</span>', $item['text'] ) : '';

				$link_key = 'link_' . $index;
				$key_html = $item['link']['url'] ? $this->get_link_control( $link_key, $item['link'], $item['text'], [ 'class' => 'word' ] ) : $key;

				$output_key[] = '<li>' . $key_html . '</li>';
			}
			$output_key[] = '</ul>';
		}

		?>
		<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?> >
			<form method="get" class="form-search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
				<div class="search-inner-content">
					<?php echo $cats_dropdown; ?>
					<div class="search-wrapper">
						<input type="text" name="s" class="search-field" autocomplete="off"
							   placeholder="<?php echo $settings['placeholder']; ?>">
						<?php echo $post_type_html; ?>
						<div class="search-results woocommerce"></div>
						<a href="#" class="close-search-results"><i class="icon-cross2"></i> </a>
					</div>
				</div>
				<button <?php echo $this->get_render_attribute_string( 'button' ); ?>
					type="submit"><?php echo $button; ?></button>
			</form>
			<?php echo implode( '', $output_key ) ?>
		</div>
		<?php
	}

	function get_product_categories( $settings ) {
		$show_cat = true;
		if ( empty( $settings['search_for'] ) ) {
			$show_cat = false;
		}
		if ( ! taxonomy_exists( 'product_cat' ) || ! $show_cat ) {
			return;
		}

		$cats_text = $settings['cats_text'];
		$depth     = intval( $settings['cats_depth'] );
		$depth     = $depth > 0 ? $depth : 0;

		$args = array(
			'name'            => 'product_cat',
			'taxonomy'        => 'product_cat',
			'orderby'         => 'NAME',
			'hierarchical'    => 1,
			'hide_empty'      => 1,
			'echo'            => 0,
			'value_field'     => 'slug',
			'class'           => 'product-cat-dd',
			'show_option_all' => esc_html( $cats_text ),
			'depth'           => $depth,
			'id'              => '',
		);

		$cat_include = $settings['cats_include'];
		if ( ! empty( $cat_include ) ) {
			$cat_include     = explode( ',', $cat_include );
			$args['include'] = $cat_include;
		}

		$cat_exclude = $settings['cats_exclude'];
		if ( ! empty( $cat_exclude ) ) {
			$cat_exclude     = explode( ',', $cat_exclude );
			$args['exclude'] = $cat_exclude;
		}

		return sprintf(
			'<div class="product-cat">' .
			'<div class="product-cat-label">%s</div>' .
			'%s' .
			'</div>',
			$cats_text,
			wp_dropdown_categories( $args )
		);
	}

	function get_hot_words( $settings ) {

	}

	protected function _content_template() {

	}

	protected function get_link_control( $link_key, $url, $content, $attr = [ ] ) {
		$attr_default = [
			'href' => $url['url'] ? $url['url'] : '#',
		];

		if ( $url['is_external'] ) {
			$attr_default['target'] = '_blank';
		}

		if ( $url['nofollow'] ) {
			$attr_default['rel'] = 'nofollow';
		}

		$attr = wp_parse_args( $attr, $attr_default );

		$this->add_render_attribute( $link_key, $attr );

		return sprintf( '<a %1$s>%2$s</a>', $this->get_render_attribute_string( $link_key ), $content );
	}
}
