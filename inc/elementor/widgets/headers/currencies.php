<?php

namespace TeckzoneAddons\Elementor\Widgets\Headers;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Currencies extends Widget_Base {

	public function get_name() {
		return 'techzone-currencies';
	}

	public function get_title() {
		return esc_html__( 'Teckzone - Currencies', 'teckzone' );
	}

	public function get_icon() {
		return 'icon-cash-dollar';
	}

	public function get_keywords() {
		return [ 'currencies' ];
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'site_header' ];
	}

	public function get_script_depends() {
		return [
			'techzone-elementor'
		];
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'currencies_section',
			[
				'label' => esc_html__( 'Currencies', 'teckzone' ),
			]
		);

		$this->add_control(
			'currencies',
			[
				'label'   => esc_html__( 'Currencies Type', 'martfury' ),
				'type'    => Controls_Manager::SELECT,
				'default' => 'name',
				'options' => [
					'name'        => esc_html__( 'Show Name', 'teckzone' ),
					'description' => esc_html__( 'Show Description', 'teckzone' ),
				],
			]
		);

		$this->add_control(
			'icon',
			[
				'label'   => esc_html__( 'Icon', 'teckzone' ),
				'type'    => Controls_Manager::ICONS,
				'default' => [
					'value' => '',
				],
			]
		);

		$this->end_controls_section();

		$this->section_style();
	}

	/**
	 * Section Style
	 */
	protected function section_style() {
		$this->section_general_style();
		$this->section_dropdown_style();
		$this->section_icon_style();
		$this->section_line_style();
	}

	/**
	 * Element in General Style
	 *
	 * Icon
	 */
	protected function section_general_style() {
		$this->start_controls_section(
			'style_general',
			[
				'label' => __( 'General', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'general_align',
			[
				'label'     => esc_html__( 'Align', 'teckzone' ),
				'type'      => Controls_Manager::CHOOSE,
				'options'   => [
					''         => [
						'title' => esc_html__( 'Left', 'teckzone' ),
						'icon'  => 'fa fa-align-left',
					],
					'center'   => [
						'title' => esc_html__( 'Center', 'teckzone' ),
						'icon'  => 'fa fa-align-center',
					],
					'flex-end' => [
						'title' => esc_html__( 'Right', 'teckzone' ),
						'icon'  => 'fa fa-align-right',
					],
				],
				'default'   => '',
				'toggle'    => false,
				'selectors' => [
					'{{WRAPPER}} .teckzone-currencies '   => 'justify-content: {{VALUE}};',
					'{{WRAPPER}} .teckzone-currencies a ' => 'justify-content: {{VALUE}};',
				],
			]
		);

		$this->add_responsive_control(
			'padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-currencies' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'margin',
			[
				'label'      => esc_html__( 'Margin', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-currencies' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'currency_typography',
				'selector' => '{{WRAPPER}} .teckzone-currencies .current',
			]
		);

		$this->start_controls_tabs( 'general_tabs' );

		$this->start_controls_tab(
			'style_normal_tab',
			[
				'label' => __( 'Normal', 'teckzone' ),
			]
		);

		$this->add_control(
			'color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-currencies .current' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'style_hover_tab',
			[
				'label' => __( 'Hover', 'teckzone' ),
			]
		);

		$this->add_control(
			'hover_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-currencies:hover .current' => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function section_dropdown_style() {
		$this->start_controls_section(
			'section_dropdown_style',
			[
				'label' => __( 'Dropdown', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'menu_dropdown_spacing',
			[
				'label'      => esc_html__( 'Distance', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 200,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-currencies .currency-content' => 'border-top-width: {{SIZE}}{{UNIT}}',
					'{{WRAPPER}} .teckzone-currencies:before'            => 'top: calc(100% + 11px + {{SIZE}}{{UNIT}} - 7px)',
					'{{WRAPPER}} .teckzone-currencies:hover:before'      => 'top: calc(100% + 5px + {{SIZE}}{{UNIT}} - 7px)',
				],
			]
		);

		$this->add_responsive_control(
			'dropdown_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-currencies .currency-content ul' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'dropdown_border_options',
			[
				'label'        => __( 'Border', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();
		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name'     => 'dropdown_border',
				'label'    => __( 'Border', 'teckzone' ),
				'selector' => '{{WRAPPER}} .teckzone-currencies .currency-content ul',
			]
		);
		$this->add_responsive_control(
			'dropdown_border_radius',
			[
				'label'      => esc_html__( 'Border Radius', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-currencies .currency-content ul' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->end_popover();

		$this->add_control(
			'dropdown_background_color',
			[
				'label'     => __( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-currencies .currency-content ul' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'dropdown_items_divider',
			[
				'label'     => __( 'Items', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'title_typography',
				'selector' => '{{WRAPPER}} .teckzone-currencies .currency-content ul li a',
			]
		);

		$this->start_controls_tabs( 'items_style_tabs' );

		$this->start_controls_tab(
			'item_style_normal_tab',
			[
				'label' => __( 'Normal', 'teckzone' ),
			]
		);

		$this->add_control(
			'item_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-currencies .currency-content ul li a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'item_style_hover_tab',
			[
				'label' => __( 'Hover/Active', 'teckzone' ),
			]
		);

		$this->add_control(
			'item_hover_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-currencies .currency-content ul li:hover a' => 'color: {{VALUE}};',
					'{{WRAPPER}} .teckzone-currencies .currency-content ul li.actived a' => 'color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	/**
	 * Element in Tab Style
	 *
	 * Icon
	 */
	protected function section_icon_style() {
		$this->start_controls_section(
			'section_icon_style',
			[
				'label' => __( 'Icon', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'icon_margin',
			[
				'label'     => esc_html__( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-currencies .teckzone-icon' => 'margin-left: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'icon_font_size',
			[
				'label'     => __( 'Font Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-currencies .teckzone-icon' => 'font-size: {{SIZE}}{{UNIT}};'
				],
			]
		);
		$this->add_control(
			'icon_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-currencies .teckzone-icon'     => 'color: {{VALUE}};',
					'{{WRAPPER}} .teckzone-currencies .teckzone-icon svg' => 'fill: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Element in Tab Style
	 *
	 * Line
	 */
	protected function section_line_style() {
		$this->start_controls_section(
			'section_line_style',
			[
				'label' => __( 'Line', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'line_spacing',
			[
				'label'     => __( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 200,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .teckzone-currencies .line' => 'margin-left: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'line_width',
			[
				'label'      => __( 'Width', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 5,
					],
				],
				'default'    => [ ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-currencies .line' => 'width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'line_height',
			[
				'label'     => __( 'Height', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'max' => 100,
						'min' => 0,
					],
					'%'  => [
						'max' => 100,
						'min' => 0,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-currencies .line' => 'height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'line_background_color',
			[
				'label'     => __( 'Bachground Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-currencies .line' => 'background-color: {{VALUE}};',
				],
			]
		);
		$this->end_controls_section();
	}

	protected function render() {
		$settings = $this->get_settings_for_display();


		$this->add_render_attribute( 'wrapper', 'class', 'teckzone-currencies' );

		$icon = '';

		if ( $settings['icon'] && ! empty( $settings['icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['icon'], [ 'aria-hidden' => 'true' ] );
			$icon = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		$line = '<span class="line"></span>';

		?>
		<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
			<?php
			echo $this->woocs_currencies( $settings );
			echo $this->alg_currencies( $settings );
			echo $icon . $line;
			?>
		</div>
		<?php
	}

	function woocs_currencies( $settings ) {
		if ( ! class_exists( 'WOOCS' ) ) {
			return;
		}

		global $WOOCS;

		$key_cur = 'name';
		if ( $settings['currencies'] == 'description' ) {
			$key_cur = 'description';
		}

		$currencies    = $WOOCS->get_currencies();
		$currency_list = array();
		foreach ( $currencies as $key => $currency ) {
			if ( $WOOCS->current_currency == $key ) {
				array_unshift(
					$currency_list, sprintf(
						'<li class="actived"><a href="#" class="woocs_flag_view_item woocs_flag_view_item_current" data-currency="%s">%s</a></li>',
						esc_attr( $currency['name'] ),
						esc_html( $currency[$key_cur] )
					)
				);
			} else {
				$currency_list[] = sprintf(
					'<li><a href="#" class="woocs_flag_view_item" data-currency="%s">%s</a></li>',
					esc_attr( $currency['name'] ),
					esc_html( $currency[$key_cur] )
				);
			}
		}

		return sprintf(
			'<span class="current">%s</span>' .
			'<div class="currency-content"><ul>%s</ul></div>',
			$currencies[$WOOCS->current_currency][$key_cur],
			implode( "\n\t", $currency_list )
		);

	}

	function alg_currencies( $settings ) {
		if ( ! class_exists( 'Alg_WC_Currency_Switcher' ) ) {
			return '';
		}

		$function_currencies    = alg_get_enabled_currencies();
		$currencies             = get_woocommerce_currencies();
		$selected_currency      = alg_get_current_currency_code();
		$selected_currency_name = '';
		$currency_list          = array();
		$first_link             = '';
		foreach ( $function_currencies as $currency_code ) {
			if ( isset( $currencies[$currency_code] ) ) {

				$the_text = $this->alg_format_currency_switcher( $currencies[$currency_code], $currency_code, false );
				$the_name = $settings['currencies'] == 'description' ? $the_text : $currency_code;
				$the_link = '<li><a id="alg_currency_' . $currency_code . '" href="' . add_query_arg( 'alg_currency', $currency_code ) . '">' . $the_name . '</a></li>';
				if ( $currency_code != $selected_currency ) {
					$currency_list[] = $the_link;
				} else {
					$first_link             = $the_link;
					$selected_currency_name = $the_name;
				}
			}
		}
		if ( '' != $first_link ) {
			$currency_list = array_merge( array( $first_link ), $currency_list );
		}

		if ( ! empty( $currency_list ) && ! empty( $selected_currency_name ) ) {
			return sprintf(
				'<span class="current">%s</span>' .
				'<ul>%s</ul>',
				$selected_currency_name,
				implode( "\n\t", $currency_list )
			);
		}

		return '';

	}

	function alg_format_currency_switcher( $currency_name, $currency_code, $check_is_product = true ) {
		$product_price = '';
		if ( ! $check_is_product || ( $check_is_product && is_product() ) ) {
			$_product = wc_get_product();
			if ( $_product ) {
				$product_price = alg_get_product_price_html_by_currency( $_product, $currency_code );
			}
		}
		$replaced_values = array(
			'%currency_name%'   => $currency_name,
			'%currency_code%'   => $currency_code,
			'%currency_symbol%' => get_woocommerce_currency_symbol( $currency_code ),
			'%product_price%'   => $product_price,
		);

		return str_replace(
			array_keys( $replaced_values ),
			array_values( $replaced_values ),
			get_option( 'alg_currency_switcher_format', '%currency_name%' )
		);
	}

	protected function _content_template() {

	}
}
