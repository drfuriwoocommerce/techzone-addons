<?php

namespace TeckzoneAddons\Elementor\Widgets\Headers;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Main_Menu extends Widget_Base {

	public function get_name() {
		return 'techzone-main-menu';
	}

	public function get_title() {
		return esc_html__( 'Main Menu', 'teckzone' );
	}

	public function get_icon() {
		return 'icon-menu';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'site_header' ];
	}

	public function get_script_depends() {
		return [
			'techzone-elementor'
		];
	}

	protected $nav_menu_index = 1;

	protected function get_nav_menu_index() {
		return $this->nav_menu_index ++;
	}

	private function get_available_menus() {
		$menus = wp_get_nav_menus();

		$options = [ ];

		foreach ( $menus as $menu ) {
			$options[$menu->slug] = $menu->name;
		}

		return $options;
	}

	protected function _register_controls() {
		$this->start_controls_section(
			'menu_department_section',
			[
				'label' => esc_html__( 'Layout', 'teckzone' ),
			]
		);

		$menus = $this->get_available_menus();

		if ( ! empty( $menus ) ) {
			$this->add_control(
				'menu',
				[
					'label'        => __( 'Menu', 'teckzone' ),
					'type'         => Controls_Manager::SELECT,
					'options'      => $menus,
					'default'      => array_keys( $menus )[0],
					'save_default' => true,
					'separator'    => 'after',
					'description'  => sprintf( __( 'Go to the <a href="%s" target="_blank">Menus screen</a> to manage your menus.', 'teckzone' ), admin_url( 'nav-menus.php' ) ),
				]
			);
		} else {
			$this->add_control(
				'menu',
				[
					'type'            => Controls_Manager::RAW_HTML,
					'raw'             => '<strong>' . __( 'There are no menus in your site.', 'teckzone' ) . '</strong><br>' . sprintf( __( 'Go to the <a href="%s" target="_blank">Menus screen</a> to create one.', 'teckzone' ), admin_url( 'nav-menus.php?action=edit&menu=0' ) ),
					'separator'       => 'after',
					'content_classes' => 'elementor-panel-alert elementor-panel-alert-info',
				]
			);
		}

		$this->add_responsive_control(
			'general_align',
			[
				'label'       => esc_html__( 'Align', 'teckzone' ),
				'type'        => Controls_Manager::CHOOSE,
				'label_block' => false,
				'options'     => [
					'left'    => [
						'title' => esc_html__( 'Left', 'teckzone' ),
						'icon'  => 'eicon-h-align-left',
					],
					'center'  => [
						'title' => esc_html__( 'Center', 'teckzone' ),
						'icon'  => 'eicon-h-align-center',
					],
					'right'   => [
						'title' => esc_html__( 'Right', 'teckzone' ),
						'icon'  => 'eicon-h-align-right',
					],
					'justify' => [
						'title' => __( 'Stretch', 'teckzone' ),
						'icon'  => 'eicon-h-align-stretch',
					],
				],
				'default'     => 'left',
				'devices'     => [ 'desktop', 'tablet' ],
			]
		);

		$this->add_control(
			'indicator',
			[
				'label'     => __( 'Submenu Indicator', 'teckzone' ),
				'type'      => Controls_Manager::SELECT,
				'default'   => 'chevron',
				'options'   => [
					'none'    => __( 'None', 'teckzone' ),
					'classic' => __( 'Classic', 'teckzone' ),
					'chevron' => __( 'Chevron', 'teckzone' ),
					'plus'    => __( 'Plus', 'teckzone' ),
				],
				'separator' => 'before',
			]
		);

		$this->end_controls_section();

		// Style Section
		$this->section_style();
	}

	/**
	 * Section Style
	 */
	protected function section_style() {
		$this->section_menu_style();
		$this->section_submenu_style();
		$this->section_submenu_indicator();
	}

	protected function section_menu_style() {
		$this->start_controls_section(
			'section_menu_style',
			[
				'label' => __( 'Menu', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'style_menu_typography',
				'selector' => '{{WRAPPER}} .teckzone-main-menu > ul > li > a',
			]
		);

		// Separator
		$this->add_control(
			'sep_style',
			[
				'label'        => __( 'Separator', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();

		$this->add_control(
			'menu_sep',
			[
				'label'     => __( 'Menu Separator', 'teckzone' ),
				'type'      => Controls_Manager::SELECT,
				'default'   => 'block',
				'options'   => [
					'block' => __( 'Show', 'teckzone' ),
					'none'  => __( 'Hide', 'teckzone' ),
				],
				'selectors' => [
					'{{WRAPPER}} .main-navigation ul.menu > li > a .menu-separator'                   => 'display: {{VALUE}}',
					'{{WRAPPER}} .main-navigation ul.tz-nav-menu > li > a .menu-separator'            => 'display: {{VALUE}}',
					'{{WRAPPER}} .main-navigation ul.menu > li:last-child > a .menu-separator'        => 'display: none',
					'{{WRAPPER}} .main-navigation ul.tz-nav-menu > li:last-child > a .menu-separator' => 'display: none',
					'{{WRAPPER}} .main-navigation ul.tz-nav-menu > li.last-child > a .menu-separator' => 'display: none',
				],
			]
		);

		$this->add_responsive_control(
			'sep_height',
			[
				'label'      => __( 'Height', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px', '%' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
					'%'  => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .main-navigation ul.menu > li > a .menu-separator'        => 'height: {{SIZE}}{{UNIT}}',
					'{{WRAPPER}} .main-navigation ul.tz-nav-menu > li > a .menu-separator' => 'height: {{SIZE}}{{UNIT}}',
				],
				'condition'  => [
					'menu_sep' => 'block',
				],
				'devices'    => [ 'desktop', 'tablet' ],
			]
		);
		$this->add_control(
			'sep_bg',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .main-navigation ul.menu > li > a .menu-separator'        => 'background-color: {{VALUE}}',
					'{{WRAPPER}} .main-navigation ul.tz-nav-menu > li > a .menu-separator' => 'background-color: {{VALUE}}',
				],
				'condition' => [
					'menu_sep' => 'block',
				],
			]
		);
		$this->end_popover();

		// Magic line
		$this->add_control(
			'magic_line_style',
			[
				'label'        => __( 'Magic line', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();

		$this->add_control(
			'magic_line',
			[
				'label'     => __( 'Menu Separator', 'teckzone' ),
				'type'      => Controls_Manager::SELECT,
				'default'   => 'none',
				'options'   => [
					'block' => __( 'Show', 'teckzone' ),
					'none'  => __( 'Hide', 'teckzone' ),
				],
				'selectors' => [
					'{{WRAPPER}} .main-navigation li.tz-menu-item__magic-line' => 'display: {{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'magic_line_height',
			[
				'label'      => __( 'Height', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 5,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .main-navigation li.tz-menu-item__magic-line' => 'height: {{SIZE}}{{UNIT}}',
				],
				'condition'  => [
					'magic_line' => 'block',
				],
				'devices'    => [ 'desktop', 'tablet' ],
			]
		);
		$this->add_control(
			'magic_line_bg',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .main-navigation li.tz-menu-item__magic-line' => 'background-color: {{VALUE}}',
				],
				'condition' => [
					'magic_line' => 'block',
				],
			]
		);
		$this->end_popover();

		// Color
		$this->start_controls_tabs( 'settings_menu_item' );

		$this->start_controls_tab( 'style_item_normal', [ 'label' => esc_html__( 'Normal', 'teckzone' ) ] );
		$this->add_control(
			'menu_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-main-menu .tz-nav-menu > li > a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();
		$this->start_controls_tab( 'style_item_active', [ 'label' => esc_html__( 'Active', 'teckzone' ) ] );

		$this->add_control(
			'menu_active_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .main-navigation ul.tz-nav-menu > li.current-menu-item > a'     => 'color: {{VALUE}}',
					'{{WRAPPER}} .main-navigation ul.tz-nav-menu > li.current_page_item > a'     => 'color: {{VALUE}}',
					'{{WRAPPER}} .main-navigation ul.tz-nav-menu > li.current_page_ancestor > a' => 'color: {{VALUE}}',
					'{{WRAPPER}} .main-navigation ul.tz-nav-menu > li.current-menu-ancestor > a' => 'color: {{VALUE}}',
					'{{WRAPPER}} .main-navigation ul.tz-nav-menu > li:hover > a'                 => 'color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->add_control(
			'menu_divider_1',
			[
				'type' => Controls_Manager::DIVIDER,
			]
		);

		$this->add_responsive_control(
			'padding_horizontal_menu_item',
			[
				'label'     => __( 'Horizontal Padding', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'devices'   => [ 'desktop', 'tablet' ],
				'selectors' => [
					'{{WRAPPER}} .main-navigation ul.tz-nav-menu > li'                     => 'padding-left: {{SIZE}}{{UNIT}}; padding-right: {{SIZE}}{{UNIT}}',
					'{{WRAPPER}} .main-navigation ul.menu > li > a .menu-separator'        => 'right: -{{SIZE}}{{UNIT}}',
					'{{WRAPPER}} .main-navigation ul.tz-nav-menu > li > a .menu-separator' => 'right: -{{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'padding_vertical_menu_item',
			[
				'label'     => __( 'Vertical Padding', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'devices'   => [ 'desktop', 'tablet' ],
				'selectors' => [
					'{{WRAPPER}} .main-navigation ul.tz-nav-menu > li'                        => 'padding-top: {{SIZE}}{{UNIT}}; padding-bottom: {{SIZE}}{{UNIT}}',
					'{{WRAPPER}} .main-navigation li.menu-item-has-children > a:before'       => 'top: calc( 100% + {{SIZE}}{{UNIT}} + 5px )',
					'{{WRAPPER}} .main-navigation li.menu-item-has-children:hover > a:before' => 'top: calc( 100% + {{SIZE}}{{UNIT}} - 5px )',
					'{{WRAPPER}} .main-navigation li.dropdown > a:before'                     => 'top: calc( 100% + {{SIZE}}{{UNIT}} + 5px )',
					'{{WRAPPER}} .main-navigation li.dropdown:hover > a:before'               => 'top: calc( 100% + {{SIZE}}{{UNIT}} - 5px )',
				],
			]
		);

		$this->add_control(
			'menu_divider_2',
			[
				'type' => Controls_Manager::DIVIDER,
			]
		);

		// Item
		$this->start_controls_tabs( 'menu_item_style' );

		$this->start_controls_tab( 'style_first_item', [ 'label' => esc_html__( 'First Item', 'teckzone' ) ] );
		$this->add_responsive_control(
			'padding_first_item',
			[
				'label'     => __( 'Padding Left', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'devices'   => [ 'desktop', 'tablet' ],
				'selectors' => [
					'{{WRAPPER}} .main-navigation ul.tz-nav-menu > li:first-child' => 'padding-left: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->end_controls_tab();
		$this->start_controls_tab( 'style_last_item', [ 'label' => esc_html__( 'Last Item', 'teckzone' ) ] );

		$this->add_responsive_control(
			'padding_last_item',
			[
				'label'     => __( 'Padding Right', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'devices'   => [ 'desktop', 'tablet' ],
				'selectors' => [
					'{{WRAPPER}} .main-navigation ul.tz-nav-menu > li:last-child' => 'padding-right: {{SIZE}}{{UNIT}}',
					'{{WRAPPER}} .main-navigation ul.tz-nav-menu > li.last-child' => 'padding-right: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function section_submenu_style() {
		$this->start_controls_section(
			'section_submenu_style',
			[
				'label' => __( 'Submenu', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'style_submenu_wrapper',
			[
				'label' => __( 'Wrapper', 'teckzone' ),
				'type'  => Controls_Manager::HEADING,
			]
		);
		$this->add_responsive_control(
			'submenu_wrapper_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'devices'    => [ 'desktop', 'tablet' ],
				'selectors'  => [
					'{{WRAPPER}} .main-navigation ul ul' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'drmenu_bk_color',
			[
				'label'     => __( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .main-navigation ul ul'                                => 'background-color: {{VALUE}};',
					'{{WRAPPER}} .main-navigation li.menu-item-has-children > a:before' => 'background-color: {{VALUE}};',
					'{{WRAPPER}} .main-navigation li.dropdown > a:before'               => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'wrapper_border_color',
			[
				'label'     => esc_html__( 'Border Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .main-navigation ul ul'                                => 'border-color: {{VALUE}};',
					'{{WRAPPER}} .main-navigation li.menu-item-has-children > a:before' => 'border-bottom-color: {{VALUE}};border-left-color: {{VALUE}}',
					'{{WRAPPER}} .main-navigation li.dropdown > a:before'               => 'border-bottom-color: {{VALUE}};border-left-color: {{VALUE}}',
				],
			]
		);

		// Item
		$this->add_control(
			'style_submenu_item',
			[
				'label'     => __( 'Item', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		$this->add_responsive_control(
			'padding_horizontal_submenu_item',
			[
				'label'     => __( 'Horizontal Padding', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'devices'   => [ 'desktop', 'tablet' ],
				'selectors' => [
					'{{WRAPPER}} .main-navigation li li' => 'padding-left: {{SIZE}}{{UNIT}}; padding-right: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'padding_vertical_submenu_item',
			[
				'label'     => __( 'Vertical Padding', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'devices'   => [ 'desktop', 'tablet' ],
				'selectors' => [
					'{{WRAPPER}} .main-navigation li li' => 'padding-top: {{SIZE}}{{UNIT}}; padding-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'style_submenu_typography',
				'selector' => '{{WRAPPER}} .main-navigation li li a',
			]
		);

		$this->start_controls_tabs( 'style_submenu_item_style' );

		$this->start_controls_tab( 'style_submenu_normal', [ 'label' => esc_html__( 'Normal', 'teckzone' ) ] );

		$this->add_control(
			'submenu_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .main-navigation li li a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'style_submenu_active', [ 'label' => esc_html__( 'Active', 'teckzone' ) ] );

		$this->add_control(
			'submenu_active_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .main-navigation li:not(.is-mega-menu) ul.dropdown-submenu > li.current-menu-item > a'     => 'color: {{VALUE}}',
					'{{WRAPPER}} .main-navigation li:not(.is-mega-menu) ul.dropdown-submenu > li.current_page_item) > a'    => 'color: {{VALUE}}',
					'{{WRAPPER}} .main-navigation li:not(.is-mega-menu) ul.dropdown-submenu > li.current_page_ancestor > a' => 'color: {{VALUE}}',
					'{{WRAPPER}} .main-navigation li:not(.is-mega-menu) ul.dropdown-submenu > li.current-menu-ancestor > a' => 'color: {{VALUE}}',
					'{{WRAPPER}} .main-navigation li:not(.is-mega-menu) ul.dropdown-submenu > li:hover > a'                 => 'color: {{VALUE}}',
				],
			]
		);
		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function section_submenu_indicator() {
		$this->start_controls_section(
			'submenu_indicator_style',
			[
				'label' => __( 'Submenu Indicator', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'indicator_font_size',
			[
				'label'      => __( 'Font Size', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .main-navigation li.menu-item-has-children > a:after' => 'font-size: {{SIZE}}{{UNIT}}',
					'{{WRAPPER}} .main-navigation li.dropdown > a:after'               => 'font-size: {{SIZE}}{{UNIT}}',
				],
				'conditions' => [
					'terms' => [
						[
							'name'     => 'indicator',
							'operator' => '!=',
							'value'    => 'none',
						],
					],
				],
				'devices'    => [ 'desktop', 'tablet' ],
			]
		);

		$this->add_responsive_control(
			'indicator_spacing',
			[
				'label'      => __( 'Spacing', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors'  => [
					'{{WRAPPER}} .main-navigation li.menu-item-has-children > a' => 'padding-right: {{SIZE}}{{UNIT}}',
					'{{WRAPPER}} .main-navigation li.dropdown > a'               => 'padding-right: {{SIZE}}{{UNIT}}',
				],
				'conditions' => [
					'terms' => [
						[
							'name'     => 'indicator',
							'operator' => '!=',
							'value'    => 'none',
						],
					],
				],
				'devices'    => [ 'desktop', 'tablet' ],
			]
		);
		$this->end_controls_section();
	}

	protected function render() {
		$settings = $this->get_settings_for_display();

		$class = [
			'teckzone-main-menu',
			'teckzone-main-menu--' . $settings['general_align'],
			'teckzone-main-menu--indicator-' . $settings['indicator'],
			'main-navigation'

		];

		$this->add_render_attribute( 'wrapper', 'class', $class );

		$args = [
			'echo'        => false,
			'menu'        => $settings['menu'],
			'menu_class'  => 'tz-nav-menu menu',
			'menu_id'     => 'menu-' . $this->get_nav_menu_index() . '-' . $this->get_id(),
			'fallback_cb' => '__return_empty_string',
			'container'   => '',
			'walker'      => new \Teckzone_Mega_Menu_Walker(),
		];

		// General Menu.
		$menu_html = wp_nav_menu( $args );

		if ( empty( $menu_html ) ) {
			return;
		}

		echo sprintf(
			'<div %s>%s</div>',
			$this->get_render_attribute_string( 'wrapper' ),
			$menu_html
		);
	}

	protected function _content_template() {

	}
}