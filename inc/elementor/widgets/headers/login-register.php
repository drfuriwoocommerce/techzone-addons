<?php

namespace TeckzoneAddons\Elementor\Widgets\Headers;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Icon Box widget
 */
class Login_Register extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'techzone-login-regis';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Teckzone - Login Register', 'teckzone' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-lock-user';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'site_header' ];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->section_content();
		$this->section_style();
	}

	/**
	 * Section Content
	 */
	protected function section_content() {
		$this->start_controls_section(
			'section_content',
			[ 'label' => esc_html__( 'Content', 'teckzone' ) ]
		);

		$this->start_controls_tabs( 'content_user_login' );

		$this->start_controls_tab( 'content_not_login', [ 'label' => esc_html__( 'Not Logged In', 'teckzone' ) ] );

		$this->add_control(
			'icon',
			[
				'label'   => esc_html__( 'Icon', 'teckzone' ),
				'type'    => Controls_Manager::ICONS,
				'default' => [
					'value'   => 'icon-user',
					'library' => 'linearicons',
				],
			]
		);

		$this->add_control(
			'link', [
				'label'         => esc_html__( 'Link', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);

		$this->add_control(
			'title',
			[
				'label'       => esc_html__( 'Title', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'Login', 'teckzone' ),
				'label_block' => true,
				'separator'   => 'before',
			]
		);

		$this->add_control(
			'after_title',
			[
				'label'       => esc_html__( 'After Title', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'Register', 'teckzone' ),
				'label_block' => true,
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'content_login', [ 'label' => esc_html__( 'Logged in', 'teckzone' ) ] );

		$this->add_control(
			'preamble',
			[
				'label'       => esc_html__( 'Preamble', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'Hi', 'teckzone' ),
				'label_block' => true,
			]
		);

		$this->add_control(
			'icon_logged',
			[
				'label'   => esc_html__( 'Icon Logout', 'teckzone' ),
				'type'    => Controls_Manager::ICONS,
				'default' => [
					'value'   => 'icon-enter-left',
					'library' => 'linearicons',
				],
			]
		);

		$this->add_control(
			'enable_preamble',
			[
				'label'     => esc_html__( 'Enable Preamble', 'teckzone' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_off' => esc_html__( 'Off', 'teckzone' ),
				'label_on'  => esc_html__( 'On', 'teckzone' ),
				'default'   => 'yes'
			]
		);

		$this->add_control(
			'title_logout',
			[
				'label'       => esc_html__( 'Logout Title', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => esc_html__( 'Logout', 'teckzone' ),
				'label_block' => true,
				'separator'   => 'after',
			]
		);

		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'link_name',
			[
				'label'       => esc_html__( 'Link Name', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => __( 'This is the name', 'teckzone' ),
				'placeholder' => esc_html__( 'Enter your name', 'teckzone' ),
				'label_block' => true,
			]
		);

		$repeater->add_control(
			'link_custom', [
				'label'         => esc_html__( 'Link', 'teckzone' ),
				'type'          => Controls_Manager::URL,
				'placeholder'   => esc_html__( 'https://your-link.com', 'teckzone' ),
				'show_external' => true,
				'default'       => [
					'url'         => '#',
					'is_external' => false,
					'nofollow'    => false,
				],
			]
		);

		$this->add_control(
			'group_setting',
			[
				'label'         => esc_html__( 'Links Group', 'teckzone' ),
				'type'          => Controls_Manager::REPEATER,
				'fields'        => $repeater->get_controls(),
				'default'       => [
					[
						'link_name'   => __( 'This is the name', 'teckzone' ),
						'link_custom' => '#',
					], [
						'link_name'   => __( 'This is the name 2', 'teckzone' ),
						'link_custom' => '#',
					], [
						'link_name'   => __( 'This is the name 3', 'teckzone' ),
						'link_custom' => '#',
					],
				],
				'title_field'   => '{{{ link_name }}}',
				'prevent_empty' => false,
			]
		);

		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	/**
	 * Section Style
	 */
	protected function section_style() {
		$this->section_general_style();
		$this->section_not_loggin_style();
		$this->section_loggin_style();
		$this->section_drop_style();
	}

	protected function section_general_style() {
		$this->start_controls_section(
			'style_general',
			[
				'label' => __( 'General', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		// Separator
		$this->add_control(
			'sep_style',
			[
				'label'        => __( 'Separator', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();
		$this->add_responsive_control(
			'item_sep',
			[
				'label'       => esc_html__( 'Separator', 'teckzone' ),
				'label_block' => true,
				'type'        => Controls_Manager::SELECT,
				'options'     => [
					'block' => esc_html__( 'Show', 'teckzone' ),
					'none'  => esc_html__( 'Hide', 'teckzone' ),
				],
				'default'     => 'none',
				'selectors'   => [
					'{{WRAPPER}} .teckzone-login-regis:after' => 'display: {{VALUE}}',
				],
			]
		);
		$this->add_responsive_control(
			'sep_height',
			[
				'label'      => __( 'Height', 'teckzone' ),
				'type'       => Controls_Manager::SLIDER,
				'range'      => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
					'%'  => [
						'min' => 0,
						'max' => 100,
					],
				],
				'size_units' => [ '%', 'px' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-login-regis:after' => 'height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'sep_bg',
			[
				'label'     => esc_html__( 'Background Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-login-regis:after' => 'background-color: {{VALUE}};',
				],
			]
		);
		$this->end_popover();

		$this->end_controls_section();
	}

	/**
	 * Element in Tab Style
	 *
	 * General
	 */

	protected function section_not_loggin_style() {
		$this->start_controls_section(
			'section_not_loggin_style',
			[
				'label' => __( 'Not Logged in', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'notlog_margin',
			[
				'label'      => esc_html__( 'Margin', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-not-login' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'log_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-not-login' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		// Elements
		$this->add_control(
			'login_elements',
			[
				'label'     => __( 'Elements', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->start_controls_tabs( 'settings_not_loggin' );

		$this->start_controls_tab( 'style_icon_not_log', [ 'label' => esc_html__( 'Icon', 'teckzone' ) ] );

		$this->add_responsive_control(
			'icon_notlog_spacing',
			[
				'label'              => esc_html__( 'Margin', 'teckzone' ),
				'type'               => Controls_Manager::DIMENSIONS,
				'allowed_dimensions' => [ 'top', 'right' ],
				'size_units'         => [ 'px', '%' ],
				'default'            => [ ],
				'selectors'          => [
					'{{WRAPPER}} .teckzone-not-login .user-icon' => 'margin-top: {{TOP}}{{UNIT}};margin-right: {{RIGHT}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'icon_notlog_size',
			[
				'label'     => __( 'Font Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-not-login .user-icon'     => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .teckzone-not-login .user-icon svg' => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'icon_notlog_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-not-login .user-icon'     => 'color: {{VALUE}};',
					'{{WRAPPER}} .teckzone-not-login .user-icon svg' => 'fill: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab( 'style_title_not_log', [ 'label' => esc_html__( 'Title', 'teckzone' ) ] );

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label'    => esc_html__( 'Before Title', 'teckzone' ),
				'name'     => 'bf_title_not_log',
				'selector' => '{{WRAPPER}} .teckzone-login-regis .bf-title',
			]
		);

		$this->add_control(
			'bf_title_not_log_color',
			[
				'label'     => esc_html__( 'Title Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-login-regis .bf-title' => 'color: {{VALUE}}',
				],
				'separator' => 'after',
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label'    => esc_html__( 'After Title', 'teckzone' ),
				'name'     => 'af_title_not_log',
				'selector' => '{{WRAPPER}} .teckzone-login-regis .af-title',
			]
		);

		$this->add_control(
			'af_title_not_log_color',
			[
				'label'     => esc_html__( 'Title Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-login-regis .af-title' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'af_title_not_log_spacing',
			[
				'label'     => __( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-login-regis .af-title' => 'margin-top: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function section_loggin_style() {
		$this->start_controls_section(
			'section_loggin_style',
			[
				'label' => __( 'Logged in', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_responsive_control(
			'log_margin',
			[
				'label'      => esc_html__( 'Margin', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-logged-in' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'logaccount_padding',
			[
				'label'      => esc_html__( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .teckzone-logged-in .login-account' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		// Elements
		$this->add_control(
			'elements',
			[
				'label'     => __( 'Elements', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		$this->start_controls_tabs( 'settings_loggin' );

		$this->start_controls_tab( 'style_icon_loged', [ 'label' => esc_html__( 'Icon', 'teckzone' ) ] );

		$this->add_responsive_control(
			'icon_loged_spacing',
			[
				'label'              => esc_html__( 'Margin', 'teckzone' ),
				'type'               => Controls_Manager::DIMENSIONS,
				'allowed_dimensions' => [ 'top', 'right' ],
				'size_units'         => [ 'px', '%' ],
				'default'            => [ ],
				'selectors'          => [
					'{{WRAPPER}} .teckzone-logged-in .user-icon' => 'margin-top: {{TOP}}{{UNIT}};margin-right: {{RIGHT}}{{UNIT}}',
				],
			]
		);

		$this->add_responsive_control(
			'icon_loged_size',
			[
				'label'     => __( 'Font Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-logged-in .user-icon'     => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .teckzone-logged-in .user-icon svg' => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'icon_loged_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-logged-in .user-icon'     => 'color: {{VALUE}};',
					'{{WRAPPER}} .teckzone-logged-in .user-icon svg' => 'fill: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		// Pre
		$this->start_controls_tab( 'style_preamble_log', [ 'label' => esc_html__( 'Preamble', 'teckzone' ) ] );

		$this->add_control(
			'preamble_spacing',
			[
				'label'     => esc_html__( 'Spacing Left', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-login-regis .preamble' => 'margin-left: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label'    => esc_html__( 'Before Title', 'teckzone' ),
				'name'     => 'preamble_log',
				'selector' => '{{WRAPPER}} .teckzone-login-regis .preamble',
			]
		);

		$this->add_control(
			'preamble_log_color',
			[
				'label'     => esc_html__( 'Title Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-login-regis .preamble' => 'color: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		// Dropdown
		$this->start_controls_tab( 'style_links_log', [ 'label' => esc_html__( 'Dropdown', 'teckzone' ) ] );
		$this->add_control(
			'normal_items_style',
			[
				'label' => __( 'Normal Items', 'teckzone' ),
				'type'  => Controls_Manager::HEADING,
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label'    => esc_html__( 'Typography', 'teckzone' ),
				'name'     => 'link_log',
				'selector' => '{{WRAPPER}} .teckzone-login-regis li:not(.logout) a',
			]
		);

		$this->add_control(
			'link_log_color',
			[
				'label'     => esc_html__( 'Title Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-login-regis li:not(.logout)' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'link_log_spacing',
			[
				'label'     => esc_html__( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-login-regis li:not(.logout)' => 'margin-bottom: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->add_control(
			'logout_item',
			[
				'label'     => __( 'Logout Item', 'teckzone' ),
				'type'      => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'label'    => esc_html__( 'Text', 'teckzone' ),
				'name'     => 'title_logout',
				'selector' => '{{WRAPPER}} .teckzone-login-regis .logout a',
			]
		);

		$this->add_control(
			'title_logout_color',
			[
				'label'     => esc_html__( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .teckzone-login-regis .logout' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'title_logout_spacing',
			[
				'label'     => esc_html__( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-login-regis .header-account--content .logout' => 'padding-top: {{SIZE}}{{UNIT}}',
				],
			]
		);

		// Icon
		$this->add_control(
			'logout_item_icon',
			[
				'label'        => __( 'Icon', 'teckzone' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'teckzone' ),
				'label_on'     => __( 'Custom', 'teckzone' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();

		$this->add_responsive_control(
			'icon_log_spacing',
			[
				'label'     => __( 'Icon Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 200,
						'min' => 10,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .teckzone-login-regis .logout .teckzone-icon' => 'margin-right: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_responsive_control(
			'icon_log_size',
			[
				'label'     => __( 'Icon Font Size', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'default'   => [ ],
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-login-regis .logout .teckzone-icon'     => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .teckzone-login-regis .logout .teckzone-icon svg' => 'width: {{SIZE}}{{UNIT}};height: {{SIZE}}{{UNIT}};',
				],
			]
		);
		$this->add_control(
			'icon_log_color',
			[
				'label'     => __( 'Icon Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .teckzone-login-regis .logout .teckzone-icon'     => 'color: {{VALUE}};',
					'{{WRAPPER}} .teckzone-login-regis .logout .teckzone-icon svg' => 'fill: {{VALUE}};',
				],
			]
		);

		$this->end_popover();

		$this->end_controls_tab();
		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	protected function section_drop_style() {
		$this->start_controls_section(
			'style_drop',
			[
				'label' => __( 'Drop Content', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'link_distance',
			[
				'label'     => esc_html__( 'Distance to top', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .teckzone-login-regis .header-account' => 'border-top-width: {{SIZE}}{{UNIT}}',
					'{{WRAPPER}} .teckzone-logged-in:before'            => 'top: calc( 120% + {{SIZE}}{{UNIT}} )',
					'{{WRAPPER}} .teckzone-logged-in:hover:before'      => 'top: calc( 100% + {{SIZE}}{{UNIT}} )',
				],
			]
		);

		$this->end_controls_section();

	}


	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$this->add_render_attribute(
			'wrapper', 'class', [
				'teckzone-login-regis',
			]
		);

		$icon = $title = $icon_logged = $title_logout = $preamble = '';

		// Not log in
		if ( $settings['icon'] && ! empty( $settings['icon']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['icon'], [ 'aria-hidden' => 'true' ] );
			$icon = '<span class="user-icon teckzone-icon">' . ob_get_clean() . '</span>';
		}

		if ( $settings['title'] ) {

			$title_1 = '<span class="bf-title">' . $settings['title'] . '</span>';
			$title_2 = '<span class="af-title">' . $settings['after_title'] . '</span>';
			$title   = '<span class="title">' . $title_1 . $title_2 . '</span>';
		}

		$output = $icon . $title;

		// Logged in
		if ( $settings['icon_logged'] && ! empty( $settings['icon_logged']['value'] ) && \Elementor\Icons_Manager::is_migration_allowed() ) {
			ob_start();
			\Elementor\Icons_Manager::render_icon( $settings['icon_logged'], [ 'aria-hidden' => 'true' ] );
			$icon_logged = '<span class="teckzone-icon">' . ob_get_clean() . '</span>';
		}

		$outputs = [ ];

		$elements = $settings['group_setting'];

		$outputs[] = '<ul class="header-account--content">';
		if ( ! empty ( $elements ) ) {

			foreach ( $elements as $index => $item ) {
				$link_name = isset( $item['link_name'] ) && ! empty( $item['link_name'] ) ? sprintf( '<span class="link-name">%s</span>', $item['link_name'] ) : '';

				$link_key  = 'link_' . $index;
				$link_html = $item['link_custom']['url'] ? sprintf( '<li>%s</li>', $this->get_link_control( $link_key, $item['link_custom'], $link_name, '' ) ) : sprintf( '<li>%s</li>', $link_name );

				$outputs[] = $link_html;
			}
		}
		$account = get_permalink( get_option( 'woocommerce_myaccount_page_id' ) );

		$outputs[] = '<li class="logout"><a href="' . esc_url( wp_logout_url( $account ) ) . '">' . $icon_logged . $settings['title_logout'] . '</a></li>';
		$outputs[] = '</ul>';

		if ( is_user_logged_in() ) {

			$this->add_render_attribute(
				'wrapper', 'class', [
					'teckzone-logged-in',
				]
			);
			$user_id = get_current_user_id();
			$author  = get_user_by( 'id', $user_id );

			$name_user   = $settings['enable_preamble'] ? sprintf( '<span class="preamble"> %s, %s!</span>', $settings['preamble'], $author->display_name ) : '';
			$output_html = sprintf( '<a href="%s" class="login-account">%s %s</a>', $account, $icon, $name_user );
			$output_html .= sprintf( '<div class="header-account">%s</div>', implode( '', $outputs ) );
		} else {
			$this->add_render_attribute(
				'wrapper', 'class', [
					'teckzone-not-login',
				]
			);

			$output_html = $settings['link']['url'] ? $this->get_link_control( 'link', $settings['link'], $output, '' ) : $output;
		}

		echo sprintf(
			'<div %s>%s</div>',
			$this->get_render_attribute_string( 'wrapper' ),
			$output_html
		);

	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected
	function _content_template() {
	}

	protected function get_link_control( $link_key, $url, $content, $attr = [ ] ) {
		$attr_default = [
			'href' => $url['url'] ? $url['url'] : '#',
		];

		if ( $url['is_external'] ) {
			$attr_default['target'] = '_blank';
		}

		if ( $url['nofollow'] ) {
			$attr_default['rel'] = 'nofollow';
		}

		$attr = wp_parse_args( $attr, $attr_default );

		$this->add_render_attribute( $link_key, $attr );

		return sprintf( '<a %1$s>%2$s</a>', $this->get_render_attribute_string( $link_key ), $content );
	}
}