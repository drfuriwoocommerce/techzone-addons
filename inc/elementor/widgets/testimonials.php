<?php

namespace TeckzoneAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Image_Size;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Border;
use Elementor\Widget_Base;
use TeckzoneAddons\Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Icon Box widget
 */
class Testimonials extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'techzone-testimonials';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Teckzone - Testimonials', 'teckzone' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-comments';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'teckzone' ];
	}

	public function get_script_depends() {
		return [
			'techzone-elementor'
		];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->section_content();
		$this->section_style();
	}

	/**
	 * Section Content
	 */
	protected function section_content() {
		$this->section_content_content();
		$this->section_carousel_content();
	}

	protected function section_content_content() {
		$this->start_controls_section(
			'section_content',
			[ 'label' => esc_html__( 'Content', 'teckzone' ) ]
		);

		$repeater = new \Elementor\Repeater();

		$repeater->add_control(
			'avatar', [
				'label'     => esc_html__( 'Avatar', 'teckzone' ),
				'type'      => Controls_Manager::MEDIA,
				'default'   => [
					'url' => 'https://via.placeholder.com/70/f8f8f8?text=70x70+Image',
				],
			]
		);

		$repeater->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name'      => 'avatar',
				'default'   => 'full',
				'separator' => 'none',
			]
		);

		$repeater->add_control(
			'logo', [
				'label'     => esc_html__( 'Logo', 'teckzone' ),
				'type'      => Controls_Manager::MEDIA,
				'default'   => [
					'url' => 'https://via.placeholder.com/110x60/f8f8f8?text=110x60+Logo',
				],
			]
		);

		$repeater->add_group_control(
			Group_Control_Image_Size::get_type(),
			[
				'name'      => 'logo',
				'default'   => 'full',
				'separator' => 'none',
			]
		);

		$repeater->add_control(
			'name',
			[
				'label'       => esc_html__( 'Name', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => '',
				'label_block' => true,
				'separator'   => 'before',
			]
		);

		$repeater->add_control(
			'location',
			[
				'label'       => esc_html__( 'Location', 'teckzone' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => '',
				'label_block' => true,
			]
		);

		$repeater->add_control(
			'desc',
			[
				'label'       => esc_html__( 'Description', 'teckzone' ),
				'type'        => Controls_Manager::TEXTAREA,
				'default'     => '',
				'label_block' => true,
			]
		);

		$this->add_control(
			'element',
			[
				'label'         => '',
				'type'          => Controls_Manager::REPEATER,
				'fields'        => $repeater->get_controls(),
				'default'       => [
					[
						'avatar'     => 'https://via.placeholder.com/70/f8f8f8?text=70x70+Image',
						'logo'       => 'https://via.placeholder.com/110x60/f8f8f8?text=110x60+Logo',
						'name'       => esc_html__( 'This is the name', 'teckzone' ),
						'location'   => esc_html__( 'Location', 'teckzone' ),
						'desc'       => esc_html__( 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'teckzone' ),
					],

					[
						'avatar'     => 'https://via.placeholder.com/70/f8f8f8?text=70x70+Image',
						'logo'       => 'https://via.placeholder.com/110x60/f8f8f8?text=110x60+Logo',
						'name'       => esc_html__( 'This is the name 2', 'teckzone' ),
						'location'   => esc_html__( 'Location 2', 'teckzone' ),
						'desc'       => esc_html__( 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'teckzone' ),
					],

					[
						'avatar'     => 'https://via.placeholder.com/70/f8f8f8?text=70x70+Image',
						'logo'       => 'https://via.placeholder.com/110x60/f8f8f8?text=110x60+Logo',
						'name'       => esc_html__( 'This is the name 3', 'teckzone' ),
						'location'   => esc_html__( 'Location 3', 'teckzone' ),
						'desc'       => esc_html__( 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'teckzone' ),
					],

					[
						'avatar'     => 'https://via.placeholder.com/70/f8f8f8?text=70x70+Image',
						'logo'       => 'https://via.placeholder.com/110x60/f8f8f8?text=110x60+Logo',
						'name'       => esc_html__( 'This is the name 4', 'teckzone' ),
						'location'   => esc_html__( 'Location 4', 'teckzone' ),
						'desc'       => esc_html__( 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.', 'teckzone' ),
					],
				],
				'prevent_empty' => false
			]
		);

		$this->end_controls_section();
	}

	protected function section_carousel_content() {
		$this->start_controls_section(
			'section_slider_settings',
			[
				'label' => esc_html__( 'Carousel Settings', 'teckzone' ),
			]
		);

		$this->add_responsive_control(
			'slidesToShow',
			[
				'label'   => esc_html__( 'Slides to show', 'teckzone' ),
				'type'    => Controls_Manager::NUMBER,
				'min'     => 1,
				'max'     => 10,
				'default' => 3,
			]
		);
		$this->add_responsive_control(
			'slidesToScroll',
			[
				'label'   => esc_html__( 'Slides to scroll', 'teckzone' ),
				'type'    => Controls_Manager::NUMBER,
				'min'     => 1,
				'max'     => 10,
				'default' => 3,
			]
		);
		$this->add_responsive_control(
			'navigation',
			[
				'label'   => esc_html__( 'Navigation', 'teckzone' ),
				'type'    => Controls_Manager::SELECT,
				'options' => [
					'both'   => esc_html__( 'Arrows and Dots', 'teckzone' ),
					'arrows' => esc_html__( 'Arrows', 'teckzone' ),
					'dots'   => esc_html__( 'Dots', 'teckzone' ),
					'none'   => esc_html__( 'None', 'teckzone' ),
				],
				'default' => 'both',
				'toggle'  => false,
			]
		);

		$this->add_control(
			'infinite',
			[
				'label'     => __( 'Infinite Loop', 'teckzone' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_off' => __( 'Off', 'teckzone' ),
				'label_on'  => __( 'On', 'teckzone' ),
				'default'   => 'yes'
			]
		);

		$this->add_control(
			'autoplay',
			[
				'label'     => __( 'Autoplay', 'teckzone' ),
				'type'      => Controls_Manager::SWITCHER,
				'label_off' => __( 'Off', 'teckzone' ),
				'label_on'  => __( 'On', 'teckzone' ),
				'default'   => 'yes'
			]
		);

		$this->add_control(
			'autoplay_speed',
			[
				'label'   => __( 'Autoplay Speed (in ms)', 'teckzone' ),
				'type'    => Controls_Manager::NUMBER,
				'default' => 3000,
				'min'     => 100,
				'step'    => 100,
			]
		);

		$this->add_control(
			'speed',
			[
				'label'       => __( 'Speed', 'teckzone' ),
				'type'        => Controls_Manager::NUMBER,
				'default'     => 800,
				'min'         => 100,
				'step'        => 100,
				'description' => esc_html__( 'Slide animation speed (in ms)', 'teckzone' ),
			]
		);


		$this->end_controls_section();
	}
		/**
	 * Section Style
	 */
	protected function section_style() {
		$this->section_general_style();
		$this->section_image_style();
		$this->section_name_style();
		$this->section_location_style();
		$this->section_desc_style();
	}

	/**
	 * Element in Tab Style
	 *
	 * General
	 */
	protected function section_general_style() {
		$this->start_controls_section(
			'section_general_style',
			[
				'label' => __( 'General', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'general_bk_color',
			[
				'label'        => esc_html__( 'Background Color', 'teckzone' ),
				'type'         => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-testimonials .box-item__wrapper' => 'background-color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'border',
				'selector' => '{{WRAPPER}} .techzone-testimonials .box-item__wrapper',
			]
		);

		$this->add_control(
			'general_border_radius',
			[
				'label' => __( 'Border Radius', 'teckzone' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}} .techzone-testimonials .box-item__wrapper' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'general_padding',
			[
				'label'      => __( 'Padding', 'teckzone' ),
				'type'       => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors'  => [
					'{{WRAPPER}} .techzone-testimonials .box-item__wrapper' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'separator' => 'before',
			]
		);
		$this->end_controls_section();
	}

	/**
	 * Element in Tab Style
	 *
	 * Icon
	 */
	protected function section_image_style() {
		$prefix = 'image';

		$this->start_controls_section(
			'section_'.$prefix.'_style',
			[
				'label' => __( 'Image', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			 $prefix .'border_radius',
			[
				'label'     => __( 'Border Radius', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'size_units' => ['%','px'],
				'range'     => [
					'px' => [
						'max' => 100,
						'min' => 10,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-testimonials .avatar img ' => 'border-radius: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			 $prefix .'_spacing',
			[
				'label'     => __( 'Margin Bottom', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 50,
						'min' => 10,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-testimonials .box-item__image ' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			 $prefix .'_spacing_padding',
			[
				'label'     => __( 'Padding Bottom', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 50,
						'min' => 10,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-testimonials .box-item__image ' => 'padding-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);



		$this->end_controls_section();
	}/**
 *
	 * Element in Tab Style
	 *
	 * Icon
	 */
	protected function section_name_style() {
		$prefix = 'name';

		$this->start_controls_section(
			'section_'.$prefix.'_style',
			[
				'label' => __( 'Name', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'name_typography',
				'selector' => '{{WRAPPER}} .techzone-testimonials .name',
			]
		);

		$this->add_responsive_control(
			 $prefix .'_spacing',
			[
				'label'     => __( 'Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 50,
						'min' => 10,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-testimonials .name ' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			$prefix.'_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-testimonials .name' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Element in Tab Style
	 *
	 * Title
	 */
	protected function section_location_style() {
		$prefix = 'location';

		$this->start_controls_section(
			'section_'.$prefix.'_style',
			[
				'label' => __( 'Location', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			$prefix.'_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-testimonials .location' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'location_typography',
				'selector' => '{{WRAPPER}} .techzone-testimonials .location',
			]
		);

		$this->add_responsive_control(
			$prefix.'_spacing',
			[
				'label'     => __( 'Bottom Spacing', 'teckzone' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 30,
						'min' => 0,
					],
				],
				'default'   => [ ],
				'selectors' => [
					'{{WRAPPER}} .techzone-testimonials .location' => 'margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Element in Tab Style
	 *
	 * Desc
	 */
	protected function section_desc_style() {
		$this->start_controls_section(
			'section_desc_style',
			[
				'label' => __( 'Description', 'teckzone' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

		$this->add_control(
			'desc_color',
			[
				'label'     => __( 'Color', 'teckzone' ),
				'type'      => Controls_Manager::COLOR,
				'default'   => '',
				'selectors' => [
					'{{WRAPPER}} .techzone-testimonials .desc' => 'color: {{VALUE}};',
				],
			]
		);
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'desc_typography',
				'selector' => '{{WRAPPER}} .techzone-testimonials .desc',
			]
		);

		$this->end_controls_section();
	}

	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$classes = [
			'teckzone-testimonials',
			'techzone-testimonials',
		];

		$this->add_render_attribute( 'wrapper', 'class', $classes );

		$element = $settings['element'];

		$output = [ ];
		$this->add_render_attribute( 'slick', 'data-slick', wp_json_encode( Elementor::get_data_slick($settings) ) );

		if ( ! empty ( $element ) ) {
			$output[] = '<div class="list-testimonials" '.$this->get_render_attribute_string( 'slick' ).'>';

			foreach ( $element as $index => $item ) {

				$logo = $avatar = $name = $location = $desc = '';

				if ( $item['avatar'] ) {

					$avatar = Group_Control_Image_Size::get_attachment_image_html( $item,'avatar' );
					$avatar = $avatar ? sprintf( '<div class="image avatar">%s</div>', $avatar ) : '';
				}

				if ( $item['logo'] ) {
					$logo = Group_Control_Image_Size::get_attachment_image_html( $item, 'logo' );
					$logo = $logo ? sprintf( '<div class="image logo">%s</div>', $logo ) : '';
				}

				if ( $item['name'] ) {
					$name = '<h6 class="name">' . $item['name'] . '</h6>';
				}

				if ( $item['location'] ) {
					$location = '<span class="location">' . $item['location'] . '</span>';
				}

				if ( $item['desc'] ) {
					$desc = '<div class="desc">' . $item['desc'] . '</div>';
				}

				$output[] = sprintf(
					'<div class="box-item">
						<div class="box-item__wrapper">
							<div class="box-item__image">%s %s</div>
							<div class="box-item__content">%s %s %s</div>
						</div>
					</div>',
					$avatar,
					$logo,
					$name,
					$location,
					$desc
				);
			}
			$output[] = '</div>';
		}

		echo sprintf(
			'<div %s>%s</div>',
			$this->get_render_attribute_string( 'wrapper' ),
			implode( '', $output )
		);

	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected
	function _content_template() {
	}
}