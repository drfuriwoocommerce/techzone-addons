<?php
/**
 * Add more data for user
 */

/**
 * Add more contact methods for users.
 *
 * @param array $methods
 *
 * @return array
 */
function teckzone_addons_user_contact_methods( $methods ) {
	$methods['facebook']   = esc_html__( 'Facebook', 'teckzone' );
	$methods['twitter']    = esc_html__( 'Twitter', 'teckzone' );
	$methods['google'] = esc_html__( 'Google Plus', 'teckzone' );
	$methods['pinterest']  = esc_html__( 'Pinterest', 'teckzone' );
	$methods['instagram']  = esc_html__( 'Instagram', 'teckzone' );

	return $methods;
}

add_filter( 'user_contactmethods', 'teckzone_addons_user_contact_methods' );