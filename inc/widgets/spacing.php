<?php

class Teckzone_Spacing_Widget extends WP_Widget {
	/**
	 * Holds widget settings defaults, populated in constructor.
	 *
	 * @var array
	 */
	protected $defaults;

	/**
	 * Constructor
	 *
	 * @return Teckzone_Spacing_Widget
	 */
	function __construct() {
		$this->defaults = array(
			'height'           => '',
			'height_medium'    => '',
			'height_tablet'    => '',
			'height_mobile'    => '',
			'background_color' => '',
		);

		parent::__construct(
			'spacing-widget',
			esc_html__( 'Teckzone - Spacing', 'teckzone' ),
			array(
				'classname'   => 'teckzone-spacing-widget',
				'description' => esc_html__( 'Blank space with custom height.', 'teckzone' )
			)
		);
	}

	/**
	 * Display widget
	 *
	 * @param array $args     Sidebar configuration
	 * @param array $instance Widget settings
	 *
	 * @return void
	 */
	function widget( $args, $instance ) {
		$instance = wp_parse_args( $instance, $this->defaults );
		extract( $args );

		$height = $instance['height'] ? (float) $instance['height'] : 0;

		if ( ! empty( $instance['height_medium'] ) || $instance['height_medium'] == '0' ) {
			$height_medium = (float) $instance['height_medium'];
		} else {
			$height_medium = $height;
		}

		if ( ! empty( $instance['height_tablet'] ) || $instance['height_tablet'] == '0' ) {
			$height_tablet = (float) $instance['height_tablet'];
		} else {
			$height_tablet = $height_medium;
		}

		if ( ! empty( $instance['height_mobile'] ) || $instance['height_mobile'] == '0' ) {
			$height_mobile = (float) $instance['height_mobile'];
		} else {
			$height_mobile = $height_tablet;
		}

		$inline_css        = $height >= 0.0 ? ' style="height: ' . esc_attr( $height ) . 'px"' : '';
		$inline_css_medium = $height_medium >= 0.0 ? ' style="height: ' . esc_attr( $height_medium ) . 'px"' : '';
		$inline_css_tablet = $height_tablet >= 0.0 ? ' style="height: ' . esc_attr( $height_tablet ) . 'px"' : '';
		$inline_css_mobile = $height_mobile >= 0.0 ? ' style="height: ' . esc_attr( $height_mobile ) . 'px"' : '';

		$style = $instance['background_color'] ? 'style="background-color:' . $instance['background_color'] . ';"' : '';

		echo wp_kses_post( $before_widget );

		echo sprintf(
			'<div class="spacing-wrapper" %s>' .
			'<div class="helendo_empty_space_lg hidden-md hidden-sm hidden-xs" %s></div>' .
			'<div class="helendo_empty_space_md hidden-lg hidden-sm hidden-xs" %s></div>' .
			'<div class="helendo_empty_space_sm hidden-lg hidden-md hidden-xs" %s></div>' .
			'<div class="helendo_empty_space_xs hidden-lg hidden-md hidden-sm" %s></div>' .
			'</div>',
			$style,
			$inline_css,
			$inline_css_medium,
			$inline_css_tablet,
			$inline_css_mobile
		);

		echo wp_kses_post( $after_widget );
	}

	/**
	 * Update widget
	 *
	 * @param array $new_instance New widget settings
	 * @param array $old_instance Old widget settings
	 *
	 * @return array
	 */
	function update( $new_instance, $old_instance ) {
		$instance                     = $old_instance;
		$instance['height']           = strip_tags( $new_instance['height'] );
		$instance['height_medium']    = strip_tags( $new_instance['height_medium'] );
		$instance['height_tablet']    = strip_tags( $new_instance['height_tablet'] );
		$instance['height_mobile']    = strip_tags( $new_instance['height_mobile'] );
		$instance['background_color'] = strip_tags( $new_instance['background_color'] );

		return $instance;
	}

	/**
	 * Display widget settings
	 *
	 * @param array $instance Widget settings
	 *
	 * @return void
	 */
	function form( $instance ) {
		$instance = wp_parse_args( $instance, $this->defaults );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'height' ) ); ?>"><?php esc_html_e( 'Height(px)', 'teckzone' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'height' ) ); ?>"
				   name="<?php echo esc_attr( $this->get_field_name( 'height' ) ); ?>" type="text"
				   value="<?php echo esc_attr( $instance['height'] ); ?>">
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'height_medium' ) ); ?>"><?php esc_html_e( 'Height on Medium devices (px)', 'teckzone' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'height_medium' ) ); ?>"
				   name="<?php echo esc_attr( $this->get_field_name( 'height_medium' ) ); ?>" type="text"
				   value="<?php echo esc_attr( $instance['height_medium'] ); ?>">
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'height_tablet' ) ); ?>"><?php esc_html_e( 'Height on Tablet(px)', 'teckzone' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'height_tablet' ) ); ?>"
				   name="<?php echo esc_attr( $this->get_field_name( 'height_tablet' ) ); ?>" type="text"
				   value="<?php echo esc_attr( $instance['height_tablet'] ); ?>">
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'height_mobile' ) ); ?>"><?php esc_html_e( 'Height on Mobile(px)', 'teckzone' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'height_mobile' ) ); ?>"
				   name="<?php echo esc_attr( $this->get_field_name( 'height_mobile' ) ); ?>" type="text"
				   value="<?php echo esc_attr( $instance['height_mobile'] ); ?>">
		</p>

		<script type='text/javascript'>
			jQuery( document ).ready( function ( $ ) {
				$( '.color-picker' ).not( '[id*="__i__"]' ).wpColorPicker({
					change: function(e, ui) {
						$(e.target).val(ui.color.toString());
						$(e.target).trigger('change');
					},
					clear: function(e, ui) {
						$(e.target).trigger('change');
					}
				});
			} );
		</script>
		<p>
			<label for="<?php echo $this->get_field_id( 'background_color' ); ?>"><?php esc_html_e( 'Background Color', 'teckzone' ); ?></label>
			<input class="color-picker" type="text" id="<?php echo $this->get_field_id( 'background_color' ); ?>"
				   name="<?php echo $this->get_field_name( 'background_color' ); ?>"
				   value="<?php echo esc_attr( $instance['background_color'] ); ?>" />
		</p>
		<?php
	}
}
