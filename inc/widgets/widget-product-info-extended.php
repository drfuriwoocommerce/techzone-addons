<?php
/**
 * Product Categories Widget
 *
 * @author   Automattic
 * @category Widgets
 * @package  WooCommerce/Widgets
 * @version  2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Product categories widget class.
 *
 * @extends WC_Widget
 */
class Teckzone_Widget_Product_Info_Extended extends WC_Widget {
	public $feature_title;

	/**
	 * Constructor.
	 */
	public function __construct() {
		$this->widget_cssclass    = 'woocommerce tz_widget_product_info_extended';
		$this->widget_description = esc_html__( 'Additional Product Information', 'teckzone' );
		$this->widget_id          = 'tz_product_info_extended';
		$this->widget_name        = esc_html__( 'Teckzone - Product Info Extended', 'teckzone' );
		$this->settings           = array(
			'title'           => array(
				'type'  => 'text',
				'std'   => '',
				'label' => esc_html__( 'Title', 'teckzone' ),
			),
			'brand'           => array(
				'type'  => 'checkbox',
				'std'   => 1,
				'label' => esc_html__( 'Brand', 'teckzone' ),
			),
			'vendor'          => array(
				'type'  => 'checkbox',
				'std'   => 1,
				'label' => esc_html__( 'Vendor', 'teckzone' ),
			),
			'feature_content' => array(
				'type'  => 'checkbox',
				'std'   => 1,
				'label' => esc_html__( 'Feature List', 'teckzone' ),
			),
			'feature_title'   => array(
				'type'  => 'text',
				'std'   => '',
				'label' => esc_html__( 'Feature List Title', 'teckzone' ),
			),
		);

		parent::__construct();
	}

	/**
	 * Output widget.
	 *
	 * @see WP_Widget
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Widget instance.
	 */
	public function widget( $args, $instance ) {
		if ( ! is_singular( 'product' ) ) {
			return;
		}

		$brand               = isset( $instance['brand'] ) ? $instance['brand'] : $this->settings['brand']['std'];
		$vendor              = isset( $instance['vendor'] ) ? $instance['vendor'] : $this->settings['vendor']['std'];
		$feature_content     = isset( $instance['feature_content'] ) ? $instance['feature_content'] : $this->settings['feature_content']['std'];
		$this->feature_title = isset( $instance['feature_title'] ) ? $instance['feature_title'] : '';

		if ( ! intval( $brand ) && ! intval( $feature_content ) ) {
			return;
		}

		$output = '';
		$el     = array();

		if ( intval( $brand ) ) {
			$output .= $this->single_product_brand();
			$el[] = 'brand';
		}

		if ( intval( $vendor ) && intval( teckzone_get_option( 'single_product_sold_by' ) ) ) {
			$output .= Teckzone_WooCommerce_Template_Product::get_sold_by_vendor();
			$el[] = 'vendor';
		}

		if ( intval( $feature_content ) ) {
			$output .= $this->single_product_features();
			$el[] = 'feature_content';
		}

		if ( empty( $output ) ) {
			return;
		}

		$css = '';

		if ( count( $el ) > 1 ) {
			$css = 'multi';
		}

		$this->widget_start( $args, $instance );

		echo '<div class="widget-wrapper ' . esc_attr( $css ) . '">';
		echo $output;
		echo '</div>';

		$this->widget_end( $args );
	}

	/**
	 * Get product brand
	 */
	public function single_product_brand() {
		global $product;
		$tax  = 'product_brand';
		$id   = $product->get_id();
		$term = get_the_terms( $id, $tax );

		if ( is_wp_error( $term ) || ! $term ) {
			return '';
		}

		$thumbnail_id = absint( get_term_meta( $term[0]->term_id, 'brand_thumbnail_id', true ) );

		$image_full       = wp_get_attachment_image_src( $thumbnail_id, 'full' );
		$image_full_ratio = apply_filters( 'teckzone_product_brand_meta_thumbnail_ratio', 71 );

		$size = [
			'width'  => floor( $image_full[1] * absint( $image_full_ratio ) / 100 ),
			'height' => floor( $image_full[2] * absint( $image_full_ratio ) / 100 )
		];

		$small_thumbnail_size = apply_filters( 'teckzone_product_brand_meta_thumbnail', array( $size['width'], $size['height'] ) );

		if ( ! $thumbnail_id ) {
			return '';
		}

		return sprintf(
			'<div class="product-brand"><a href="%s">%s</a></div>',
			esc_url( get_term_link( $term[0]->term_id, $tax ) ),
			teckzone_get_image_html( $thumbnail_id, $small_thumbnail_size )
		);
	}

	/**
	 * Get product features
	 */
	public function single_product_features() {
		global $product;
		$features = get_post_meta( $product->get_id(), 'product_feature_items', true );

		if ( ! $features ) {
			return '';
		}

		$title = $this->feature_title;

		return sprintf(
			'<div class="feature-item-widget">
				<div class="feature-list-title">%s</div>
				<ul class="product-feature-items">%s</ul>
			</div>',
			$title,
			$features
		);
	}
}