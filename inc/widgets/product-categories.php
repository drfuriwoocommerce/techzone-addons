<?php
/**
 * Product Categories Widget
 *
 * @author   Automattic
 * @category Widgets
 * @package  WooCommerce/Widgets
 * @version  2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Product categories widget class.
 *
 * @extends WC_Widget
 */
class Teckzone_Widget_Product_Categories extends WC_Widget {

	/**
	 * Category ancestors.
	 *
	 * @var array
	 */
	public $cat_ancestors;

	/**
	 * Current Category.
	 *
	 * @var bool
	 */
	public $current_cat;

	/**
	 * Constructor.
	 */
	public function __construct() {
		$this->widget_cssclass    = 'woocommerce tz_widget_product_categories';
		$this->widget_description = esc_html__( 'A list of product categories.', 'teckzone' );
		$this->widget_id          = 'tz_product_categories';
		$this->widget_name        = esc_html__( 'Teckzone - Product Categories', 'teckzone' );
		$this->settings           = array(
			'title'      => array(
				'type'  => 'text',
				'std'   => esc_html__( 'Product categories', 'teckzone' ),
				'label' => esc_html__( 'Title', 'teckzone' ),
			),
			'cat_number' => array(
				'type'  => 'number',
				'std'   => 10,
				'label' => esc_html__( 'Number Of Categories', 'teckzone' ),
			),
			'orderby'    => array(
				'type'    => 'select',
				'std'     => 'name',
				'label'   => esc_html__( 'Order by', 'teckzone' ),
				'options' => array(
					'order' => esc_html__( 'Category order', 'teckzone' ),
					'title' => esc_html__( 'Name', 'teckzone' ),
					'count' => esc_html__( 'Count', 'teckzone' ),
				),
			),
			'count'      => array(
				'type'  => 'checkbox',
				'std'   => 0,
				'label' => esc_html__( 'Show product counts', 'teckzone' ),
			),
			'hide_empty' => array(
				'type'  => 'checkbox',
				'std'   => 0,
				'label' => esc_html__( 'Hide empty categories', 'teckzone' ),
			),
			'max_depth'  => array(
				'type'  => 'text',
				'std'   => '',
				'label' => esc_html__( 'Maximum depth', 'teckzone' ),
			),
		);

		parent::__construct();
	}

	/**
	 * Output widget.
	 *
	 * @see WP_Widget
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Widget instance.
	 */
	public function widget( $args, $instance ) {
		global $wp_query, $post;
		$taxonomy = 'product_cat';

		$count         = isset( $instance['count'] ) ? $instance['count'] : $this->settings['count']['std'];
		$orderby       = isset( $instance['orderby'] ) ? $instance['orderby'] : $this->settings['orderby']['std'];
		$hide_empty    = isset( $instance['hide_empty'] ) ? $instance['hide_empty'] : $this->settings['hide_empty']['std'];
		$dropdown_args = array(
			'hide_empty' => $hide_empty,
		);
		$list_args     = array(
			'show_count'   => $count,
			'hierarchical' => 1,
			'taxonomy'     => $taxonomy,
			'hide_empty'   => $hide_empty,
		);
		$max_depth     = absint( isset( $instance['max_depth'] ) ? $instance['max_depth'] : $this->settings['max_depth']['std'] );

		$list_args['menu_order'] = false;
		$dropdown_args['depth']  = $max_depth;
		$list_args['depth']      = $max_depth;

		if ( 'order' === $orderby ) {
			$list_args['menu_order'] = 'asc';
		} else {
			$list_args['orderby'] = $orderby;
			if ( $orderby === 'count' ) {
				$atts['order'] = 'desc';
			}
		}

		$this->current_cat   = false;
		$this->cat_ancestors = array();

		if ( is_tax( $taxonomy ) ) {
			$this->current_cat   = $wp_query->queried_object;
			$this->cat_ancestors = get_ancestors( $this->current_cat->term_id, $taxonomy );

		} elseif ( is_singular( 'product' ) ) {
			$product_category = wc_get_product_terms(
				$post->ID, $taxonomy, apply_filters(
					'woocommerce_product_categories_widget_product_terms_args', array(
						'orderby' => 'parent',
					)
				)
			);

			if ( ! empty( $product_category ) ) {
				$current_term = '';
				foreach ( $product_category as $term ) {
					if ( $term->parent != 0 ) {
						$current_term = $term;
						break;
					}
				}
				$this->current_cat   = $current_term ? $current_term : $product_category[0];
				$this->cat_ancestors = get_ancestors( $this->current_cat->term_id, $taxonomy );
			}

		}

		$this->widget_start( $args, $instance );

		$list_args['title_li']                   = '';
		$list_args['pad_counts']                 = 1;
		$list_args['show_option_none']           = esc_html__( 'No product categories exist.', 'teckzone' );
		$list_args['current_category']           = ( $this->current_cat ) ? $this->current_cat->term_id : '';
		$list_args['current_category_ancestors'] = $this->cat_ancestors;
		$list_args['max_depth']                  = $max_depth;

		$parent_term_id = 0;
		if ( is_tax( $taxonomy ) || is_singular( 'product' ) ) {
			if ( count( $this->cat_ancestors ) > 0 ) {
				$parent_term_id = end( $this->cat_ancestors );
			}

			$children_terms = get_term_children( $parent_term_id, $taxonomy );
			if ( count( $children_terms ) <= 0 ) {
				$parent_term_id = 0;
			}

		}
		$list_args['child_of'] = $parent_term_id;

		echo '<ul class="product-categories">';
		if ( $parent_term_id ) {
			$parent_term = get_term_by( 'id', $parent_term_id, $taxonomy );
			echo '<li class="current-cat-parent tz-current-cat-parent"><a href="' . esc_url( get_term_link( $parent_term_id, $taxonomy ) ) . '">' . $parent_term->name . '</a>';
			echo '<ul class="children">';
		}
		wp_list_categories( apply_filters( 'woocommerce_product_categories_widget_args', $list_args ) );
		if ( $parent_term_id ) {
			echo '</ul>';
			echo '</li>';
		}
		echo '</ul>';

		// Add Show More/Show Less Button

		if ( ! is_singular( 'product' ) ) {
			$parent_id = 0;
			$current_cat_lv = 0;
			if ( function_exists( 'is_product_category' ) && is_product_category() ) {
				global $wp_query;
				$current_cat = $wp_query->get_queried_object();
				$term_id     = $current_cat->term_id;

				$cat_ancestors = count( get_ancestors( $term_id, $taxonomy ) ); // Get cat level
				$current_cat_lv = $cat_ancestors;

				// Get parent cat ID while be in child cat
				if ( $cat_ancestors != 0 ) {
					$child_term  = get_term( $term_id, $taxonomy );
					$parent_term = get_term( $child_term->parent, $taxonomy );

					$parent_id = $parent_term->term_id;
				}
			}

			$tax_args = array(
				'orderby'    => $orderby,
				'order'      => $orderby === 'count' ? 'DESC' : 'ASC',
				'hide_empty' => $hide_empty,
				'child_of'   => $parent_id,
				'parent'     => $parent_id
			);

			$count      = intval( wp_count_terms( $taxonomy, $tax_args ) );
			$cat_number = isset( $instance['cat_number'] ) ? $instance['cat_number'] : $this->settings['cat_number']['std'];
			$perShow    = $cat_number;
			if ( $count > $cat_number && $current_cat_lv == 0 ) {
				echo '<div class="tz-widget-product-cats-btn">
			<span class="show-more"><i class="icon-plus-square"></i>' . apply_filters( 'teckzone_widget_product_cats_show_more_btn', esc_html__( 'SHOW MORE' ) ) . '</span>
			<span class="show-less"><i class="icon-minus-square"></i>' . apply_filters( 'teckzone_widget_product_cats_show_less_btn', esc_html__( 'SHOW LESS' ) ) . '</span>
			</div>';

				$perShow = $count - $cat_number;
			}

			echo '<input type="hidden" class="cats-number" value="' . esc_attr( $cat_number ) . '" name="cats_number">';
			echo '<input type="hidden" class="per-show" value="' . esc_attr( apply_filters( 'teckzone_product_cats_per_show', $perShow ) ) . '" name="per_show">';
		}

		$this->widget_end( $args );
	}
}