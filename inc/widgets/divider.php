<?php

class Teckzone_Divider_Widget extends WP_Widget {
	/**
	 * Holds widget settings defaults, populated in constructor.
	 *
	 * @var array
	 */
	protected $defaults;

	/**
	 * Constructor
	 *
	 * @return Teckzone_Divider_Widget
	 */
	function __construct() {
		$this->defaults = array(
			'background_color' => '',
			'height'           => 1,
			'top_spacing'      => 5,
			'bot_spacing'      => 5,
		);

		parent::__construct(
			'divider-widget',
			esc_html__( 'Teckzone - Divider', 'teckzone' ),
			array(
				'classname'   => 'teckzone-divider-widget',
				'description' => esc_html__( 'A line that divides different widgets.', 'teckzone' )
			)
		);
	}

	/**
	 * Display widget
	 *
	 * @param array $args     Sidebar configuration
	 * @param array $instance Widget settings
	 *
	 * @return void
	 */
	function widget( $args, $instance ) {
		$instance = wp_parse_args( $instance, $this->defaults );
		extract( $args );

		$style = array();

		if ( $instance['background_color'] ) {
			$style[] = 'background-color:' . $instance['background_color'] . ';';
		}

		if ( $instance['height'] ) {
			$style[] = 'height:' . $this->techzone_attr( intval( $instance['height'] ) ) . ';';
		}

		if ( $instance['top_spacing'] ) {
			$style[] = 'margin-top:' . $this->techzone_attr( intval( $instance['top_spacing'] ) ) . ';';
		}

		if ( $instance['bot_spacing'] ) {
			$style[] = 'margin-bottom:' . $this->techzone_attr( intval( $instance['bot_spacing'] ) ) . ';';
		}

		echo wp_kses_post( $before_widget );

		echo sprintf(
			'<div class="divider-wrapper" style="%s"></div>',
			esc_attr( implode( '', $style ) )
		);

		echo wp_kses_post( $after_widget );
	}

	/**
	 * Update widget
	 *
	 * @param array $new_instance New widget settings
	 * @param array $old_instance Old widget settings
	 *
	 * @return array
	 */
	function update( $new_instance, $old_instance ) {
		$instance                     = $old_instance;
		$instance['height']           = strip_tags( $new_instance['height'] );
		$instance['top_spacing']      = strip_tags( $new_instance['top_spacing'] );
		$instance['bot_spacing']      = strip_tags( $new_instance['bot_spacing'] );
		$instance['background_color'] = strip_tags( $new_instance['background_color'] );

		return $instance;
	}

	/**
	 * Display widget settings
	 *
	 * @param array $instance Widget settings
	 *
	 * @return void
	 */
	function form( $instance ) {
		$instance = wp_parse_args( $instance, $this->defaults );
		?>
		<script type='text/javascript'>
			jQuery( document ).ready( function ( $ ) {
				$( '.color-picker' ).not( '[id*="__i__"]' ).wpColorPicker({
					change: function(e, ui) {
						$(e.target).val(ui.color.toString());
						$(e.target).trigger('change');
					},
					clear: function(e, ui) {
						$(e.target).trigger('change');
					}
				});
			} );
		</script>
		<p>
			<label for="<?php echo $this->get_field_id( 'background_color' ); ?>"><?php esc_html_e( 'Background Color', 'teckzone' ); ?></label>
			<input class="color-picker" type="text" id="<?php echo $this->get_field_id( 'background_color' ); ?>"
				   name="<?php echo $this->get_field_name( 'background_color' ); ?>"
				   value="<?php echo esc_attr( $instance['background_color'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'height' ) ); ?>"><?php esc_html_e( 'Height', 'teckzone' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'height' ) ); ?>"
				   name="<?php echo esc_attr( $this->get_field_name( 'height' ) ); ?>" type="text"
				   value="<?php echo esc_attr( $instance['height'] ); ?>">
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'top_spacing' ) ); ?>"><?php esc_html_e( 'Top Spacing', 'teckzone' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'top_spacing' ) ); ?>"
				   name="<?php echo esc_attr( $this->get_field_name( 'top_spacing' ) ); ?>" type="text"
				   value="<?php echo esc_attr( $instance['top_spacing'] ); ?>">
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'bot_spacing' ) ); ?>"><?php esc_html_e( 'Bottom Spacing', 'teckzone' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'bot_spacing' ) ); ?>"
				   name="<?php echo esc_attr( $this->get_field_name( 'bot_spacing' ) ); ?>" type="text"
				   value="<?php echo esc_attr( $instance['bot_spacing'] ); ?>">
		</p>

		<?php
	}

	/**
	 * @param string $attr
	 *
	 * @return string
	 */
	protected function techzone_attr( $attr ) {
		$attr = preg_replace( '/\s+/', '', $attr );

		$pattern = '/^(\d*(?:\.\d+)?)\s*(px|\%|in|cm|mm|em|rem|ex|pt|pc|vw|vh|vmin|vmax)?$/';
		// allowed metrics: http://www.w3schools.com/cssref/css_units.asp
		$regexr   = preg_match( $pattern, $attr, $matches );
		$value    = isset( $matches[1] ) ? (float) $matches[1] : (float) $attr;
		$unit     = isset( $matches[2] ) ? $matches[2] : 'px';
		$fontSize = $value . $unit;

		return $fontSize;
	}
}
