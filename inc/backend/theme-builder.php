<?php

/**
 * Register portfolio support
 */
class Teckzone_Theme_Builder {

	/**
	 * Class constructor.
	 */
	public function __construct() {

		if ( ! defined( 'ELEMENTOR_VERSION' ) ) {
			return;
		}

		add_action( 'admin_menu', array( $this, 'register_admin_menu' ), 50 );

		// Make sure the post types are loaded for imports
		add_action( 'import_start', array( $this, 'register_post_type' ) );

		// Register custom post type and custom taxonomy
		add_action( 'init', array( $this, 'register_post_type' ) );

		add_filter( 'single_template', array( $this, 'load_canvas_template' ) );

	}

	/**
	 * Register portfolio post type
	 */
	public function register_post_type() {

		$labels = array(
			'name'               => esc_html__( 'Header Builder Template', 'teckzone' ),
			'singular_name'      => esc_html__( 'Elementor Header', 'teckzone' ),
			'menu_name'          => esc_html__( 'Header Template', 'teckzone' ),
			'name_admin_bar'     => esc_html__( 'Elementor Header', 'teckzone' ),
			'add_new'            => esc_html__( 'Add New', 'teckzone' ),
			'add_new_item'       => esc_html__( 'Add New Header', 'teckzone' ),
			'new_item'           => esc_html__( 'New Header Template', 'teckzone' ),
			'edit_item'          => esc_html__( 'Edit Header Template', 'teckzone' ),
			'view_item'          => esc_html__( 'View Header Template', 'teckzone' ),
			'all_items'          => esc_html__( 'All Elementor Header', 'teckzone' ),
			'search_items'       => esc_html__( 'Search Header Templates', 'teckzone' ),
			'parent_item_colon'  => esc_html__( 'Parent Header Templates:', 'teckzone' ),
			'not_found'          => esc_html__( 'No Header Templates found.', 'teckzone' ),
			'not_found_in_trash' => esc_html__( 'No Header Templates found in Trash.', 'teckzone' ),
		);

		$args = array(
			'labels'              => $labels,
			'public'              => true,
			'rewrite'             => false,
			'show_ui'             => true,
			'show_in_menu'        => false,
			'show_in_nav_menus'   => false,
			'exclude_from_search' => true,
			'capability_type'     => 'post',
			'hierarchical'        => false,
			'menu_icon'           => 'dashicons-editor-kitchensink',
			'supports'            => array( 'title', 'thumbnail', 'elementor' ),
		);

		if ( ! post_type_exists( 'tz_header' ) ) {
			register_post_type( 'tz_header', $args );
		}

		// Footer Builder
		$labels = array(
			'name'               => esc_html__( 'Footer Builder Template', 'teckzone' ),
			'singular_name'      => esc_html__( 'Elementor Footer', 'teckzone' ),
			'menu_name'          => esc_html__( 'Footer Template', 'teckzone' ),
			'name_admin_bar'     => esc_html__( 'Elementor Footer', 'teckzone' ),
			'add_new'            => esc_html__( 'Add New', 'teckzone' ),
			'add_new_item'       => esc_html__( 'Add New Footer', 'teckzone' ),
			'new_item'           => esc_html__( 'New Footer Template', 'teckzone' ),
			'edit_item'          => esc_html__( 'Edit Footer Template', 'teckzone' ),
			'view_item'          => esc_html__( 'View Footer Template', 'teckzone' ),
			'all_items'          => esc_html__( 'All Elementor Footer', 'teckzone' ),
			'search_items'       => esc_html__( 'Search Footer Templates', 'teckzone' ),
			'parent_item_colon'  => esc_html__( 'Parent Footer Templates:', 'teckzone' ),
			'not_found'          => esc_html__( 'No Footer Templates found.', 'teckzone' ),
			'not_found_in_trash' => esc_html__( 'No Footer Templates found in Trash.', 'teckzone' ),
		);

		$args = array(
			'labels'              => $labels,
			'public'              => true,
			'rewrite'             => false,
			'show_ui'             => true,
			'show_in_menu'        => false,
			'show_in_nav_menus'   => false,
			'exclude_from_search' => true,
			'capability_type'     => 'post',
			'hierarchical'        => false,
			'menu_icon'           => 'dashicons-editor-kitchensink',
			'supports'            => array( 'title', 'thumbnail', 'elementor' ),
		);


		if ( ! post_type_exists( 'tz_footer' ) ) {
			register_post_type( 'tz_footer', $args );
		}

		// Mega Menu Builder
		$labels = array(
			'name'               => esc_html__( 'Mega Menu Builder Template', 'teckzone' ),
			'singular_name'      => esc_html__( 'Elementor Mega Menu', 'teckzone' ),
			'menu_name'          => esc_html__( 'Mega Menu Template', 'teckzone' ),
			'name_admin_bar'     => esc_html__( 'Elementor Mega Menu', 'teckzone' ),
			'add_new'            => esc_html__( 'Add New', 'teckzone' ),
			'add_new_item'       => esc_html__( 'Add New Mega Menu', 'teckzone' ),
			'new_item'           => esc_html__( 'New Mega Menu Template', 'teckzone' ),
			'edit_item'          => esc_html__( 'Edit Mega Menu Template', 'teckzone' ),
			'view_item'          => esc_html__( 'View Mega Menu Template', 'teckzone' ),
			'all_items'          => esc_html__( 'All Elementor Mega Menu', 'teckzone' ),
			'search_items'       => esc_html__( 'Search Mega Menu Templates', 'teckzone' ),
			'parent_item_colon'  => esc_html__( 'Parent Mega Menu Templates:', 'teckzone' ),
			'not_found'          => esc_html__( 'No Mega Menu Templates found.', 'teckzone' ),
			'not_found_in_trash' => esc_html__( 'No Mega Menu Templates found in Trash.', 'teckzone' ),
		);

		$args = array(
			'labels'              => $labels,
			'public'              => true,
			'rewrite'             => false,
			'show_ui'             => true,
			'show_in_menu'        => false,
			'show_in_nav_menus'   => false,
			'exclude_from_search' => true,
			'capability_type'     => 'post',
			'hierarchical'        => false,
			'menu_icon'           => 'dashicons-editor-kitchensink',
			'supports'            => array( 'title', 'thumbnail', 'elementor' ),
		);


		if ( ! post_type_exists( 'tz_mega_menu' ) ) {
			register_post_type( 'tz_mega_menu', $args );
		}


	}

	public function register_admin_menu() {
		add_submenu_page(
			'edit.php?post_type=elementor_library',
			esc_html__( 'Header Builder', 'teckzone' ),
			esc_html__( 'Header Builder', 'teckzone' ),
			'edit_pages',
			'edit.php?post_type=tz_header'
		);

		add_submenu_page(
			'edit.php?post_type=elementor_library',
			esc_html__( 'Footer Builder', 'teckzone' ),
			esc_html__( 'Footer Builder', 'teckzone' ),
			'edit_pages',
			'edit.php?post_type=tz_footer'
		);

	}


	function load_canvas_template( $single_template ) {

		global $post;

		if ( 'tz_header' == $post->post_type || 'tz_footer' == $post->post_type || 'tz_mega_menu' == $post->post_type ) {

			return ELEMENTOR_PATH . '/modules/page-templates/templates/canvas.php';
		}

		return $single_template;
	}

}