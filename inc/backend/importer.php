<?php
/**
 * Hooks for importer
 *
 * @package Teckzone
 */


/**
 * Importer the demo content
 *
 * @since  1.0
 *
 */
function teckzone_vc_addons_importer() {
	return array(
		array(
			'name'       => 'Home Local Shop',
			'preview'    => 'http://localhost/import/local_shop/preview.jpg',
			'content'    => 'http://localhost/import/local_shop/demo-content.xml',
			'customizer' => 'http://localhost/import/local_shop/customizer.dat',
			'widgets'    => 'http://localhost/import/local_shop/widgets.wie',
			'pages'      => array(
				'front_page' => 'Home Local Shop',
				'blog'       => 'Blog',
				'shop'       => 'Shop',
				'cart'       => 'Cart',
				'checkout'   => 'Checkout',
				'my_account' => 'My Account',
			),
			'menus'      => array(
				'primary' => 'primary-menu',
			),
			'options'    => array(
				'shop_catalog_image_size'   => array(
					'width'  => 400,
					'height' => 400,
					'crop'   => 1,
				),
				'shop_single_image_size'    => array(
					'width'  => 600,
					'height' => 600,
					'crop'   => 1,
				),
				'shop_thumbnail_image_size' => array(
					'width'  => 70,
					'height' => 70,
					'crop'   => 1,
				),
			),
		),
	);
}

add_filter( 'soo_demo_packages', 'teckzone_vc_addons_importer', 20 );
