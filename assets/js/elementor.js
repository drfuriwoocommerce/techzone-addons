(function ( $ ) {
	'use strict';

	/**
	 * Product Deals Carousel 2
	 */
	var productDealsCarousel2Handler = function ( $scope, $ ) {
		$scope.find( '.tz-product-deals-carousel-2' ).each( function () {
			var $selector = $( this ),
				$gallery = $selector.find( '.woocommerce-product-gallery' );

			var options = {
				selector      : '.woocommerce-product-gallery__wrapper > .woocommerce-product-gallery__image',
				allowOneSlide : false,
				animation     : "slide",
				animationLoop : false,
				animationSpeed: 500,
				controlNav    : "thumbnails",
				directionNav  : false,
				rtl           : false,
				slideshow     : false,
				smoothHeight  : true,
				start         : function () {
					$gallery.css( 'opacity', 1 );
				}
			};

			$gallery.flexslider( options );

			$selector.find( '.deal-expire-countdown' ).each( function () {
				$( document ).trigger( 'deal_expire_countdown', $( this ) );
			} );

			$gallery.each( function () {
				var $el = $( this );
				$el.imagesLoaded( function () {

					var $thumbnail = $el.find( '.flex-control-thumbs' );

					setTimeout( function () {
						if ( $thumbnail.length < 1 ) {
							return;
						}
						var columns = $el.data( 'columns' );
						var count = $thumbnail.find( 'li' ).length;
						if ( count > columns ) {
							$thumbnail.not( '.slick-initialized' ).slick( {
								slidesToShow  : columns,
								slidesToScroll: 1,
								focusOnSelect : true,
								infinite      : false,
								arrows        : true,
								prevArrow     : '<span class="icon-chevron-left slick-prev-arrow"></span>',
								nextArrow     : '<span class="icon-chevron-right slick-next-arrow"></span>',
								responsive    : [
									{
										breakpoint: 768,
										settings  : {
											slidesToShow: 4
										}
									},
									{
										breakpoint: 480,
										settings  : {
											slidesToShow: 3
										}
									}
								]
							} );
						} else {
							$thumbnail.addClass( 'no-slick' );
						}
					}, 100 );

				} );
			} );

			$selector.find( '.woocommerce-product-gallery__image' ).on( 'click', function () {
				return false;
			} );

			$selector.on( 'afterChange', function () {
				lazyLoadHandler( $selector );
			} );
		} );
	};

	/**
	 * Product Deals Carousel 3
	 */
	var productDealsCarousel3Handler = function ( $scope, $ ) {
		$scope.find( '.tz-product-deals-carousel-3' ).each( function () {
			var $selector = $( this ),
				$gallery = $selector.find( '.woocommerce-product-gallery' );

			var options = {
				selector      : '.woocommerce-product-gallery__wrapper > .woocommerce-product-gallery__image',
				allowOneSlide : false,
				animation     : "slide",
				animationLoop : false,
				animationSpeed: 500,
				controlNav    : "thumbnails",
				directionNav  : false,
				rtl           : false,
				slideshow     : false,
				smoothHeight  : true,
				start         : function () {
					$gallery.css( 'opacity', 1 );
				}
			};

			$gallery.flexslider( options );

			$selector.find( '.deal-expire-countdown' ).each( function () {
				$( document ).trigger( 'deal_expire_countdown', $( this ) );
			} );

			$gallery.each( function () {
				var $el = $( this );
				$el.imagesLoaded( function () {

					var $thumbnail = $el.find( '.flex-control-thumbs' );

					setTimeout( function () {
						if ( $thumbnail.length < 1 ) {
							return;
						}
						var columns = $el.data( 'columns' );
						var count = $thumbnail.find( 'li' ).length;
						if ( count > columns ) {
							$thumbnail.not( '.slick-initialized' ).slick( {
								slidesToShow  : columns,
								slidesToScroll: 1,
								focusOnSelect : true,
								infinite      : false,
								arrows        : true,
								prevArrow     : '<span class="icon-chevron-left slick-prev-arrow"></span>',
								nextArrow     : '<span class="icon-chevron-right slick-next-arrow"></span>',
								responsive    : [
									{
										breakpoint: 768,
										settings  : {
											slidesToShow: 4
										}
									},
									{
										breakpoint: 480,
										settings  : {
											slidesToShow: 3
										}
									}
								]
							} );
						} else {
							$thumbnail.addClass( 'no-slick' );
						}
					}, 100 );

				} );
			} );

			$selector.find( '.woocommerce-product-gallery__image' ).on( 'click', function () {
				return false;
			} );

			$selector.on( 'afterChange', function () {
				lazyLoadHandler( $selector );
			} );
		} );
	};

	/**
	 * LazyLoad
	 */
	var lazyLoadHandler = function ( $els ) {
		if ( $els.length === 0 ) {
			$els = $( 'body' );
		}
		$els.find( 'img.lazy' ).lazyload( {
			load: function () {
				$( this ).removeClass( 'lazy' );
			}
		} );
	};

	/**
	 * CountDown
	 */
	var countDownHandler = function ( $scope, $ ) {
		$scope.find( '.teckzone-countdown' ).mf_countdown();
	};

	/**
	 * Counter
	 * @param $scope
	 * @param $
	 */
	var counterHandler = function ( $scope, $ ) {
		$scope.find( '.teckzone-counter' ).each( function () {
			var $selector = $( this );
			$selector.find( '.value' ).counterUp( {
				delay: 20,
				time : 800
			} );

		} );
	};

	/**
	 * Masonry
	 * @param $scope
	 * @param $
	 */
	var imageMasonry = function ( $scope, $ ) {
		$scope.find( '.teckzone-image-masonry' ).each( function () {
			var $selector = $( this );
			$selector.imagesLoaded( function () {
				$selector.find( '.gallery-wrapper' ).isotope( {
					itemSelector   : '.image-masonry-item',
					percentPosition: true,
					masonry        : {
						columnWidth: '.grid-sizer'
					}
				} );
			} );
		} );
	};

	/**
	 * Carousel
	 * @param $scope
	 * @param $
	 */
	var imageCarousel = function ( $scope, $ ) {
		$scope.find( '.teckzone-image-slides' ).each( function () {
			var $selector = $( this ),
				options = {
					arrows   : false,
					dots     : true,
					prevArrow: '<span class="slick-prev-arrow"><i class="icon-chevron-left"></i></span>',
					nextArrow: '<span class="slick-next-arrow"><i class="icon-chevron-right"></i></span>'
				};
			$selector.find( '.gallery-wrapper' ).not( '.slick-initialized' ).slick( options );

			$selector.find( '.gallery-wrapper' ).on( 'afterChange', function () {
				lazyLoadHandler( $selector );
			} );
		} )
	};

	/**
	 * Timeline
	 * @param $scope
	 * @param $
	 */
	var timeline = function ( $scope, $ ) {
		$scope.find( '.teckzone-timeline' ).each( function () {
			var $selector = $( this ),
				elementSettings = $selector.data( 'settings' );

			$selector.find( 'ul.timeline__date' ).not( '.slick-initialized' ).slick( {
				rtl           : $( 'body' ).hasClass( 'rtl' ),
				slidesToShow  : parseInt( elementSettings.show ),
				slidesToScroll: parseInt( elementSettings.scroll ),
				arrows        : false,
				dots          : false,
				infinite      : false,
				prevArrow     : '',
				nextArrow     : '',
				autoplay      : false,
				responsive    : [
					{
						breakpoint: 1400,
						settings  : {
							slidesToShow  : parseInt( elementSettings.show ) > 7 ? 7 : elementSettings.show,
							slidesToScroll: parseInt( elementSettings.scroll ) > 7 ? 7 : elementSettings.scroll
						}
					},
					{
						breakpoint: 1025,
						settings  : {
							slidesToShow  : elementSettings.tablet.slidesToShow,
							slidesToScroll: elementSettings.tablet.slidesToScroll
						}
					},
					{
						breakpoint: 768,
						settings  : {
							slidesToShow  : elementSettings.mobile.slidesToShow,
							slidesToScroll: elementSettings.mobile.slidesToScroll
						}
					}
				]
			} );
		} )
	};

	/**
	 * Testimonials
	 * @param $scope
	 * @param $
	 */
	var testimonial = function ( $scope, $ ) {
		$scope.find( '.teckzone-testimonials' ).each( function () {
			var $selector = $( this ),
				options = {
					prevArrow: '<span class="icon-chevron-left slick-prev-arrow"></span>',
					nextArrow: '<span class="icon-chevron-right slick-next-arrow"></span>'
				};
			$selector.find( '.list-testimonials' ).not( '.slick-initialized' ).slick( options );
		} )
	};

	/**
	 * Testimonials
	 * @param $scope
	 * @param $
	 */
	var testimonial2 = function ( $scope, $ ) {
		$scope.find( '.teckzone-testimonials-2' ).each( function () {
			var $selector = $( this ),
				$arrow_wrapper = $( this ).find( '.slick-nav__arrows' ),
				$dot_wrapper = $( this ).find( '.slick-nav__dots' ),
				options = {
					appendArrows: $arrow_wrapper,
					appendDots  : $dot_wrapper,
					prevArrow   : '<span class="icon-chevron-left slick-prev-arrow"></span>',
					nextArrow   : '<span class="icon-chevron-right slick-next-arrow"></span>'
				};
			$selector.find( '.list-testimonials' ).not( '.slick-initialized' ).slick( options );
		} )
	};

	/**
	 * Tab
	 * @param $scope
	 * @param $
	 */
	var tab = function ( $scope, $ ) {
		$scope.find( '.teckzone-tab-list' ).each( function () {
			var $selector = $( this ),
				tabMnu = $selector.find( '.tab__nav .box-nav' ),
				tabContent = $selector.find( '.tab__content .box-content' );

			$selector.find( '.tab__nav .box-nav:nth-child(' + 1 + ')' ).addClass( 'active' );
			$selector.find( '.tab__content .box-content:nth-child(' + 1 + ')' ).addClass( 'active' );

			tabMnu.each( function ( i ) {
				$( this ).attr( 'data-tab', 'tab' + i );
			} );
			tabContent.each( function ( i ) {
				$( this ).attr( 'data-tab', 'tab' + i );
			} );

			tabMnu.click( function () {
				var tabData = $( this ).data( 'tab' );
				$selector.find( tabContent ).removeClass( 'active' );
				$selector.find( tabContent ).filter( '[data-tab=' + tabData + ']' ).addClass( 'active' );
			} );

			$selector.find( '.tab__nav > .box-nav' ).click( function () {
				var before = $( '.tab__nav .box-nav.active' );
				before.removeClass( 'active' );
				$( this ).addClass( 'active' );
			} );
		} )
	};

	var videoLightBox = function ( $scope, $ ) {
		$scope.find( '.teckzone-video' ).each( function () {

			var $selector = $( this ).find( '.play-banner' );
			$selector.magnificPopup( {
				disableOn      : 700,
				type           : 'iframe',
				mainClass      : 'mfp-fade',
				removalDelay   : 300,
				preloader      : false,
				fixedContentPos: false
			} );
		} )
	};

	var slideCarousel = function ( $scope, $ ) {
		$scope.find( '.teckzone-slides-wrapper' ).each( function () {
			var $selector = $( this ).find( '.teckzone-slides' ),
				elementSettings = $selector.data( 'slider_options' ),
				$arrow_wrapper = $( this ).find( '.arrows-inner' ),
				slidesToShow = parseInt( elementSettings.slidesToShow );

			$selector.not( '.slick-initialized' ).slick( {
				rtl          : $( 'body' ).hasClass( 'rtl' ),
				slidesToShow : slidesToShow,
				arrows       : elementSettings.arrows,
				appendArrows : $arrow_wrapper,
				dots         : elementSettings.dots,
				infinite     : elementSettings.infinite,
				prevArrow    : '<span class="slick-prev-arrow"><i class="icon-chevron-left"></i></span>',
				nextArrow    : '<span class="slick-next-arrow"><i class="icon-chevron-right"></i></span>',
				autoplay     : elementSettings.autoplay,
				autoplaySpeed: parseInt( elementSettings.autoplaySpeed ),
				speed        : parseInt( elementSettings.speed ),
				fade         : elementSettings.fade,
				pauseOnHover : elementSettings.pauseOnHover,
				responsive   : []
			} );

			$selector.imagesLoaded( function () {
				$selector.closest( '.teckzone-slides-wrapper' ).removeClass( 'loading' );
			} );

			var animation = $selector.data( 'animation' );

			if ( animation ) {
				$selector
					.on( 'beforeChange', function () {
						var $sliderContent = $selector.find( '.teckzone-slide-content' ),
							$sliderPriceBox = $selector.find( '.teckzone-slide-price-box' );

						$sliderContent.removeClass( 'animated' + ' ' + animation ).hide();

						$sliderPriceBox.removeClass( 'animated zoomIn' ).hide();
					} )
					.on( 'afterChange', function ( event, slick, currentSlide ) {
						var $currentSlide = $( slick.$slides.get( currentSlide ) ).find( '.teckzone-slide-content' ),
							$currentPriceBox = $( slick.$slides.get( currentSlide ) ).find( '.teckzone-slide-price-box' );

						$currentSlide.show().addClass( 'animated' + ' ' + animation );

						$currentPriceBox.show().addClass( 'animated zoomIn' );
					} );
			}

			$selector.on( 'afterChange', function () {
				lazyLoadHandler( $selector );
			} );
		} );
	};

	/**
	 * Testimonials
	 * @param $scope
	 * @param $
	 */
	var postsCarousel = function ( $scope, $ ) {
		$scope.find( '.teckzone-posts-carousel' ).each( function () {
			var $selector = $( this ),

				options = {
					arrows      : true,
					dots        : false,
					prevArrow   : '<span class="icon-chevron-left slick-prev-arrow"></span>',
					nextArrow   : '<span class="icon-chevron-right slick-next-arrow"></span>',
					appendArrows: $selector.find( '.box-nav' )
				};

			$selector.find( '.post-list' ).not( '.slick-initialized' ).slick( options );
		} )
	};

	var imagesCarousel = function ( $scope, $ ) {
		$scope.find( '.teckzone-images-carousel--slide' ).each( function () {
			var $selector = $( this ),
				options = {
					arrows      : true,
					dots        : false,
					prevArrow   : '<span class="icon-chevron-left slick-prev-arrow"></span>',
					nextArrow   : '<span class="icon-chevron-right slick-next-arrow"></span>',
					appendArrows: $selector.find( '.slick-arrows' )
				};

			$selector.find( '.images-list' ).not( '.slick-initialized' ).slick( options );

			$selector.on( 'afterChange', function () {
				lazyLoadHandler( $selector );
			} );

		} );
	};

	var trendingSearchCarousel = function ( $scope, $ ) {
		$scope.find( '.teckzone-elementor-trending-search-carousel' ).each( function () {
			var $selector = $( this ),
				options = {
					prevArrow: '<span class="icon-chevron-left slick-prev-arrow"></span>',
					nextArrow: '<span class="icon-chevron-right slick-next-arrow"></span>'
				};

			if ( $selector.hasClass( 'teckzone-trending-search-carousel-2' ) || $selector.hasClass( 'teckzone-trending-search-carousel-3' ) ) {
				options.appendArrows = $selector.find( '.slick-arrows' );
			}

			$selector.find( '.collection-list' ).not( '.slick-initialized' ).slick( options );

			$selector.on( 'afterChange', function () {
				lazyLoadHandler( $selector );
			} );

		} );
	};

	var promotionHandler = function ( $scope, $ ) {
		$scope.find( '.teckzone-promotion' ).each( function () {
			var $selector = $( this );

			$selector.on( 'click', '.pro-close', function ( e ) {
				e.preventDefault();

				$( this ).closest( '.teckzone-promotion' ).slideUp().addClass( 'invisible' );
			} );

		} );
	};

	var searchFormHandler = function ( $scope, $ ) {
		$scope.find( '.teckzone-products-search' ).each( function () {
			var $selector = $( this ),
				elementSettings = $selector.data( 'settings' );
			$selector.on( 'change', '.product-cat-dd', function () {
				var value = $( this ).find( 'option:selected' ).text().trim();
				$selector.find( '.product-cat-label' ).html( value );
			} );

			$selector.find( '.products-search' ).submit( function () {
				if ( $( this ).find( '.product-cat-dd' ).val() == '0' ) {
					$( this ).find( '.product-cat-dd' ).removeAttr( 'name' );
				}
			} );


			if ( elementSettings.ajax_search === 'no' ) {
				return;
			}

			var xhr = null,
				$form = $selector.closest( 'form.form-search' ),
				searchCache = {};

			$selector.on( 'keyup', '.search-field', function ( e ) {
				var valid = false;

				if ( typeof e.which == 'undefined' ) {
					valid = true;
				} else if ( typeof e.which == 'number' && e.which > 0 ) {
					valid = !e.ctrlKey && !e.metaKey && !e.altKey;
				}

				if ( !valid ) {
					return;
				}

				if ( xhr ) {
					xhr.abort();
				}

				var $currentForm = $( this ).closest( '.form-search' ),
					$search = $currentForm.find( 'input.search-field' );

				if ( $search.val().length < 2 ) {
					$currentForm.removeClass( 'searching searched actived found-products found-no-product invalid-length' );
				}

				search( $currentForm );
			} ).on( 'change', '.product-cat-dd', function () {
				if ( xhr ) {
					xhr.abort();
				}

				var $currentForm = $( this ).closest( '.form-search' );


				search( $currentForm );
			} ).on( 'focusout', '.search-field', function () {
				var $currentForm = $( this ).closest( '.form-search' ),
					$search = $currentForm.find( 'input.search-field' );
				if ( $search.val().length < 2 ) {
					$currentForm.removeClass( 'searching searched actived found-products found-no-product invalid-length' );
				}
			} );

			$selector.on( 'click', '.close-search-results', function ( e ) {
				e.preventDefault();
				$selector.find( '.search-field' ).val( '' );
				$selector.find( '.form-search' ).removeClass( 'searching searched actived found-products found-no-product invalid-length' );
			} );

			/**
			 * Private function for search
			 */
			function search( $currentForm ) {
				var $search = $currentForm.find( 'input.search-field' ),
					keyword = $search.val(),
					cat = 0,
					$results = $currentForm.find( '.search-results' );

				if ( $currentForm.find( '.product-cat-dd' ).length > 0 ) {
					cat = $currentForm.find( '.product-cat-dd' ).val();
				}


				if ( keyword.trim().length < 2 ) {
					$currentForm.removeClass( 'searching found-products found-no-product' ).addClass( 'invalid-length' );
					return;
				}

				$currentForm.removeClass( 'found-products found-no-product' ).addClass( 'searching' );

				var keycat = keyword + cat;

				if ( keycat in searchCache ) {
					var result = searchCache[keycat];

					$currentForm.removeClass( 'searching' );

					$currentForm.addClass( 'found-products' );

					$results.html( result.products );

					$( document.body ).trigger( 'teckzone_ajax_search_request_success', [$results] );

					$currentForm.removeClass( 'invalid-length' );

					$currentForm.addClass( 'searched actived' );
				} else {
					var data = {
							'term'              : keyword,
							'cat'               : cat,
							'ajax_search_number': elementSettings.ajax_search_number,
							'search_type'       : elementSettings.search_for
						},
						ajax_url = teckzoneData.ajax_url.toString().replace( '%%endpoint%%', 'teckzone_instance_search_form' );

					xhr = $.post(
						ajax_url,
						data,
						function ( response ) {
							var $products = response.data;

							$currentForm.removeClass( 'searching' );


							$currentForm.addClass( 'found-products' );

							$results.html( $products );

							$currentForm.removeClass( 'invalid-length' );

							$( document.body ).trigger( 'teckzone_ajax_search_request_success', [$results] );

							// Cache
							searchCache[keycat] = {
								found   : true,
								products: $products
							};

							$currentForm.addClass( 'searched actived' );
						}
					);
				}
			}

		} );
	};

	var recentViewedProductsCarousel = function ( $scope, $ ) {
		$scope.find( '.tz-product-recently-viewed-carousel' ).each( function () {
			var $el = $( this );

			if ( $el.hasClass( 'tz-empty-product' ) ) {
				return;
			}

			getProductCarousel( $el );
		} );
	};

	var recentlyViewedProducts = function ( $scope, $ ) {
		$scope.find( '.teckzone-header-recently-viewed' ).each( function () {
			var $el = $( this );
			loadAjaxRecently( $el );
		} );

		$scope.find( '.teckzone-content-recently-viewed' ).each( function () {
			var $el = $( this );

			$( this ).addClass( 'loaded' );
			loadAjaxRecently( $el );
		} );

		$scope.find( '.teckzone-footer-recently-viewed' ).each( function () {
			var $el = $( this ),
				found = true,
				$content = $el.find( '.recently-viewed-inner' ),
				$layer = $el.find( '.overlay' );

			$el.find( '.recently-title' ).on( 'click', function ( e ) {
				e.preventDefault();

				$layer.toggleClass( 'opened' );

				$content.slideToggle( 400, function () {
					if ( found ) {
						loadAjaxRecently( $el );
						found = false;
					}
				} );

				$( this ).toggleClass( 'active' );
			} );

			$layer.on( 'click', function () {
				$( this ).removeClass( 'opened' );
				$content.slideUp( 400 );
				$el.find( '.recently-title' ).removeClass( 'active' );
			} );
		} );

		$scope.find( '.tz-product-recently-viewed-carousel' ).each( function () {
			var $el = $( this ),
				$checkProduct = $el.find( '.recently-has-products' ).length;

			if ( $checkProduct < 0 ) {
				$el.find( '.recently-viewed-products' ).remove();
			} else {
				$el.find( '.recently-empty-products' ).remove();
			}

			getProductCarousel( $el );
		} );
	};

	function loadAjaxRecently( $selector ) {
		var $recently = $selector.find( '.recently-has-products' ),
			elementSettings = $selector.data( 'number' );

		var data = {
				numbers: elementSettings.numbers,
				nonce  : teckzoneData.nonce
			},
			ajax_url = teckzoneData.ajax_url.toString().replace( '%%endpoint%%', 'teckzone_header_recently_viewed' );

		$.post(
			ajax_url,
			data,
			function ( response ) {
				var $data = $( response.data );

				$recently.html( $data );

				if ( $recently.find( '.product-list' ).hasClass( 'no-products' ) ) {
					$selector.find( '.recently-viewed-products' ).remove();
				} else {
					$selector.find( '.recently-empty-products' ).remove();
				}

				getProductCarousel( $selector );
				$selector
					.addClass( 'products-loaded' )
					.find( '.teckzone-loading--wrapper' ).remove();
			}
		);
	}

	var productsCarousel = function ( $scope, $ ) {
		$scope.find( '.tz-elementor-product-carousel' ).each( function () {
			var $el = $( this ),
				extendOptions = {};

			if ( $el.hasClass( 'tz-product-deals-carousel-2' ) ||
				$el.hasClass( 'tz-product-deals-carousel-3' ) ||
				$el.hasClass( 'tz-product-list-carousel' )
			) {
				extendOptions.appendArrows = $el.find( '.slick-arrows' );
			}

			getProductCarousel( $el, extendOptions );
		} );
	};

	var productsWithCategoryCarousel = function ( $scope, $ ) {
		$scope.find( '.teckzone-products-carousel-with-category' ).each( function () {
			var $el = $( this ),
				bannersSelector = $el.find( '.images-list' ),
				productsSelector = $el.find( '.products-box' );

			var options = {
				prevArrow: '<span class="icon-chevron-left slick-prev-arrow"></span>',
				nextArrow: '<span class="icon-chevron-right slick-next-arrow"></span>'
			};

			bannersSelector.not( '.slick-initialized' ).slick( options );

			$el.on( 'afterChange', function () {
				lazyLoadHandler( $el );
			} );

			getProductCarousel( productsSelector );
		} );
	};

	var productsCategoriesCarousel = function ( $scope, $ ) {
		$scope.find( '.teckzone-product-categories-carousel' ).each( function () {
			var $el = $( this ),
				sliderSelector = $el.find( 'ul.product-cats' );

			var options = {
				prevArrow: '<span class="icon-chevron-left slick-prev-arrow"></span>',
				nextArrow: '<span class="icon-chevron-right slick-next-arrow"></span>'
			};

			sliderSelector.not( '.slick-initialized' ).slick( options );

			$el.on( 'afterChange', function () {
				lazyLoadHandler( $el );
			} );
		} );
	};

	var productsBannerCarousel = function ( $scope, $ ) {
		$scope.find( '.tz-product-with-category' ).each( function () {
			var $selector = $( this ),
				$slider = $selector.find( '.images-list' );

			var options = {
				prevArrow: '<span class="icon-chevron-left slick-prev-arrow"></span>',
				nextArrow: '<span class="icon-chevron-right slick-next-arrow"></span>'
			};

			$slider.not( '.slick-initialized' ).slick( options );

			$selector.on( 'afterChange', function () {
				lazyLoadHandler( $selector );
			} );

		} );
	};

	var productsBannerCarousel2 = function ( $scope, $ ) {
		$scope.find( '.tz-product-with-category-2' ).each( function () {
			var $selector = $( this ),
				$slider = $selector.find( '.images-list' );

			var options = {
				prevArrow: '<span class="icon-chevron-left slick-prev-arrow"></span>',
				nextArrow: '<span class="icon-chevron-right slick-next-arrow"></span>'
			};

			$slider.not( '.slick-initialized' ).slick( options );

			$selector.on( 'afterChange', function () {
				lazyLoadHandler( $selector );
			} );

		} );
	};

	/**
	 * Get Product AJAX
	 */
	var getProductsAJAXHandler = function ( $el, $tabs ) {
		var tab = $el.data( 'href' ),
			$content = $tabs.find( '.tabs-' + tab );

		if ( $content.hasClass( 'tab-loaded' ) ) {
			return;
		}

		var data = {},
			elementSettings = $content.data( 'settings' ),
			ajax_url = teckzoneData.ajax_url.toString().replace( '%%endpoint%%', 'tz_elementor_load_products' );

		$.each( elementSettings, function ( key, value ) {
			data[key] = value;
		} );

		$.post(
			ajax_url,
			data,
			function ( response ) {
				if ( !response ) {
					return;
				}

				$content.html( response.data );

				getProductCarousel( $content.closest( '.tz-elementor-product-carousel' ) );

				lazyLoadHandler( $content );
				$content.addClass( 'tab-loaded' );
			}
		);
	};

	/**
	 * Get Product sList AJAX
	 */
	var getProductsListAJAXHandler = function ( $el, $tabs ) {
		var tab = $el.data( 'href' ),
			$content = $tabs.find( '.tabs-' + tab );

		if ( $content.hasClass( 'tab-loaded' ) ) {
			return;
		}

		var data = {},
			elementSettings = $content.data( 'settings' ),
			ajax_url = teckzoneData.ajax_url.toString().replace( '%%endpoint%%', 'teckzone_ajax_load_products_list' );

		$.each( elementSettings, function ( key, value ) {
			data[key] = value;
		} );

		$.post(
			ajax_url,
			data,
			function ( response ) {
				if ( !response ) {
					return;
				}

				$content.html( response.data );

				getProductCarousel( $content.closest( '.tz-elementor-product-carousel' ) );

				lazyLoadHandler( $content );
				$content.addClass( 'tab-loaded' );
			}
		);
	};

	/**
	 * Load Products
	 */
	var loadProductsGrid = function ( $scope, $ ) {
		$scope.find( '.tz-products-grid' ).each( function () {
			var $this = $( this ),
				ajax_url = teckzoneData.ajax_url.toString().replace( '%%endpoint%%', 'teckzone_ajax_load_products' );

			// Load Products
			$this.on( 'click', 'a.ajax-load-products', function ( e ) {
				e.preventDefault();

				var $el = $( this );

				if ( $el.hasClass( 'loading' ) ) {
					return;
				}

				$el.addClass( 'loading' );

				$.post(
					ajax_url,
					{
						page     : $el.data( 'page' ),
						settings : $el.data( 'settings' ),
						type     : $el.data( 'type' ),
						load_more: $el.data( 'load_more' ),
						text     : $el.data( 'text' ),
						nonce    : $el.data( 'nonce' )
					},
					function ( response ) {
						if ( !response ) {
							return;
						}

						$el.removeClass( 'loading' );

						var $data = $( response.data ),
							$products = $data.find( 'ul.products > li' ),
							$button = $data.find( '.ajax-load-products' ),
							$container = $el.closest( '.tz-products-grid' ),
							$grid = $container.find( 'ul.products' );

						// If has products
						if ( $products.length ) {
							// Add classes before append products to grid
							$products.addClass( 'product' );

							for ( var index = 0; index < $products.length; index++ ) {
								$( $products[index] ).css( 'animation-delay', index * 100 + 100 + 'ms' );
							}

							$products.addClass( 'teckzoneFadeInUp teckzoneAnimation' );
							$grid.append( $products );

							if ( $button.length ) {
								$el.replaceWith( $button );
							} else {
								$el.slideUp();
								$el.closest( '.load-more' ).addClass( 'loaded' );
							}

							lazyLoadHandler( $grid );
						}
					}
				);
			} );

			// AJAX Filter
			$this.find( 'ul.product-filter li:first' ).addClass( 'active' );
			$this.on( 'click', 'ul.product-filter li', function ( e ) {
				e.preventDefault();

				var $el = $( this );

				if ( $el.hasClass( 'loading' ) ) {
					return;
				}

				$el.addClass( 'active' ).siblings( '.active' ).removeClass( 'active' );

				var filter = $el.attr( 'data-filter' ),
					$wrapper = $this.find( '.products-wrapper' );

				var data = {
					nonce: $this.data( 'nonce' ),
					text : $this.data( 'text' ),
					type : $this.data( 'type' )
				};

				data.settings = $this.data( 'settings' );
				data.settings.category = filter;

				data.load_more = $this.data( 'load_more' );

				$this.addClass( 'loading' );

				$.post(
					ajax_url,
					data,
					function ( response ) {
						if ( !response ) {
							return;
						}

						var $data = $( response.data ),
							$products = $data.find( 'ul.products > li' ),
							$button = $data.find( '.ajax-load-products' );

						$this.removeClass( 'loading' );

						// If has products
						if ( $products.length ) {
							// Add classes before append products to grid
							$products.addClass( 'product' );

							for ( var index = 0; index < $products.length; index++ ) {
								$( $products[index] ).css( 'animation-delay', index * 100 + 100 + 'ms' );
							}

							$products.addClass( 'teckzoneFadeInUp teckzoneAnimation' );

							$wrapper.children( 'div.woocommerce, .load-more' ).remove();
							$button.css( 'animation-delay', $products.length * 100 + 100 + 'ms' ).addClass( 'teckzoneFadeIn teckzoneAnimation' );
							$wrapper.append( $data );

							lazyLoadHandler( $wrapper );
						}
					}
				);
			} );
		} );
	};

	/**
	 * Load Products
	 */
	var loadProductsDeals = function ( $scope, $ ) {
		$scope.find( '.tz-product-deals-grid' ).each( function () {
			var $this = $( this ),
				ajax_url = teckzoneData.ajax_url.toString().replace( '%%endpoint%%', 'teckzone_ajax_load_products_deal' );

			// Load products
			$this.on( 'click', 'a.ajax-load-products', function ( e ) {
				e.preventDefault();

				var $el = $( this );

				if ( $el.hasClass( 'loading' ) ) {
					return;
				}

				$el.addClass( 'loading' );

				var data = {
					page        : $el.data( 'page' ),
					product_deal: $el.data( 'settings' ),
					type        : $el.data( 'type' ),
					load_more   : $el.data( 'load_more' ),
					text        : $el.data( 'text' ),
					nonce       : $el.data( 'nonce' )
				};

				$.post(
					ajax_url,
					data,
					function ( response ) {
						if ( !response ) {
							return;
						}

						$el.removeClass( 'loading' );

						var $data = $( response.data ),
							$products = $data.find( 'ul.products > li' ),
							$button = $data.find( '.ajax-load-products' ),
							$container = $el.closest( '.tz-product-deals-grid' ),
							$grid = $container.find( 'ul.products' );

						// If has products
						if ( $products.length ) {
							// Add classes before append products to grid
							$products.addClass( 'product' );

							for ( var index = 0; index < $products.length; index++ ) {
								$( $products[index] ).css( 'animation-delay', index * 100 + 100 + 'ms' );
							}

							$products.addClass( 'teckzoneFadeInUp teckzoneAnimation' );
							$grid.append( $products );

							if ( $button.length ) {
								$el.replaceWith( $button );
							} else {
								$el.slideUp();
								$el.closest( '.load-more' ).addClass( 'loaded' );
							}

							lazyLoadHandler( $grid );
						}
					}
				);
			} );

			// AJAX Filter
			$this.find( 'ul.product-filter li:first' ).addClass( 'active' );
			$this.on( 'click', 'ul.product-filter li', function ( e ) {
				e.preventDefault();

				var $el = $( this );

				if ( $el.hasClass( 'loading' ) ) {
					return;
				}

				$el.addClass( 'active' ).siblings( '.active' ).removeClass( 'active' );

				var filter = $el.attr( 'data-filter' ),
					$wrapper = $this.find( '.products-wrapper' );

				var data = {
					nonce: $this.data( 'nonce' ),
					text : $this.data( 'text' ),
					type : $this.data( 'type' )
				};

				data.product_deal = $this.data( 'settings' );
				data.product_deal.product_cats = filter;

				data.load_more = $this.data( 'load_more' );

				$this.addClass( 'loading' );

				$.post(
					ajax_url,
					data,
					function ( response ) {
						if ( !response ) {
							return;
						}

						var $data = $( response.data ),
							$products = $data.find( 'ul.products > li' ),
							$button = $data.find( '.load-more' );

						$this.removeClass( 'loading' );

						// Add classes before append products to grid
						$products.addClass( 'product' );

						for ( var index = 0; index < $products.length; index++ ) {
							$( $products[index] ).css( 'animation-delay', index * 100 + 100 + 'ms' );
						}

						$products.addClass( 'teckzoneFadeInUp teckzoneAnimation' );
						$button.css( 'animation-delay', $products.length * 100 + 200 + 'ms' ).addClass( 'teckzoneFadeIn teckzoneAnimation' );
						$wrapper.children( 'div.woocommerce, .load-more' ).remove();
						$wrapper.append( $data );

						lazyLoadHandler( $wrapper );
					}
				);
			} );
		} );
	};

	/**
	 *    Load Brands
	 */
	var loadBrands = function ( $scope, $ ) {
		$scope.find( '.tz-products-of-brands' ).each( function () {
			var $this = $( this ),
				text = $this.data( 'text' );

			$this.find( '.load-more a' ).html( '<span class="button-text">' + text + '</span>' + '<span class="teckzone-loading"></span>' );

			$this.on( 'click', '.load-more a', function ( e ) {
				e.preventDefault();

				var $el = $( this ),
					elementId = $el.closest( '.elementor-element' ).data( 'id' );

				if ( $el.data( 'requestRunning' ) ) {
					return;
				}

				$el.data( 'requestRunning', true );

				var $pagination = $el.closest( '.load-more' ),
					$products = $pagination.prev( '.product-brands' );

				$el.addClass( 'loading' );

				$.get(
					$el.attr( 'href' ),
					function ( response ) {
						var $wrapper = $( response ).find( '.elementor-element[data-id="' + elementId.toString() + '"]' ),
							content = $wrapper.find( '.product-brands' ).children( '.brand-item-wrapper' ),
							$pagination_html = $wrapper.find( '.load-more' ).html();

						$pagination.html( $pagination_html );

						for ( var index = 0; index < content.length; index++ ) {
							$( content[index] ).css( {
								'animation-delay': index * 100 + 100 + 'ms'
							} );
						}

						content.addClass( 'teckzoneFadeInUp' );

						$products.append( content );

						getProductCarousel( content.closest( '.tz-products-of-brands' ) );

						$pagination.find( 'a' )
							.data( 'requestRunning', false )
							.html( '<span class="button-text">' + text + '</span>' + '<span class="teckzone-loading"></span>' );

						$el.removeClass( 'loading' );

						if ( !$pagination_html ) {
							$pagination.addClass( 'loaded' );
						}
					}
				);
			} );
		} );
	};

	/**
	 * Product Tabs Carousel
	 */
	var productTabsCarouselHandler = function ( $scope, $ ) {
		$scope.find( '.tz-product-tab-carousel' ).each( function () {
			var $this = $( this );
			$this.find( '.tabs-nav' ).on( 'click', 'a', function ( e ) {
				getProductsListAJAXHandler( $( this ), $this );
			} );
		} );

		$scope.find( '.tz-product-tab-carousel-2' ).each( function () {
			var $this = $( this );
			$this.find( '.tabs-nav' ).on( 'click', 'a', function ( e ) {
				getProductsListAJAXHandler( $( this ), $this );
			} );
		} );

		$scope.find( '.tz-product-tab-carousel-3' ).each( function () {
			var $this = $( this );
			$this.find( '.tabs-nav' ).on( 'click', 'a', function ( e ) {
				getProductsAJAXHandler( $( this ), $this );
			} );
		} );
	};

	var getProductCarousel = function ( $els, extendOptions = {} ) {
		var $selector = $els,
			$slider = $selector.find( 'ul.products,ul.product-list' ),
			dataSettings = $selector.data( 'settings' );

		var data = JSON.stringify( dataSettings );

		var options = {
			prevArrow: '<span class="icon-chevron-left slick-prev-arrow"></span>',
			nextArrow: '<span class="icon-chevron-right slick-next-arrow"></span>'
		};

		$.extend( true, options, extendOptions );

		$slider.attr( 'data-slick', data );
		$slider.not( '.slick-initialized' ).slick( options );

		$selector.on( 'afterChange', function () {
			lazyLoadHandler( $selector );
		} );
	};

	/**
	 * Elementor JS Hooks
	 */
	$( window ).on( "elementor/frontend/init", function () {
		// Countdown
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-countdown.default",
			countDownHandler
		);

		// Counter
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-counter.default",
			counterHandler
		);

		// Masonry
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-image-masonry.default",
			imageMasonry
		);

		// Carousel
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-image-slides.default",
			imageCarousel
		);

		// Timeline
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-timeline.default",
			timeline
		);

		// Testimonials
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-testimonials.default",
			testimonial
		);

		// Testimonials 2
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-testimonials-2.default",
			testimonial2
		);

		// Tab
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-tab-list.default",
			tab
		);

		// Video
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-video.default",
			videoLightBox
		);

		// Slider
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-slides.default",
			slideCarousel
		);

		// Posts Carousel
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-posts-carousel.default",
			postsCarousel
		);

		// Image Carousel
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-images-carousel.default",
			imagesCarousel
		);

		// Trending Carousel
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-trending-search-carousel.default",
			trendingSearchCarousel
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-trending-search-carousel-2.default",
			trendingSearchCarousel
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-trending-search-carousel-3.default",
			trendingSearchCarousel
		);

		// Promotion
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-promotion.default",
			promotionHandler
		);

		// Search Form
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-search-form.default",
			searchFormHandler
		);

		// Recently View Products
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-recently-viewed-products.default",
			recentlyViewedProducts
		);
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-content-recently-viewed.default",
			recentlyViewedProducts
		);
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-footer-viewed-products.default",
			recentlyViewedProducts
		);
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/tz-product-recently-viewed-carousel.default",
			recentViewedProductsCarousel
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/tz-product-recently-viewed-carousel-2.default",
			recentViewedProductsCarousel
		);

		// Product Deals Carousel
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-deals-carousel.default",
			productsCarousel
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-deals-carousel-2.default",
			productsCarousel
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-deals-carousel-3.default",
			productsCarousel
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-deals-carousel-2.default",
			productDealsCarousel2Handler
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-deals-carousel-3.default",
			productDealsCarousel3Handler
		);

		// Product Deals Grid
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-deals-grid.default",
			loadProductsDeals
		);

		// Product Grid
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-grid.default",
			loadProductsGrid
		);

		// Product Carousel
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-carousel.default",
			productsCarousel
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-products-carousel-2.default",
			productsCarousel
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-products-carousel-with-category.default",
			productsWithCategoryCarousel
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-products-carousel-with-banner.default",
			productsCarousel
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-products-carousel-with-banner-2.default",
			productsCarousel
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-list-carousel.default",
			productsCarousel
		);

		// Product With Category
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-with-category.default",
			productsBannerCarousel
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-with-category-2.default",
			productsBannerCarousel2
		);

		// Product tab carousel
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-tab-carousel.default",
			productsCarousel
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-tab-carousel-2.default",
			productsCarousel
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-tab-carousel-3.default",
			productsCarousel
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-tab-carousel.default",
			productTabsCarouselHandler
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-tab-carousel-2.default",
			productTabsCarouselHandler
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-tab-carousel-3.default",
			productTabsCarouselHandler
		);

		// Product Categories Carousel
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-product-categories-carousel.default",
			productsCategoriesCarousel
		);

		// Product Brand
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-products-brands.default",
			productsCarousel
		);

		elementorFrontend.hooks.addAction(
			"frontend/element_ready/techzone-products-brands.default",
			loadBrands
		);
	} );
})
( jQuery );