jQuery( document ).ready( function ( $ ) {
	"use strict";

	/*---------
	 Thumbnail
	 ---------*/
// Only show the "remove image" button when needed
	if ( !$( '#product_collection_thumb_id' ).val() ) {
		$( '.remove_image_button' ).hide();
	}

// Uploading files
	var file_frame;

	$( '#product-collection-thumb-box' ).on( 'click', '.upload_image_button', function ( event ) {

		event.preventDefault();

		// If the media frame already exists, reopen it.
		if ( file_frame ) {
			file_frame.open();
			return;
		}

		// Create the media frame.
		file_frame = wp.media.frames.downloadable_file = wp.media( {
			multiple: false
		} );

		// When an image is selected, run a callback.
		file_frame.on( 'select', function () {
			var attachment = file_frame.state().get( 'selection' ).first().toJSON();
			var url = '';
			$( '#product_collection_thumb_id' ).val( attachment.id );
			var attachment_image = attachment.sizes && attachment.sizes.thumbnail ? attachment.sizes.thumbnail.url : attachment.url;
			$( '#product_collection_thumb' ).find( 'img' ).attr( 'src', attachment_image );
			$( '.remove_image_button' ).show();
		} );

		// Finally, open the modal.
		file_frame.open();
	} );

	$( '#product-collection-thumb-box' ).on( 'click', '.remove_image_button', function () {
		var image_src = $( '#product_collection_thumb' ).data( 'rel' );
		$( '#product_collection_thumb' ).find( 'img' ).attr( 'src', image_src );
		$( '#product_collection_thumb_id' ).val( '' );
		$( '.remove_image_button' ).hide();
		return false;
	} );

	/*-------
	 Gallery
	 -------*/
	var file_frame_2,
		$collection_gallery_id = $( '#product_collection_gallery_id' ),
		$collection_gallery_link = $( '#product_collection_gallery_link' ),
		$collection_gallery = $( '#product_collection_gallery' ),
		$collection_images = $collection_gallery.find( '.product-collection-images' );

	$collection_gallery.on( 'click', '.upload_images_button', function ( event ) {
		var $el = $( this );

		event.preventDefault();

		// If the media frame already exists, reopen it.
		if ( file_frame_2 ) {
			file_frame_2.open();
			return;
		}

		// Create the media frame.
		file_frame_2 = wp.media.frames.downloadable_file = wp.media( {
			multiple: true
		} );

		// When an image is selected, run a callback.
		file_frame_2.on( 'select', function () {
			var selection = file_frame_2.state().get( 'selection' ),
				attachment_ids = $collection_gallery_id.val();

			selection.map( function ( attachment ) {
				attachment = attachment.toJSON();

				if ( attachment.id ) {
					attachment_ids = attachment_ids ? attachment_ids + ',' + attachment.id : attachment.id;
					var attachment_image = attachment.sizes && attachment.sizes.thumbnail ? attachment.sizes.thumbnail.url : attachment.url;

					$collection_images.append( '<li class="image" data-attachment_id="' + attachment.id + '"><img src="' + attachment_image + '" width="100px" height="100px" /><ul class="actions"><li><a href="#" class="delete" title="' + $el.data( 'delete' ) + '">' + $el.data( 'text' ) + '</a></li></ul></li>' );
				}

			} );
			$collection_gallery_id.val( attachment_ids );
		} );


		// Finally, open the modal.
		file_frame_2.open();
	} );

	// Image ordering.
	$collection_images.sortable( {
		items               : 'li.image',
		cursor              : 'move',
		scrollSensitivity   : 40,
		forcePlaceholderSize: true,
		forceHelperSize     : false,
		helper              : 'clone',
		opacity             : 0.65,
		placeholder         : 'wc-metabox-sortable-placeholder',
		start               : function ( event, ui ) {
			ui.item.css( 'background-color', '#f6f6f6' );
		},
		stop                : function ( event, ui ) {
			ui.item.removeAttr( 'style' );
		},
		update              : function () {
			var attachment_ids = '';

			$collection_images.find( 'li.image' ).css( 'cursor', 'default' ).each( function () {
				var attachment_id = $( this ).attr( 'data-attachment_id' );
				attachment_ids = attachment_ids + attachment_id + ',';
			} );

			$collection_gallery_id.val( attachment_ids );
		}
	} );

	// Remove images.
	$collection_gallery.on( 'click', 'a.delete', function () {
		$( this ).closest( 'li.image' ).remove();

		var attachment_ids = '';

		$collection_images.find( 'li.image' ).css( 'cursor', 'default' ).each( function () {
			var attachment_id = $( this ).attr( 'data-attachment_id' );
			attachment_ids = attachment_ids + attachment_id + ',';
		} );

		$collection_gallery_id.val( attachment_ids );

		return false;
	} );

	/*------------
	 Ajax Complete
	 ------------*/

	$( document ).ajaxComplete( function ( event, request, options ) {
		if ( request && 4 === request.readyState && 200 === request.status
			&& options.data && 0 <= options.data.indexOf( 'action=add-tag' ) ) {

			var res = wpAjax.parseAjaxResponse( request.responseXML, 'ajax-response' );
			if ( !res || res.errors ) {
				return;
			}
			// Clear Thumbnail fields on submit
			$( '#product_collection_thumb' ).find( 'img' ).attr( 'src', $( '#product_collection_thumb' ).data( 'rel' ) );
			$( '#product_collection_thumb_id' ).val( '' );
			$( '.remove_image_button' ).hide();

			$collection_gallery.find( 'li.image' ).remove();
			$collection_gallery_id.val( '' );
			$collection_gallery_link.val( '' );
			return;
		}
	} );

} );