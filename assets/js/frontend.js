(function ( $ ) {
	'use strict';

	var teckzone = teckzone || {};

	teckzone.init = function () {
		teckzone.$body = $( document.body ),
			teckzone.$window = $( window ),
			teckzone.$header = $( '#masthead' );
	};

	/**
	 * Document ready
	 */
	$( function () {
		teckzone.init();
	} );

})( jQuery );